/** * Copyright (c) 2017 Kouchan, Inc. All Rights Reserved*/
package com.kouchan.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.kouchan.dao.ApplicationUserDao;


@Component("customUserDetailsService")
public class ApplicationUserService implements UserDetailsService {

	@Autowired
	private ApplicationUserDao repo;
	
	@Override
	public UserDetails loadUserByUsername(String mobile) throws UsernameNotFoundException {
		return (UserDetails) repo.findByUsername(mobile);		
	}
	


}
