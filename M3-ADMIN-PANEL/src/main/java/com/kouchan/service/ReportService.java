package com.kouchan.service;

import java.util.Date;
import java.util.List;

import com.kouchan.dto.CustomerTransactionCreditAndDebitDto;
import com.kouchan.to.CustomerModel;
import com.kouchan.to.TransactionModel;
import com.kouchan.to.WalletBalanceModel;

public interface ReportService {
	
	public List<CustomerModel> getWalletBalanceReportData();

	public List<TransactionModel> getCustomerTransactionReportData();

	public List<WalletBalanceModel> getWalletMISReportData();
	
	public List<CustomerModel> getWalletBalanceReportForDaily();
	
	public List<CustomerModel> getWalletBalanceReportForWeekly();
	
	public List<CustomerModel> getWalletBalanceReportForMonthly();
	
	public List<TransactionModel> getCustomerTransactionReportForDaily();
	
	public List<TransactionModel> getCustomerTransactionReportForWeekly();
	
	public List<TransactionModel> getCustomerTransactionReportForMonthly();

	
	public List<CustomerTransactionCreditAndDebitDto> getCustomerTransactionReportForDaily(Date date);

}
