/** * Copyright (c) 2017 Kouchan, Inc. All Rights Reserved*/
package com.kouchan.security;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import com.kouchan.dao.ApplicationUserDao;
import com.kouchan.service.ApplicationUserService;
import com.kouchan.to.UserRole;
import com.kouchan.util.DESEncryptionUtil;

@Component("customAuthenticationProvider")
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private ApplicationUserDao repo;

	@Autowired
	ApplicationUserService applicationUserService;

	String role;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		try {
			UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
			//!DESEncryptionUtil.decrypt(user.getPassword())
			com.kouchan.to.User user = repo.findByUsername(token.getName());
			if (user != null) {
				if (!DESEncryptionUtil.decrypt(user.getPassword()).equalsIgnoreCase(token.getCredentials().toString())) {
					throw new BadCredentialsException("The credentials are invalid");
				}
			} else {
				throw new BadCredentialsException("The credentials are invalid");
			}

			List<GrantedAuthority> authorities = buildUserAuthority(user.getUserRole());

			buildUserForAuthentication(user, authorities);

			return new UsernamePasswordAuthenticationToken(user, user.getPassword(), authorities);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
			throw new BadCredentialsException("The credentials are invalid");
		}

	}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.equals(authentication);
	}

	// Converts com.kouchan.users.model.User user to
	private User buildUserForAuthentication(com.kouchan.to.User user, List<GrantedAuthority> authorities) {
		return new User(user.getUserName(), user.getPassword(), user.isEnabled(), true, true, true, authorities);
	}

	private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		// Build user's authorities
		for (UserRole userRole : userRoles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
		}

		List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

		return Result;
	}

}
