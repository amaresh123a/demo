package com.kouchan.daoImpl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.stereotype.Repository;

import com.kouchan.dao.ICommonDao;
import com.kouchan.to.CustomerModel;

@Repository
public class CommonDaoImpl<T> implements Serializable, ICommonDao<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4883625114561194339L;

	@Override
	public void save(T entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(T entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void saveOrUpdate(T entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(T entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void trash(T entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<T> findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<T> findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<T> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<T> findAll(DetachedCriteria criteria) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<T> findAll(Class entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CustomerModel getCustomer(String customerId) {
		// TODO Auto-generated method stub
		return null;
	}
}
