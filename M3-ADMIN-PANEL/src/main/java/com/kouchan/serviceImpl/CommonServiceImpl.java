package com.kouchan.serviceImpl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kouchan.dao.CustomerDao;
import com.kouchan.dao.ICommonDao;
import com.kouchan.service.ICommonService;
import com.kouchan.to.CustomerModel;

@Service("commonServiceImpl")
public class CommonServiceImpl<T> implements java.io.Serializable,
		ICommonService<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -102576500954927480L;
	@Autowired
	ICommonDao<T> commonDao;
	@Autowired
	CustomerDao customerDao;

	@Override
	@Transactional
	public void save(T entity) {

		commonDao.save(entity);
	}

	@Override
	@Transactional
	public void update(T entity) {

		commonDao.update(entity);
	}

	@Override
	@Transactional
	public void saveOrUpdate(T entity) {

		commonDao.saveOrUpdate(entity);
	}

	@Override
	@Transactional
	public void delete(T entity) {

		commonDao.delete(entity);
	}

	@Override
	@Transactional
	public void trash(T entity) {
		
		commonDao.trash(entity);
	}

	@Override
	@Transactional
	public List<T> findByName(String name) {

		return commonDao.findByName(name);
	}

	@Override
	@Transactional
	public List<T> findById(int id) {

		return commonDao.findById(id);
	}

	@Override
	@Transactional
	public List<T> findAll() {

		return commonDao.findAll();
	}

	@Override
	@Transactional
	public List<T> findAll(DetachedCriteria criteria) {

		return commonDao.findAll(criteria);
	}

	@Override
	@Transactional
	public List<T> findAll(@SuppressWarnings("rawtypes") Class entity) {

		return commonDao.findAll(entity);
	}

	@Override
	public CustomerModel getCustomer(String customerId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CustomerModel> getTop10Profiles() {
		// TODO Auto-generated method stub
		return customerDao.getTop10Profiles();
	}

}
