package com.kouchan.serviceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.dao.ReportDao;
import com.kouchan.dto.CustomerTransactionCreditAndDebitDto;
import com.kouchan.service.ReportService;
import com.kouchan.to.CustomerModel;
import com.kouchan.to.TransactionModel;
import com.kouchan.to.WalletBalanceModel;
import com.kouchan.util.DateUtil;
import com.kouchan.util.SortByCustomerId;

@Service("reportService")
public class ReportServiceImpl implements ReportService {

	
	@Autowired
	ReportDao reportDao;

	@Override
	public List<CustomerModel> getWalletBalanceReportData() {
		 List<CustomerModel> walletBalanceReportData = reportDao.getWalletBalanceReportData();
		
		return reportDao.getWalletBalanceReportData();
	}

	@Override
	public List<TransactionModel> getCustomerTransactionReportData() {
		return reportDao.getCustomerTransactionReportData();
	}

	@Override
	public List<WalletBalanceModel> getWalletMISReportData() {
		return reportDao.getWalletMISReportData();
	}

	@Override
	public List<CustomerModel> getWalletBalanceReportForDaily() {
		return reportDao.getWalletBalanceReportForDaily(new Date());
	}

	@Override
	public List<CustomerModel> getWalletBalanceReportForWeekly() {
		 Map<String, Date> weekDifference = DateUtil.getWeekDifference();
		 Date startDate = (Date) weekDifference.get("startDate");
		 Date endDate = (Date) weekDifference.get("endDate");
		return reportDao.getWalletBalanceReportForWeeklyOrMonthly(endDate, startDate);
	}
	
	@Override
	public List<CustomerModel> getWalletBalanceReportForMonthly() {
		 Map<String, Date> monthDifference = DateUtil.getMonthDifference();
		 Date startDate = (Date) monthDifference.get("startDate");
		 Date endDate = (Date) monthDifference.get("endDate");
		return reportDao.getWalletBalanceReportForWeeklyOrMonthly(endDate, startDate);
	}

	@Override
	public List<TransactionModel> getCustomerTransactionReportForDaily() {
		return reportDao.getTransactionReportForDaily(new Date());
	}

	@Override
	public List<TransactionModel> getCustomerTransactionReportForWeekly() {
		 Map<String, Date> weekDifference = DateUtil.getWeekDifference();
		 Date startDate = (Date) weekDifference.get("startDate");
		 Date endDate = (Date) weekDifference.get("endDate");
		return reportDao.getTransactionReportForWeeklyOrMonthly(endDate, startDate);
	}

	@Override
	public List<TransactionModel> getCustomerTransactionReportForMonthly() {
		 Map<String, Date> monthDifference = DateUtil.getMonthDifference();
		 Date startDate = (Date) monthDifference.get("startDate");
		 Date endDate = (Date) monthDifference.get("endDate");
		return reportDao.getTransactionReportForWeeklyOrMonthly(endDate, startDate);
	}

	@Override
	 public List<CustomerTransactionCreditAndDebitDto> getCustomerTransactionReportForDaily(Date date) {
	  List<TransactionModel> customerTransactionReportForDaily = reportDao.getTransactionReportForDaily(date);
	  List<CustomerTransactionCreditAndDebitDto> customerTransaction = new ArrayList<>();
	  if(!customerTransactionReportForDaily.isEmpty() && customerTransactionReportForDaily != null)
	  {
	   for(TransactionModel transactionModel : customerTransactionReportForDaily)
	   {
	    CustomerTransactionCreditAndDebitDto creditAndDebit = new CustomerTransactionCreditAndDebitDto();
	    creditAndDebit.setCustomerId(transactionModel.getCustomer().getId());
	    creditAndDebit.setMobileNumber(transactionModel.getCustomer().getMobile());
	    creditAndDebit.setEmailId(transactionModel.getCustomer().getEmail());
	    creditAndDebit.setTransactionName(transactionModel.getFundType().getName());
	    creditAndDebit.setCreatedDate(transactionModel.getCreatedDate());
	    creditAndDebit.setTransactionAmount(transactionModel.getAmount());
	    customerTransaction.add(creditAndDebit);
	   }
	  }
	  Collections.sort(customerTransaction, new SortByCustomerId());
	  return customerTransaction;
	 }

}
