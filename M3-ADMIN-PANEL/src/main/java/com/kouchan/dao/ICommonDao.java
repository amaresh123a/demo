package com.kouchan.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import com.kouchan.to.CustomerModel;

public interface ICommonDao<T> {

	public void save(T entity);

	public void update(T entity);

	public void saveOrUpdate(T entity);

	public void delete(T entity);

	public void trash(T entity);

	public List<T> findByName(String name);

	public List<T> findById(int id);

	public List<T> findAll();

	public List<T> findAll(DetachedCriteria criteria);

	@SuppressWarnings("rawtypes")
	public List<T> findAll(Class entity);

	public CustomerModel getCustomer(String customerId);

}
