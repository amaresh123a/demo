/** * Copyright (c) 2017 Kouchan, Inc. All Rights Reserved*/
package com.kouchan.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kouchan.to.User;

@Repository
public interface ApplicationUserDao extends JpaRepository<User, Long>{
	
	@Query("FROM  User  t where t.userName= :userName")
	public User findByUsername(@Param("userName") String userName);
	
	
	
	@Query("SELECT max(id) FROM  User")
	public Long getMaxId();
	
	@Query("SELECT max(loginAttemptsCount) FROM  User")
	public Long getMaxLoginAttemptsCount();
}
