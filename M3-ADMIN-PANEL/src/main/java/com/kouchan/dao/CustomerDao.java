/** * Copyright (c) 2017 Kouchan, Inc. All Rights Reserved*/
package com.kouchan.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kouchan.to.CustomerModel;

@Repository
public interface CustomerDao extends JpaRepository<CustomerModel, Long>{
	/*@Query(nativeQuery = true,
		       value ="SELECT * FROM customer  ORDER BY id DESC LIMIT 0, 10")*/
	@Query(nativeQuery = true,
    value ="SELECT * FROM customer  ORDER BY id DESC LIMIT 0, 10")
	public List<CustomerModel> getTop10Profiles();
	
	

}
