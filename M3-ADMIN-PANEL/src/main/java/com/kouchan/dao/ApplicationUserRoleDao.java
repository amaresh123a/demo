/** * Copyright (c) 2017 Kouchan, Inc. All Rights Reserved*/
package com.kouchan.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kouchan.to.UserRole;

@Repository
public interface ApplicationUserRoleDao extends JpaRepository<UserRole, Long>{
	
	@Query("FROM  UserRole  t where t.role= :role")
	public UserRole getUserRole(@Param("role") Long role);
	

}
