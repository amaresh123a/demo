package com.kouchan.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kouchan.to.CustomerModel;
import com.kouchan.to.TransactionModel;
import com.kouchan.to.WalletBalanceModel;
@Repository
public interface ReportDao extends JpaRepository<CustomerModel, Long>{
	
	//@Query(nativeQuery=true,value="select c.mobile,c.name,c.email,wb.balance,wb.created_date,cs.name as status from customer c inner join wallet_balance wb on c.wallet_balance_id = wb.id inner join customer_status cs on c.customer_status_id=cs.id")
	@Query("SELECT Cus FROM CustomerModel Cus")
	List<CustomerModel> getWalletBalanceReportData();
	
	@Query("SELECT Cus FROM CustomerModel Cus where Cus.createdDate = ?1 ")
	List<CustomerModel> getWalletBalanceReportForDaily(Date presentDate);
	
	@Query("SELECT c FROM CustomerModel c where DATE(c.createdDate) between ?1 and ?2 ")
	List<CustomerModel> getWalletBalanceReportForWeeklyOrMonthly(Date createdDate, Date toDate);
	
	List<CustomerModel> findByCreatedDateBetween(Date formDate,Date toDate);
	
	//select t from TransactionHistory t where t.mobileNumber=?1  and DATE(t.transactionHisoryDate) between ?2 and ?3 
	
	//Amaresh
	/*@Query("SELECT Cus FROM CustomerModel Cus")
	List<CustomerModel> getCustomerTransactionReportData();*/
	
	//Devus
	@Query("select t from TransactionModel t")
	List<TransactionModel> getCustomerTransactionReportData();

	@Query("SELECT t FROM TransactionModel t where t.createdDate = ?1 ")
	List<TransactionModel> getTransactionReportForDaily(Date createdDate);

	@Query("SELECT t FROM TransactionModel t where DATE(t.createdDate) between ?1 and ?2 ")
	List<TransactionModel> getTransactionReportForWeeklyOrMonthly(Date startDate, Date endDate);

	@Query("SELECT WB FROM WalletBalanceModel WB")
	List<WalletBalanceModel> getWalletMISReportData();
}
