/** * Copyright (c) 2017 Kouchan, Inc. All Rights Reserved*/
package com.kouchan.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kouchan.dao.ApplicationUserDao;
import com.kouchan.dto.CustomerTransactionDetailsDto;
import com.kouchan.service.ICommonService;
import com.kouchan.service.ReportService;
import com.kouchan.to.CustomerModel;
import com.kouchan.to.TransactionModel;
import com.kouchan.to.User;
import com.kouchan.to.UserRole;
import com.kouchan.util.DESEncryptionUtil;

@Controller
@RequestMapping("/")
public class ApplicationUserController {

	@Autowired
	private ApplicationUserDao repo;
	
	@Autowired
	ICommonService iCommonService;
	
	@Autowired
	 ReportService reportService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String goLogin() {
		return "login";
	}

	@RequestMapping(method = RequestMethod.GET)
	public String goHome() {
		return "home";
	}

	@RequestMapping("/index")
	public String goDashBoard(HttpServletRequest request,CustomerModel user,Model model) {
		
		
		//Long maxLoginAttemptsCount = repo.getMaxLoginAttemptsCount();
		
		HttpSession session = request.getSession(false);
		List<CustomerModel> top10ProfileList = iCommonService.getTop10Profiles();
		session.setAttribute("top10ProfileList",top10ProfileList);
		List<TransactionModel> customers = reportService.getCustomerTransactionReportData();
		  List<CustomerTransactionDetailsDto> customerDetails = new ArrayList<>();
		  for(TransactionModel transactionModel : customers)
		  {
		   CustomerTransactionDetailsDto detailsDto = new CustomerTransactionDetailsDto();
		   detailsDto.setCustomerId(transactionModel.getCustomer().getId());
		   detailsDto.setMobileNumber(transactionModel.getCustomer().getMobile());
		   detailsDto.setTransactionAmount(transactionModel.getAmount());
		   detailsDto.setTransactionType(transactionModel.getFundType().getName());
		   detailsDto.setTransferType(transactionModel.getTransferType().getName());
		   detailsDto.setVendorName(transactionModel.getVendorType().getName());
		   detailsDto.setTransferStatus(transactionModel.getTransferStatus().getName());
		   detailsDto.setWalletBalance(transactionModel.getCustomer().getWalletBalance().getBalance());
		   detailsDto.setCommission(transactionModel.getCommission());
		   customerDetails.add(detailsDto);
		  }
		  model.addAttribute("transactionDetailsList", customerDetails);
		return "index";
	}

	@RequestMapping(value = "/createUser", method = RequestMethod.POST)
	@ResponseBody
	public String createUser(HttpServletRequest request, @ModelAttribute User user) {
		StringBuilder status = new StringBuilder("");

		if(validateUser(request, user, status)) {
			try {
				Long nextId = repo.getMaxId();
				nextId = nextId + 1;
				user.setId(new Long(nextId));
				user.setEnabled(true);
				String role = (String) request.getParameter("role");
				UserRole userRole = new UserRole(user, role);
				user.getUserRole().add(userRole);
				user.setCreatedOn(new Date());
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				User principal =(User) auth.getPrincipal();
				user.setCreatedBy(principal.getUserName());
				String encryptPassWord = DESEncryptionUtil.encrypt(user.getPassword());
				user.setPassword(encryptPassWord);

				repo.save(user);
			} catch (Exception e) {
				e.printStackTrace();
				status.append("ERROR:"+ e.getMessage());
			}
		}
		
		return status.toString();
	}

	@RequestMapping(value = "/createUser", method = RequestMethod.GET)
	public String createUserForm() {
		return "createUser";
	}
	
	private boolean validateUser(HttpServletRequest request, User user, StringBuilder status) {
		boolean validateSuccess = true;

		StringBuilder errorMessage = new StringBuilder(status.toString());
		if(StringUtils.isEmpty(user.getUserName())) {
			validateSuccess = false;
			errorMessage.append(" User Name required ");
		}
		if(StringUtils.isEmpty(user.getPassword())) {
			validateSuccess = false;
			errorMessage.append(" Password required "+"\n");
		}
		
		String confirmPassword = (String) request.getParameter("confirmPassword");
		
		if(StringUtils.isEmpty(confirmPassword)) {
			validateSuccess = false;
			errorMessage.append(" Confirm Password is required "+"\n");
		}
		if(validateSuccess && !user.getPassword().equals(confirmPassword)) {
			validateSuccess = false;
			errorMessage.append(" Confirm password not matched "+"\n");
		}
		
		if(validateSuccess) {
			if(!isUserExist(user)) {
				validateSuccess = false;
				errorMessage.append(" User already exists");
			}
		}
		if(!validateSuccess) {
			status.append("ERROR:");
			status.append(errorMessage);
		}
		return validateSuccess;
	}

	private boolean isUserExist(User user) {
		boolean validateSuccess = true;
		User existedUser = repo.findByUsername(user.getUserName());
		if(existedUser != null) {
			validateSuccess = false;
		}
		return validateSuccess;
	}
}
