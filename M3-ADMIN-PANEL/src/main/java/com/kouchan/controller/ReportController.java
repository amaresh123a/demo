
package com.kouchan.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.kouchan.dto.CustomerTransactionCreditAndDebitDto;
import com.kouchan.dto.CustomerTransactionDetailsDto;
import com.kouchan.service.ReportService;
import com.kouchan.to.CustomerModel;
import com.kouchan.to.TransactionModel;
import com.kouchan.to.WalletBalanceModel;
import com.kouchan.utils.FileUtils;

@Controller
@RequestMapping("/")
public class ReportController {
	@Autowired
	ReportService reportService;
	private Document document;
	private PdfWriter writer;

	@RequestMapping(value = "/customerTransactionReport", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void customerTransactionReport(@RequestParam(value = "reportType", required = false) String reportType,
			HttpServletRequest request, HttpServletResponse response)
			throws  DocumentException, IOException 
	{
		List<TransactionModel> customerTransactionReportData = reportService.getCustomerTransactionReportData();

		if ("customerTransactionPdf".equals(reportType)) {
			ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
			openPdf(byteArray);

			PdfPTable table1 = new PdfPTable(1);
			table1.setWidthPercentage(100);
			table1.addCell(getCell("Customer Transaction Report", PdfPCell.ALIGN_CENTER));
			
			PdfPTable table = new PdfPTable(6);
			
			table.setWidthPercentage(100.0f);
			table.setWidths(new float[] { 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f});
			table.setSpacingBefore(10);

			// define font for table header row
			Font font = FontFactory.getFont(FontFactory.HELVETICA);
			font.setColor(BaseColor.WHITE);

			// define table header cell
			PdfPCell cell = new PdfPCell();
			cell.setBackgroundColor(BaseColor.GRAY);
			cell.setPadding(5);

			// write table header
			cell.setPhrase(new Phrase("Mobile Number", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Customer Email ID", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Transaction Date", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Transaction Type", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Reference Number", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Transaction Amount", font));
			table.addCell(cell);

			/*cell.setPhrase(new Phrase("Merchant_Detail", font));
			table.addCell(cell);*/

			// write table row data

			if (!customerTransactionReportData.isEmpty() && customerTransactionReportData != null) {

				for (TransactionModel model : customerTransactionReportData) {
					table.addCell(model.getCustomer().getMobile());
					table.addCell(model.getCustomer().getEmail());
					table.addCell(model.getCreatedDate().toString());
					table.addCell(model.getTransferType().getName());
					table.addCell(model.getKouchanReferenceNumber());
					table.addCell(String.valueOf(model.getAmount()));
					/*MerchantDetailsDto merchantDetailsDto = new ObjectMapper().readValue(model.getDetails(), MerchantDetailsDto.class);
					table.addCell(merchantDetailsDto.getSentTo());*/
				}
			}
			document.add(table1);
			document.add(table);
			document.close();

			String title = "Customer Transaction Report";
			writeAndUploadFile(byteArray, title, response);
		} else {
			if ("transactionExcelDaily".equals(reportType)) {
				customerTransactionReportData = reportService.getCustomerTransactionReportForDaily();
			} else if ("transactionExcelWeekly".equals(reportType)) {
				customerTransactionReportData = reportService.getCustomerTransactionReportForWeekly();
			} else if ("transactionExcelMonthly".equals(reportType)) {
				customerTransactionReportData = reportService.getCustomerTransactionReportForMonthly();
			}
			HSSFWorkbook workBook = new HSSFWorkbook();
			String workSheetName = "Transaction Details";
			HSSFSheet workSheet = workBook.createSheet(workSheetName);

			// Header Font
			HSSFFont headerFont = workBook.createFont();
			headerFont.setFontHeightInPoints((short) 11);
			headerFont.setFontName("Calibri");

			HSSFFont rowFont = workBook.createFont();
			rowFont.setFontHeightInPoints((short) 11);
			rowFont.setFontName("Calibri");

			// Header Style
			HSSFCellStyle headerCellStyle = workBook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyle = workBook.createCellStyle();
			rowCellStyle.setFont(rowFont);
			HSSFFont rowFontTableHeaderBold1 = workBook.createFont();
			rowFontTableHeaderBold1.setFontHeightInPoints((short) 11);
			rowFontTableHeaderBold1.setFontName("Calibri");
			rowFontTableHeaderBold1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			HSSFCellStyle rowCellStyleTotal1 = workBook.createCellStyle();
			rowCellStyleTotal1.setFont(rowFontTableHeaderBold1);
			HSSFRow row1 = workSheet.createRow(0);
			HSSFCell nationalIdCell1 = row1.createCell(0);
			nationalIdCell1.setCellStyle(rowCellStyleTotal1);

			// Header
			HSSFRow headerRow = workSheet.createRow(1);
			HSSFCell headerCell = null;
			 for(int columnPosition = 0; columnPosition< 6; columnPosition++) {
				 workSheet.autoSizeColumn((short) (columnPosition));
	        }
			String[] thisIsAStringArray = { "Mobile Number", "Customer email ID", "Transaction Date", "Transaction Type",
					"Reference Number", "Transaction Amount"};
			for (int i = 0; i < thisIsAStringArray.length; i++) {
				headerCell = headerRow.createCell(i);
				headerCell.setCellValue(thisIsAStringArray[i]);
				HSSFFont rowFontTableHeaderBold = workBook.createFont();
				rowFontTableHeaderBold.setFontHeightInPoints((short) 11);
				rowFontTableHeaderBold.setFontName("Calibri");
				rowFontTableHeaderBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

				// Header Style
				HSSFCellStyle headerCellStyleTableHeader = workBook.createCellStyle();
				headerCellStyleTableHeader.setFont(headerFont);
				headerCellStyleTableHeader.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
				headerCellStyleTableHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

				// Row Style
				HSSFCellStyle rowCellStyleTotal = workBook.createCellStyle();
				rowCellStyleTotal.setFont(rowFontTableHeaderBold);
				headerCell.setCellStyle(rowCellStyleTotal);
			}
			// short rowNumber1 = (short) (workSheet.getLastRowNum() + 1);

			HSSFFont rowFontTableHeaderBold = workBook.createFont();
			rowFontTableHeaderBold.setFontHeightInPoints((short) 11);
			rowFontTableHeaderBold.setFontName("Calibri");
			rowFontTableHeaderBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

			// Header Style
			HSSFCellStyle headerCellStyleTotal = workBook.createCellStyle();
			headerCellStyleTotal.setFont(headerFont);
			headerCellStyleTotal.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyleTotal.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyleTotal = workBook.createCellStyle();
			rowCellStyleTotal.setFont(rowFontTableHeaderBold);

			HSSFCellStyle cellStyleForDate = workBook.createCellStyle();

			if (customerTransactionReportData != null && !customerTransactionReportData.isEmpty()) {

				for (TransactionModel transaction : customerTransactionReportData) {

					short rowNumber = (short) (workSheet.getLastRowNum() + 1);

					HSSFRow row = workSheet.createRow(rowNumber);
					for(int columnPosition = 0; columnPosition< 6; columnPosition++) {
						 workSheet.autoSizeColumn((short) (columnPosition));
			        }
					// For Mobile No
					HSSFCell nationalIdCell = row.createCell(0);
					nationalIdCell.setCellStyle(rowCellStyle);
					nationalIdCell.setCellValue(transaction.getCustomer().getMobile());

					// For Email ID
					HSSFCell firstNameCell = row.createCell(1);
					firstNameCell.setCellStyle(rowCellStyle);
					firstNameCell.setCellValue(transaction.getCustomer().getEmail());

					// For Transaction date
					HSSFCell employeeNumberCell = row.createCell(2);
					employeeNumberCell.setCellStyle(rowCellStyle);

					CreationHelper createHelper = workBook.getCreationHelper();
					cellStyleForDate.setDataFormat(createHelper.createDataFormat().getFormat("yyyy/MM/dd"));
					if (null != transaction.getCreatedDate()) {

						employeeNumberCell.setCellValue(transaction.getCreatedDate());
						employeeNumberCell.setCellStyle(cellStyleForDate);
					}

					// For Transaction type
					HSSFCell middleNameCell = row.createCell(3);
					middleNameCell.setCellStyle(rowCellStyle);
					middleNameCell.setCellValue(transaction.getTransferType().getName());

					// For Reference number
					HSSFCell middleNameCell1 = row.createCell(4);
					middleNameCell1.setCellStyle(rowCellStyle);
					middleNameCell1.setCellValue(transaction.getKouchanReferenceNumber());
					middleNameCell1.setCellStyle(cellStyleForDate);

					// For Transaction amount
					HSSFCell middleNameCell2 = row.createCell(5);
					middleNameCell2.setCellStyle(rowCellStyle);
					middleNameCell2.setCellValue(transaction.getAmount());

					/*// For Merchant details
					HSSFCell middleNameCell3 = row.createCell(6);
					middleNameCell3.setCellStyle(rowCellStyle);
					
					MerchantDetailsDto merchantDetailsDto = new ObjectMapper().readValue(transaction.getDetails(), MerchantDetailsDto.class);
					middleNameCell3.setCellValue(merchantDetailsDto.getSentTo());*/

				}
			}
			short roweachTablefooter = (short) (workSheet.getLastRowNum() + 1);
			HSSFRow eachTablefooter = workSheet.createRow(roweachTablefooter);

			HSSFFont rowFontTotalFooterBold = workBook.createFont();
			rowFontTotalFooterBold.setFontHeightInPoints((short) 11);
			rowFontTotalFooterBold.setFontName("Calibri");
			rowFontTotalFooterBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

			// Header Style
			HSSFCellStyle headerCellStyleTotalFooterBold = workBook.createCellStyle();
			headerCellStyleTotalFooterBold.setFont(headerFont);
			headerCellStyleTotalFooterBold.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyleTotalFooterBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyleTotalFooterBold = workBook.createCellStyle();
			rowCellStyleTotalFooterBold.setFont(rowFontTotalFooterBold);

			HSSFCell rowEachTableCell0 = eachTablefooter.createCell(0);
			rowEachTableCell0.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell1 = eachTablefooter.createCell(1);
			rowEachTableCell1.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell2 = eachTablefooter.createCell(2);
			rowEachTableCell2.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell3 = eachTablefooter.createCell(3);
			rowEachTableCell3.setCellStyle(rowCellStyleTotalFooterBold);
			short roweachTablefooterSpace = (short) (workSheet.getLastRowNum() + 1);
			HSSFRow row = workSheet.createRow(roweachTablefooterSpace);

			// For Label
			HSSFCell nationalIdCell = row.createCell(0);
			nationalIdCell.setCellStyle(rowCellStyle);

			Map<String, String> fileExportStatusMap = new HashMap<>();
			createExcelFile(workBook, workSheetName, fileExportStatusMap, response);
		}
	}

	@RequestMapping(value = "/customerTransactionReports", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void customerTransactionReports(@RequestParam(value = "reportType", required = false) String reportType,
			HttpServletRequest request, HttpServletResponse response)
			throws DocumentException, IOException 
	{
		List<TransactionModel> customerTransactionReportData = null;

		if (reportType.startsWith("transactionPdf")) {
			if ("transactionPdfDaily".equals(reportType)) {

				customerTransactionReportData = reportService.getCustomerTransactionReportForDaily();
			} else if ("transactionPdfWeekly".equals(reportType)) {
				customerTransactionReportData = reportService.getCustomerTransactionReportForWeekly();
			} else if ("transactionPdfMonthly".equals(reportType)) {
				customerTransactionReportData = reportService.getCustomerTransactionReportForMonthly();
			}

			ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
			openPdf(byteArray);

			PdfPTable table1 = new PdfPTable(1);
			table1.setWidthPercentage(100);
			table1.addCell(getCell("Customer Transaction Report", PdfPCell.ALIGN_CENTER));

			PdfPTable table = new PdfPTable(6);
			table.setWidthPercentage(100.0f);
			table.setWidths(new float[] { 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f});
			table.setSpacingBefore(10);

			// define font for table header row
			Font font = FontFactory.getFont(FontFactory.HELVETICA);
			font.setColor(BaseColor.WHITE);

			// define table header cell
			PdfPCell cell = new PdfPCell();
			cell.setBackgroundColor(BaseColor.GRAY);
			cell.setPadding(5);

			// write table header
			cell.setPhrase(new Phrase("Mobile Number", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Customer Email ID", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Transaction Date", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Transaction Type", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Reference Number", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Transaction Amount", font));
			table.addCell(cell);

			/*cell.setPhrase(new Phrase("Merchant_Detail", font));
			table.addCell(cell);*/

			// write table row data

			if (!customerTransactionReportData.isEmpty() && customerTransactionReportData != null) {

				for (TransactionModel model : customerTransactionReportData) {
					table.addCell(model.getCustomer().getMobile());
					table.addCell(model.getCustomer().getEmail());
					table.addCell(model.getCreatedDate().toString());
					table.addCell(model.getTransferType().getName());
					table.addCell(model.getKouchanReferenceNumber());
					table.addCell(String.valueOf(model.getAmount()));
					/*MerchantDetailsDto merchantDetailsDto = new ObjectMapper().readValue(model.getDetails(), MerchantDetailsDto.class);
					table.addCell(merchantDetailsDto.getSentTo());*/
				}
			}
			document.add(table1);
			document.add(table);
			document.close();

			String title = "Customer Transaction Report";
			writeAndUploadFile(byteArray, title, response);
		} else {
			if ("transactionExcelDaily".equals(reportType)) {
				customerTransactionReportData = reportService.getCustomerTransactionReportForDaily();
			} else if ("transactionExcelWeekly".equals(reportType)) {
				customerTransactionReportData = reportService.getCustomerTransactionReportForWeekly();
			} else if ("transactionExcelMonthly".equals(reportType)) {
				customerTransactionReportData = reportService.getCustomerTransactionReportForMonthly();
			}
			HSSFWorkbook workBook = new HSSFWorkbook();
			String workSheetName = "Transaction Details";
			HSSFSheet workSheet = workBook.createSheet(workSheetName);

			// Header Font
			HSSFFont headerFont = workBook.createFont();
			headerFont.setFontHeightInPoints((short) 11);
			headerFont.setFontName("Calibri");

			HSSFFont rowFont = workBook.createFont();
			rowFont.setFontHeightInPoints((short) 11);
			rowFont.setFontName("Calibri");

			// Header Style
			HSSFCellStyle headerCellStyle = workBook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyle = workBook.createCellStyle();
			rowCellStyle.setFont(rowFont);
			HSSFFont rowFontTableHeaderBold1 = workBook.createFont();
			rowFontTableHeaderBold1.setFontHeightInPoints((short) 11);
			rowFontTableHeaderBold1.setFontName("Calibri");
			rowFontTableHeaderBold1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			HSSFCellStyle rowCellStyleTotal1 = workBook.createCellStyle();
			rowCellStyleTotal1.setFont(rowFontTableHeaderBold1);
			HSSFRow row1 = workSheet.createRow(0);
			HSSFCell nationalIdCell1 = row1.createCell(0);
			nationalIdCell1.setCellStyle(rowCellStyleTotal1);

			// Header
			HSSFRow headerRow = workSheet.createRow(1);
			HSSFCell headerCell = null;
			for(int columnPosition = 0; columnPosition< 6; columnPosition++) {
				 workSheet.autoSizeColumn((short) (columnPosition));
	        }

			String[] thisIsAStringArray = { "Mobile Number", "Customer email ID", "Transaction Date", "Transaction Type",
					"Reference Number", "Transaction Amount"};
			for (int i = 0; i < thisIsAStringArray.length; i++) {
				headerCell = headerRow.createCell(i);
				headerCell.setCellValue(thisIsAStringArray[i]);
				HSSFFont rowFontTableHeaderBold = workBook.createFont();
				rowFontTableHeaderBold.setFontHeightInPoints((short) 11);
				rowFontTableHeaderBold.setFontName("Calibri");
				rowFontTableHeaderBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

				// Header Style
				HSSFCellStyle headerCellStyleTableHeader = workBook.createCellStyle();
				headerCellStyleTableHeader.setFont(headerFont);
				headerCellStyleTableHeader.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
				headerCellStyleTableHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

				// Row Style
				HSSFCellStyle rowCellStyleTotal = workBook.createCellStyle();
				rowCellStyleTotal.setFont(rowFontTableHeaderBold);
				headerCell.setCellStyle(rowCellStyleTotal);
			}
			// short rowNumber1 = (short) (workSheet.getLastRowNum() + 1);

			HSSFFont rowFontTableHeaderBold = workBook.createFont();
			rowFontTableHeaderBold.setFontHeightInPoints((short) 11);
			rowFontTableHeaderBold.setFontName("Calibri");
			rowFontTableHeaderBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

			// Header Style
			HSSFCellStyle headerCellStyleTotal = workBook.createCellStyle();
			headerCellStyleTotal.setFont(headerFont);
			headerCellStyleTotal.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyleTotal.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyleTotal = workBook.createCellStyle();
			rowCellStyleTotal.setFont(rowFontTableHeaderBold);

			HSSFCellStyle cellStyleForDate = workBook.createCellStyle();

			if (customerTransactionReportData != null && !customerTransactionReportData.isEmpty()) {

				for (TransactionModel transaction : customerTransactionReportData) {

					short rowNumber = (short) (workSheet.getLastRowNum() + 1);

					HSSFRow row = workSheet.createRow(rowNumber);

					// For Mobile No
					HSSFCell nationalIdCell = row.createCell(0);
					nationalIdCell.setCellStyle(rowCellStyle);
					nationalIdCell.setCellValue(transaction.getCustomer().getMobile());

					// For Email ID
					HSSFCell firstNameCell = row.createCell(1);
					firstNameCell.setCellStyle(rowCellStyle);
					firstNameCell.setCellValue(transaction.getCustomer().getEmail());

					// For Transaction date
					HSSFCell employeeNumberCell = row.createCell(2);
					employeeNumberCell.setCellStyle(rowCellStyle);

					CreationHelper createHelper = workBook.getCreationHelper();
					cellStyleForDate.setDataFormat(createHelper.createDataFormat().getFormat("yyyy/MM/dd"));
					if (null != transaction.getCreatedDate()) {

						employeeNumberCell.setCellValue(transaction.getCreatedDate());
						employeeNumberCell.setCellStyle(cellStyleForDate);
					}

					// For Transaction type
					HSSFCell middleNameCell = row.createCell(3);
					middleNameCell.setCellStyle(rowCellStyle);
					middleNameCell.setCellValue(transaction.getTransferType().getName());

					// For Reference number
					HSSFCell middleNameCell1 = row.createCell(4);
					middleNameCell1.setCellStyle(rowCellStyle);
					middleNameCell1.setCellValue(transaction.getKouchanReferenceNumber());
					middleNameCell1.setCellStyle(cellStyleForDate);

					// For Transaction amount
					HSSFCell middleNameCell2 = row.createCell(5);
					middleNameCell2.setCellStyle(rowCellStyle);
					middleNameCell2.setCellValue(transaction.getAmount());

					/*// For Merchant details
					HSSFCell middleNameCell3 = row.createCell(6);
					middleNameCell3.setCellStyle(rowCellStyle);
					MerchantDetailsDto merchantDetailsDto = new ObjectMapper().readValue(transaction.getDetails(), MerchantDetailsDto.class);
					middleNameCell3.setCellValue(merchantDetailsDto.getSentTo());*/

				}
			}
			short roweachTablefooter = (short) (workSheet.getLastRowNum() + 1);
			HSSFRow eachTablefooter = workSheet.createRow(roweachTablefooter);

			HSSFFont rowFontTotalFooterBold = workBook.createFont();
			rowFontTotalFooterBold.setFontHeightInPoints((short) 11);
			rowFontTotalFooterBold.setFontName("Calibri");
			rowFontTotalFooterBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

			// Header Style
			HSSFCellStyle headerCellStyleTotalFooterBold = workBook.createCellStyle();
			headerCellStyleTotalFooterBold.setFont(headerFont);
			headerCellStyleTotalFooterBold.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyleTotalFooterBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyleTotalFooterBold = workBook.createCellStyle();
			rowCellStyleTotalFooterBold.setFont(rowFontTotalFooterBold);

			HSSFCell rowEachTableCell0 = eachTablefooter.createCell(0);
			rowEachTableCell0.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell1 = eachTablefooter.createCell(1);
			rowEachTableCell1.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell2 = eachTablefooter.createCell(2);
			rowEachTableCell2.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell3 = eachTablefooter.createCell(3);
			rowEachTableCell3.setCellStyle(rowCellStyleTotalFooterBold);
			short roweachTablefooterSpace = (short) (workSheet.getLastRowNum() + 1);
			HSSFRow row = workSheet.createRow(roweachTablefooterSpace);

			// For Label
			HSSFCell nationalIdCell = row.createCell(0);
			nationalIdCell.setCellStyle(rowCellStyle);

			Map<String, String> fileExportStatusMap = new HashMap<>();
			createExcelFile(workBook, workSheetName, fileExportStatusMap, response);
		}
	}

	@RequestMapping(value = "/customerTransactionReportWithCommission", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void getCustomerTransactionReportWithCommission(
			@RequestParam(value = "reportType", required = false) String reportType, HttpServletRequest request,
			HttpServletResponse response) throws  DocumentException, IOException {
		List<TransactionModel> customerTransactionReportData = null;

		if (reportType.startsWith("transactionPdf")) {
			if ("transactionPdfDaily".equals(reportType)) {

				customerTransactionReportData = reportService.getCustomerTransactionReportForDaily();
			} else if ("transactionPdfWeekly".equals(reportType)) {
				customerTransactionReportData = reportService.getCustomerTransactionReportForWeekly();
			} else if ("transactionPdfMonthly".equals(reportType)) {
				customerTransactionReportData = reportService.getCustomerTransactionReportForMonthly();
			}

			ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
			openPdf(byteArray);

			PdfPTable table1 = new PdfPTable(1);
			table1.setWidthPercentage(100);
			table1.addCell(getCell("Customer Transaction Report With Commission", PdfPCell.ALIGN_CENTER));

			PdfPTable table = new PdfPTable(7);
			table.setWidthPercentage(100.0f);
			table.setWidths(new float[] { 3.0f, 5.0f, 3.0f, 3.0f, 2.0f, 2.0f, 2.0f });
			table.setSpacingBefore(10);

			// define font for table header row
			Font font = FontFactory.getFont(FontFactory.HELVETICA);
			font.setColor(BaseColor.WHITE);

			// define table header cell
			PdfPCell cell = new PdfPCell();
			cell.setBackgroundColor(BaseColor.GRAY);
			cell.setPadding(5);

			// write table header
			cell.setPhrase(new Phrase("Mobile Number", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Customer Email ID", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Transaction Date", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Transaction Type", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Reference Number", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Transaction Amount", font));
			table.addCell(cell);

			/*cell.setPhrase(new Phrase("Merchant_Detail", font));
			table.addCell(cell);*/

			cell.setPhrase(new Phrase("Commission", font));
			table.addCell(cell);

			// write table row data

			if (!customerTransactionReportData.isEmpty() && customerTransactionReportData != null) {

				for (TransactionModel model : customerTransactionReportData) {
					table.addCell(model.getCustomer().getMobile());
					table.addCell(model.getCustomer().getEmail());
					table.addCell(model.getCreatedDate().toString());
					table.addCell(model.getTransferType().getName());
					table.addCell(model.getKouchanReferenceNumber());
					table.addCell(String.valueOf(model.getAmount()));
					/* JSONParser parser = new JSONParser(); */

					/*
					 * JsonParser jsonParser = new JSONObject json =
					 * (JSONObject) parser.parse(stringToParse);
					 * 
					 * 
					 * // JSONObject jsonObject = new
					 * JSONObject(model.getDetails());
					 * table.addCell(jsonObject.getString("sentTo"));
					 */
					/*table.addCell(" ");*/
					table.addCell(String.valueOf(model.getCommission()));
				}
			}
			document.add(table1);
			document.add(table);
			document.close();

			String title = "Customer Transaction Report With Commission";
			writeAndUploadFile(byteArray, title, response);
		} else {
			if ("transactionExcelDaily".equals(reportType)) {
				customerTransactionReportData = reportService.getCustomerTransactionReportForDaily();
			} else if ("transactionExcelWeekly".equals(reportType)) {
				customerTransactionReportData = reportService.getCustomerTransactionReportForWeekly();
			} else if ("transactionExcelMonthly".equals(reportType)) {
				customerTransactionReportData = reportService.getCustomerTransactionReportForMonthly();
			}
			HSSFWorkbook workBook = new HSSFWorkbook();
			String workSheetName = "Transaction Details";
			HSSFSheet workSheet = workBook.createSheet(workSheetName);

			// Header Font
			HSSFFont headerFont = workBook.createFont();
			headerFont.setFontHeightInPoints((short) 11);
			headerFont.setFontName("Calibri");

			HSSFFont rowFont = workBook.createFont();
			rowFont.setFontHeightInPoints((short) 11);
			rowFont.setFontName("Calibri");

			// Header Style
			HSSFCellStyle headerCellStyle = workBook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyle = workBook.createCellStyle();
			rowCellStyle.setFont(rowFont);
			HSSFFont rowFontTableHeaderBold1 = workBook.createFont();
			rowFontTableHeaderBold1.setFontHeightInPoints((short) 11);
			rowFontTableHeaderBold1.setFontName("Calibri");
			rowFontTableHeaderBold1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			HSSFCellStyle rowCellStyleTotal1 = workBook.createCellStyle();
			rowCellStyleTotal1.setFont(rowFontTableHeaderBold1);
			HSSFRow row1 = workSheet.createRow(0);
			HSSFCell nationalIdCell1 = row1.createCell(0);
			nationalIdCell1.setCellStyle(rowCellStyleTotal1);

			// Header
			HSSFRow headerRow = workSheet.createRow(1);
			HSSFCell headerCell = null;
			for(int columnPosition = 0; columnPosition< 7; columnPosition++) {
				 workSheet.autoSizeColumn((short) (columnPosition));
	        }
			String[] thisIsAStringArray = { "Mobile Number", "Customer email ID", "Transaction Date", "Transaction Type",
					"Reference Number", "Transaction Amount","Commission" };
			for (int i = 0; i < thisIsAStringArray.length; i++) {
				headerCell = headerRow.createCell(i);
				headerCell.setCellValue(thisIsAStringArray[i]);
				HSSFFont rowFontTableHeaderBold = workBook.createFont();
				rowFontTableHeaderBold.setFontHeightInPoints((short) 11);
				rowFontTableHeaderBold.setFontName("Calibri");
				rowFontTableHeaderBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

				// Header Style
				HSSFCellStyle headerCellStyleTableHeader = workBook.createCellStyle();
				headerCellStyleTableHeader.setFont(headerFont);
				headerCellStyleTableHeader.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
				headerCellStyleTableHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

				// Row Style
				HSSFCellStyle rowCellStyleTotal = workBook.createCellStyle();
				rowCellStyleTotal.setFont(rowFontTableHeaderBold);
				headerCell.setCellStyle(rowCellStyleTotal);
			}
			// short rowNumber1 = (short) (workSheet.getLastRowNum() + 1);

			HSSFFont rowFontTableHeaderBold = workBook.createFont();
			rowFontTableHeaderBold.setFontHeightInPoints((short) 11);
			rowFontTableHeaderBold.setFontName("Calibri");
			rowFontTableHeaderBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

			// Header Style
			HSSFCellStyle headerCellStyleTotal = workBook.createCellStyle();
			headerCellStyleTotal.setFont(headerFont);
			headerCellStyleTotal.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyleTotal.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyleTotal = workBook.createCellStyle();
			rowCellStyleTotal.setFont(rowFontTableHeaderBold);

			HSSFCellStyle cellStyleForDate = workBook.createCellStyle();

			if (customerTransactionReportData != null && !customerTransactionReportData.isEmpty()) {

				for (TransactionModel transaction : customerTransactionReportData) {

					short rowNumber = (short) (workSheet.getLastRowNum() + 1);

					HSSFRow row = workSheet.createRow(rowNumber);
					for(int columnPosition = 0; columnPosition< 7; columnPosition++) {
						 workSheet.autoSizeColumn((short) (columnPosition));
			        }
					// For Mobile No
					HSSFCell nationalIdCell = row.createCell(0);
					nationalIdCell.setCellStyle(rowCellStyle);
					nationalIdCell.setCellValue(transaction.getCustomer().getMobile());

					// For Email ID
					HSSFCell firstNameCell = row.createCell(1);
					firstNameCell.setCellStyle(rowCellStyle);
					firstNameCell.setCellValue(transaction.getCustomer().getEmail());

					// For Transaction date
					HSSFCell employeeNumberCell = row.createCell(2);
					employeeNumberCell.setCellStyle(rowCellStyle);

					CreationHelper createHelper = workBook.getCreationHelper();
					cellStyleForDate.setDataFormat(createHelper.createDataFormat().getFormat("yyyy/MM/dd"));
					if (null != transaction.getCreatedDate()) {

						employeeNumberCell.setCellValue(transaction.getCreatedDate());
						employeeNumberCell.setCellStyle(cellStyleForDate);
					}

					// For Transaction type
					HSSFCell middleNameCell = row.createCell(3);
					middleNameCell.setCellStyle(rowCellStyle);
					middleNameCell.setCellValue(transaction.getTransferType().getName());

					// For Reference number
					HSSFCell middleNameCell1 = row.createCell(4);
					middleNameCell1.setCellStyle(rowCellStyle);
					middleNameCell1.setCellValue(transaction.getKouchanReferenceNumber());
					middleNameCell1.setCellStyle(cellStyleForDate);

					// For Transaction amount
					HSSFCell middleNameCell2 = row.createCell(5);
					middleNameCell2.setCellStyle(rowCellStyle);
					middleNameCell2.setCellValue(transaction.getAmount());

				/*	// For Merchant details
					HSSFCell middleNameCell3 = row.createCell(6);
					middleNameCell3.setCellStyle(rowCellStyle);
					middleNameCell3.setCellValue(" ");*/

					// For Commission
					HSSFCell middleNameCell4 = row.createCell(6);
					middleNameCell4.setCellStyle(rowCellStyle);
					middleNameCell4.setCellValue(transaction.getCommission());

				}
			}
			short roweachTablefooter = (short) (workSheet.getLastRowNum() + 1);
			HSSFRow eachTablefooter = workSheet.createRow(roweachTablefooter);

			HSSFFont rowFontTotalFooterBold = workBook.createFont();
			rowFontTotalFooterBold.setFontHeightInPoints((short) 11);
			rowFontTotalFooterBold.setFontName("Calibri");
			rowFontTotalFooterBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

			// Header Style
			HSSFCellStyle headerCellStyleTotalFooterBold = workBook.createCellStyle();
			headerCellStyleTotalFooterBold.setFont(headerFont);
			headerCellStyleTotalFooterBold.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyleTotalFooterBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyleTotalFooterBold = workBook.createCellStyle();
			rowCellStyleTotalFooterBold.setFont(rowFontTotalFooterBold);

			HSSFCell rowEachTableCell0 = eachTablefooter.createCell(0);
			rowEachTableCell0.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell1 = eachTablefooter.createCell(1);
			rowEachTableCell1.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell2 = eachTablefooter.createCell(2);
			rowEachTableCell2.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell3 = eachTablefooter.createCell(3);
			rowEachTableCell3.setCellStyle(rowCellStyleTotalFooterBold);
			short roweachTablefooterSpace = (short) (workSheet.getLastRowNum() + 1);
			HSSFRow row = workSheet.createRow(roweachTablefooterSpace);

			// For Label
			HSSFCell nationalIdCell = row.createCell(0);
			nationalIdCell.setCellStyle(rowCellStyle);

			Map<String, String> fileExportStatusMap = new HashMap<>();
			createExcelFile(workBook, workSheetName, fileExportStatusMap, response);
		}
	}

	@RequestMapping(value = "/creditAndDebitTransactions", method = RequestMethod.GET)
	public void customerDebitAndCredittransactions(
			@RequestParam(value = "reportType", required = false) String reportType, HttpServletRequest request,
			HttpServletResponse response) throws ParseException, DocumentException, IOException {
		String dateaAsString = "2017/12/01";
		Date date = new SimpleDateFormat("yyyy/MM/dd").parse(dateaAsString);
		// System.out.println(sDate1+"\t"+date1);
		List<CustomerTransactionCreditAndDebitDto> customerTransaction = reportService.getCustomerTransactionReportForDaily(date);

		if ("creditAndDebitTransactionPdf".equals(reportType)) {

			System.out.println("sorted list " + customerTransaction);

			/* Map<Long, String> map = new HashMap<>(); */

			List<CustomerTransactionCreditAndDebitDto> customersList = arrangedCustomers(customerTransaction);

			ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
			openPdf(byteArray);

			PdfPTable table1 = new PdfPTable(1);
			table1.setWidthPercentage(100);
			table1.addCell(getCell("Customer Transaction", PdfPCell.ALIGN_CENTER));

			PdfPTable table = new PdfPTable(6);
			table.setWidthPercentage(100.0f);
			table.setWidths(new float[] { 3.0f, 5.0f, 5.0f, 4.0f, 3.0f, 3.0f });
			table.setSpacingBefore(10);

			// define font for table header row
			Font font = FontFactory.getFont(FontFactory.HELVETICA);
			font.setColor(BaseColor.WHITE);

			// define table header cell
			PdfPCell cell = new PdfPCell();
			cell.setBackgroundColor(BaseColor.GRAY);
			cell.setPadding(5);

			// write table header
			cell.setPhrase(new Phrase("Date of Creation", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Mobile Number", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Email", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Transaction type ", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Transaction Count", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Total Amount", font));
			table.addCell(cell);

			// write table row data

			if (!customersList.isEmpty() && customersList != null) {
				for (CustomerTransactionCreditAndDebitDto customer : customersList) {
					table.addCell(customer.getCreatedDate().toString());
					table.addCell(customer.getMobileNumber());
					table.addCell(customer.getEmailId());
					table.addCell(customer.getTransactionName());
					table.addCell(String.valueOf(customer.getTransactionCount()));
					table.addCell(String.valueOf(customer.getTransactionAmount()));
				}
			}
			document.add(table1);
			document.add(table);
			document.close();

			String title = "Customer Transaction Amount";
			writeAndUploadFile(byteArray, title, response);
		} else {

			HSSFWorkbook workBook = new HSSFWorkbook();
			String workSheetName = "Transaction Details";
			HSSFSheet workSheet = workBook.createSheet(workSheetName);
			HSSFFont headerFont = workBook.createFont();
			headerFont.setFontHeightInPoints((short) 11);
			headerFont.setFontName("Calibri");

			HSSFFont rowFont = workBook.createFont();
			rowFont.setFontHeightInPoints((short) 11);
			rowFont.setFontName("Calibri");

			// Header Style
			HSSFCellStyle headerCellStyle = workBook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyle = workBook.createCellStyle();
			rowCellStyle.setFont(rowFont);
			HSSFFont rowFontTableHeaderBold1 = workBook.createFont();
			rowFontTableHeaderBold1.setFontHeightInPoints((short) 11);
			rowFontTableHeaderBold1.setFontName("Calibri");
			rowFontTableHeaderBold1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			HSSFCellStyle rowCellStyleTotal1 = workBook.createCellStyle();
			rowCellStyleTotal1.setFont(rowFontTableHeaderBold1);
			HSSFRow row1 = workSheet.createRow(0);
			HSSFCell nationalIdCell1 = row1.createCell(0);
			nationalIdCell1.setCellStyle(rowCellStyleTotal1);

			// Header
			HSSFRow headerRow = workSheet.createRow(1);
			HSSFCell headerCell = null;
			for(int columnPosition = 0; columnPosition< 6; columnPosition++) {
				 workSheet.autoSizeColumn((short) (columnPosition));
	        }
			String[] thisIsAStringArray = { "Created Date", "Mobile Number", "Email", "Transaction Type",
					"Transaction Count", "Total Amount" };
			for (int i = 0; i < thisIsAStringArray.length; i++) {
				headerCell = headerRow.createCell(i);
				headerCell.setCellValue(thisIsAStringArray[i]);
				HSSFFont rowFontTableHeaderBold = workBook.createFont();
				rowFontTableHeaderBold.setFontHeightInPoints((short) 11);
				rowFontTableHeaderBold.setFontName("Calibri");
				rowFontTableHeaderBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

				// Header Style
				HSSFCellStyle headerCellStyleTableHeader = workBook.createCellStyle();
				headerCellStyleTableHeader.setFont(headerFont);
				headerCellStyleTableHeader.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
				headerCellStyleTableHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

				// Row Style
				HSSFCellStyle rowCellStyleTotal = workBook.createCellStyle();
				rowCellStyleTotal.setFont(rowFontTableHeaderBold);
				headerCell.setCellStyle(rowCellStyleTotal);
			}
			// short rowNumber1 = (short) (workSheet.getLastRowNum() + 1);

			HSSFFont rowFontTableHeaderBold = workBook.createFont();
			rowFontTableHeaderBold.setFontHeightInPoints((short) 11);
			rowFontTableHeaderBold.setFontName("Calibri");
			rowFontTableHeaderBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

			// Header Style
			HSSFCellStyle headerCellStyleTotal = workBook.createCellStyle();
			headerCellStyleTotal.setFont(headerFont);
			headerCellStyleTotal.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyleTotal.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyleTotal = workBook.createCellStyle();
			rowCellStyleTotal.setFont(rowFontTableHeaderBold);

			HSSFCellStyle cellStyleForDate = workBook.createCellStyle();

			List<CustomerTransactionCreditAndDebitDto> customersList = arrangedCustomers(customerTransaction);
			if (customersList != null && !customersList.isEmpty()) {
				// Header Font

				for (CustomerTransactionCreditAndDebitDto customer : customersList) {

					short rowNumber = (short) (workSheet.getLastRowNum() + 1);

					HSSFRow row = workSheet.createRow(rowNumber);
					for(int columnPosition = 0; columnPosition< 6; columnPosition++) {
						 workSheet.autoSizeColumn((short) (columnPosition));
			        }
					// For Transaction date
					HSSFCell employeeNumberCell = row.createCell(0);
					employeeNumberCell.setCellStyle(rowCellStyle);

					CreationHelper createHelper = workBook.getCreationHelper();
					cellStyleForDate.setDataFormat(createHelper.createDataFormat().getFormat("yyyy/MM/dd"));
					if (null != customer.getCreatedDate()) {

						employeeNumberCell.setCellValue(customer.getCreatedDate());
						employeeNumberCell.setCellStyle(cellStyleForDate);
					}

					// For Mobile No
					HSSFCell nationalIdCell = row.createCell(1);
					nationalIdCell.setCellStyle(rowCellStyle);
					nationalIdCell.setCellValue(customer.getMobileNumber());

					// For Email ID
					HSSFCell firstNameCell = row.createCell(2);
					firstNameCell.setCellStyle(rowCellStyle);
					firstNameCell.setCellValue(customer.getEmailId());

					// For Transaction type
					HSSFCell middleNameCell = row.createCell(3);
					middleNameCell.setCellStyle(rowCellStyle);
					middleNameCell.setCellValue(customer.getTransactionName());

					// For Transaction Count
					HSSFCell middleNameCell1 = row.createCell(4);
					middleNameCell1.setCellStyle(rowCellStyle);
					middleNameCell1.setCellValue(customer.getTransactionCount());

					// For Total amount
					HSSFCell middleNameCell2 = row.createCell(5);
					middleNameCell2.setCellStyle(rowCellStyle);
					middleNameCell2.setCellValue(customer.getTransactionAmount());
				}

			}
			short roweachTablefooter = (short) (workSheet.getLastRowNum() + 1);
			HSSFRow eachTablefooter = workSheet.createRow(roweachTablefooter);

			HSSFFont rowFontTotalFooterBold = workBook.createFont();
			rowFontTotalFooterBold.setFontHeightInPoints((short) 11);
			rowFontTotalFooterBold.setFontName("Calibri");
			rowFontTotalFooterBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

			// Header Style
			HSSFCellStyle headerCellStyleTotalFooterBold = workBook.createCellStyle();
			headerCellStyleTotalFooterBold.setFont(headerFont);
			headerCellStyleTotalFooterBold.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyleTotalFooterBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyleTotalFooterBold = workBook.createCellStyle();
			rowCellStyleTotalFooterBold.setFont(rowFontTotalFooterBold);

			HSSFCell rowEachTableCell0 = eachTablefooter.createCell(0);
			rowEachTableCell0.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell1 = eachTablefooter.createCell(1);
			rowEachTableCell1.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell2 = eachTablefooter.createCell(2);
			rowEachTableCell2.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell3 = eachTablefooter.createCell(3);
			rowEachTableCell3.setCellStyle(rowCellStyleTotalFooterBold);
			short roweachTablefooterSpace = (short) (workSheet.getLastRowNum() + 1);
			HSSFRow row = workSheet.createRow(roweachTablefooterSpace);

			// For Label
			HSSFCell nationalIdCell = row.createCell(0);
			nationalIdCell.setCellStyle(rowCellStyle);

			Map<String, String> fileExportStatusMap = new HashMap<>();
			createExcelFile(workBook, workSheetName, fileExportStatusMap, response);
		}
	}

	@RequestMapping(value = "/walletBalanceReport", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	void walletBalanceReport(@RequestParam(value = "reportType", required = false) String reportType,
			HttpServletRequest request, HttpServletResponse response) throws DocumentException, IOException {

		List<CustomerModel> walletBalanceReportData = reportService.getWalletBalanceReportData();
		if (reportType.equals("walletBalancePdf")) {

			ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
			openPdf(byteArray);

			PdfPTable table1 = new PdfPTable(1);
			table1.setWidthPercentage(100);
			table1.addCell(getCell("Customer Wallet Balance", PdfPCell.ALIGN_CENTER));

			PdfPTable table = new PdfPTable(6);
			table.setWidthPercentage(100.0f);
			table.setWidths(new float[] { 3.0f, 5.0f, 5.0f, 4.0f, 3.0f, 3.0f });
			table.setSpacingBefore(10);

			// define font for table header row
			Font font = FontFactory.getFont(FontFactory.HELVETICA);
			font.setColor(BaseColor.WHITE);

			// define table header cell
			PdfPCell cell = new PdfPCell();
			cell.setBackgroundColor(BaseColor.GRAY);
			cell.setPadding(5);

			// write table header
			cell.setPhrase(new Phrase("Mobile Number", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Customer Name ", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Email", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Balance On wallet (EOD Balance)", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Date of Creation", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Status", font));
			table.addCell(cell);

			// write table row data

			if (!walletBalanceReportData.isEmpty() && walletBalanceReportData != null) {

				for (CustomerModel customer : walletBalanceReportData) {
					table.addCell(customer.getMobile());
					table.addCell(customer.getName());
					table.addCell(customer.getEmail());
					table.addCell(String.valueOf(customer.getWalletBalance().getBalance()));
					table.addCell(customer.getCreatedDate().toString());
					table.addCell(String.valueOf(customer.getCustomerStatus().getName()));
				}
			}
			document.add(table1);
			document.add(table);
			document.close();
			writeAndUploadFile(byteArray, "Wallet Balance", response);

			String title = "Wallet Balance";
			writeAndUploadFile(byteArray, title, response);
		} else {

			HSSFWorkbook workBook = new HSSFWorkbook();
			String workSheetName = "Wallet Balance";
			HSSFSheet workSheet = workBook.createSheet(workSheetName);

			// Header Font
			HSSFFont headerFont = workBook.createFont();
			headerFont.setFontHeightInPoints((short) 11);
			headerFont.setFontName("Calibri");

			HSSFFont rowFont = workBook.createFont();
			rowFont.setFontHeightInPoints((short) 11);
			rowFont.setFontName("Calibri");

			// Header Style
			HSSFCellStyle headerCellStyle = workBook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyle = workBook.createCellStyle();
			rowCellStyle.setFont(rowFont);
			HSSFFont rowFontTableHeaderBold1 = workBook.createFont();
			rowFontTableHeaderBold1.setFontHeightInPoints((short) 11);
			rowFontTableHeaderBold1.setFontName("Calibri");
			rowFontTableHeaderBold1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			HSSFCellStyle rowCellStyleTotal1 = workBook.createCellStyle();
			rowCellStyleTotal1.setFont(rowFontTableHeaderBold1);
			HSSFRow row1 = workSheet.createRow(0);
			HSSFCell nationalIdCell1 = row1.createCell(0);
			nationalIdCell1.setCellStyle(rowCellStyleTotal1);

			// Header
			HSSFRow headerRow = workSheet.createRow(1);
			HSSFCell headerCell = null;
			for(int columnPosition = 0; columnPosition< 6; columnPosition++) {
				 workSheet.autoSizeColumn((short) (columnPosition));
	        }
			String[] thisIsAStringArray = { "Mobile Number", "Customer Name", "Customer Email ID",
					"Balance On wallet (EOD Balance)", "Date of Creation", "Status" };
			for (int i = 0; i < thisIsAStringArray.length; i++) {
				headerCell = headerRow.createCell(i);
				headerCell.setCellValue(thisIsAStringArray[i]);
				HSSFFont rowFontTableHeaderBold = workBook.createFont();
				rowFontTableHeaderBold.setFontHeightInPoints((short) 11);
				rowFontTableHeaderBold.setFontName("Calibri");
				rowFontTableHeaderBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

				// Header Style
				HSSFCellStyle headerCellStyleTableHeader = workBook.createCellStyle();
				headerCellStyleTableHeader.setFont(headerFont);
				headerCellStyleTableHeader.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
				headerCellStyleTableHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

				// Row Style
				HSSFCellStyle rowCellStyleTotal = workBook.createCellStyle();
				rowCellStyleTotal.setFont(rowFontTableHeaderBold);
				headerCell.setCellStyle(rowCellStyleTotal);
			}
			// short rowNumber1 = (short) (workSheet.getLastRowNum() + 1);

			HSSFFont rowFontTableHeaderBold = workBook.createFont();
			rowFontTableHeaderBold.setFontHeightInPoints((short) 11);
			rowFontTableHeaderBold.setFontName("Calibri");
			rowFontTableHeaderBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

			// Header Style
			HSSFCellStyle headerCellStyleTotal = workBook.createCellStyle();
			headerCellStyleTotal.setFont(headerFont);
			headerCellStyleTotal.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyleTotal.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyleTotal = workBook.createCellStyle();
			rowCellStyleTotal.setFont(rowFontTableHeaderBold);

			HSSFCellStyle cellStyleForDate = workBook.createCellStyle();

			if (walletBalanceReportData != null && !walletBalanceReportData.isEmpty()) {

				for (CustomerModel customer : walletBalanceReportData) {

					short rowNumber = (short) (workSheet.getLastRowNum() + 1);

					HSSFRow row = workSheet.createRow(rowNumber);
					for(int columnPosition = 0; columnPosition< 6; columnPosition++) {
						 workSheet.autoSizeColumn((short) (columnPosition));
			        }
					// For Mobile No
					HSSFCell mobileCell = row.createCell(0);
					mobileCell.setCellStyle(rowCellStyle);
					mobileCell.setCellValue(customer.getMobile());

					// For Name
					HSSFCell customerNameCell = row.createCell(1);
					customerNameCell.setCellStyle(rowCellStyle);
					customerNameCell.setCellValue(customer.getName());

					// For Email ID
					HSSFCell emailIdCell = row.createCell(2);
					emailIdCell.setCellStyle(rowCellStyle);
					emailIdCell.setCellValue(customer.getEmail());

					// For Wallet Balance
					HSSFCell walletBalanceCell = row.createCell(3);
					walletBalanceCell.setCellStyle(rowCellStyle);
					// double balance =
					// attachment.getWalletBalance().getBalance();
					walletBalanceCell.setCellValue(customer.getWalletBalance().getBalance());

					// For Created Date
					HSSFCell createdDateCell = row.createCell(4);
					createdDateCell.setCellStyle(rowCellStyle);
					CreationHelper createHelper = workBook.getCreationHelper();
					cellStyleForDate.setDataFormat(createHelper.createDataFormat().getFormat("yyyy/MM/dd"));
					createdDateCell.setCellValue(customer.getCreatedDate());
					createdDateCell.setCellStyle(cellStyleForDate);

					// For Status
					HSSFCell statusCell = row.createCell(5);
					statusCell.setCellStyle(rowCellStyle);
					statusCell.setCellValue(customer.getCustomerStatus().getName());

				}
			}
			short roweachTablefooter = (short) (workSheet.getLastRowNum() + 1);
			HSSFRow eachTablefooter = workSheet.createRow(roweachTablefooter);

			HSSFFont rowFontTotalFooterBold = workBook.createFont();
			rowFontTotalFooterBold.setFontHeightInPoints((short) 11);
			rowFontTotalFooterBold.setFontName("Calibri");
			rowFontTotalFooterBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

			// Header Style
			HSSFCellStyle headerCellStyleTotalFooterBold = workBook.createCellStyle();
			headerCellStyleTotalFooterBold.setFont(headerFont);
			headerCellStyleTotalFooterBold.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyleTotalFooterBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyleTotalFooterBold = workBook.createCellStyle();
			rowCellStyleTotalFooterBold.setFont(rowFontTotalFooterBold);

			HSSFCell rowEachTableCell0 = eachTablefooter.createCell(0);
			rowEachTableCell0.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell1 = eachTablefooter.createCell(1);
			rowEachTableCell1.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell2 = eachTablefooter.createCell(2);
			rowEachTableCell2.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell3 = eachTablefooter.createCell(3);
			rowEachTableCell3.setCellStyle(rowCellStyleTotalFooterBold);
			short roweachTablefooterSpace = (short) (workSheet.getLastRowNum() + 1);
			HSSFRow row = workSheet.createRow(roweachTablefooterSpace);

			// For Label
			HSSFCell nationalIdCell = row.createCell(0);
			nationalIdCell.setCellStyle(rowCellStyle);

			Map<String, String> fileExportStatusMap = new HashMap<>();
			createExcelFile(workBook, workSheetName, fileExportStatusMap, response);

		}

	}

	
	@RequestMapping(value = "/walletBalanceReports", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	void walletBalanceReports(@RequestParam(value = "reportType", required = false) String reportType,
			HttpServletRequest request, HttpServletResponse response) throws DocumentException, IOException {

		List<CustomerModel> walletBalanceReportData = null;
		if (reportType.startsWith("walletBalancePdf")) {
			if ("walletBalancePdfDaily".equals(reportType)) {

				walletBalanceReportData = reportService.getWalletBalanceReportForDaily();
			} else if ("walletBalancePdfWeekly".equals(reportType)) {
				walletBalanceReportData = reportService.getWalletBalanceReportForWeekly();
			} else if ("walletBalancePdfMonthly".equals(reportType)) {
				walletBalanceReportData = reportService.getWalletBalanceReportForMonthly();
			}

			System.out.println("wallet balance list " + walletBalanceReportData);

			ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
			openPdf(byteArray);

			PdfPTable table1 = new PdfPTable(1);
			table1.setWidthPercentage(100);
			table1.addCell(getCell("Customer Wallet Balance", PdfPCell.ALIGN_CENTER));

			PdfPTable table = new PdfPTable(6);
			table.setWidthPercentage(100.0f);
			table.setWidths(new float[] { 3.0f, 5.0f, 5.0f, 4.0f, 3.0f, 3.0f });
			table.setSpacingBefore(10);

			// define font for table header row
			Font font = FontFactory.getFont(FontFactory.HELVETICA);
			font.setColor(BaseColor.WHITE);

			// define table header cell
			PdfPCell cell = new PdfPCell();
			cell.setBackgroundColor(BaseColor.GRAY);
			cell.setPadding(5);

			// write table header
			cell.setPhrase(new Phrase("Mobile Number", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Customer Name ", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Email", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Balance On wallet (EOD Balance)", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Date of Creation", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Status", font));
			table.addCell(cell);

			// write table row data

			if (!walletBalanceReportData.isEmpty() && walletBalanceReportData != null) {

				for (CustomerModel customer : walletBalanceReportData) {
					table.addCell(customer.getMobile());
					table.addCell(customer.getName());
					table.addCell(customer.getEmail());
					table.addCell(String.valueOf(customer.getWalletBalance().getBalance()));
					table.addCell(customer.getCreatedDate().toString());
					table.addCell(String.valueOf(customer.getCustomerStatus().getName()));
				}
			}
			document.add(table1);
			document.add(table);
			document.close();
			writeAndUploadFile(byteArray, "Wallet Balance", response);

			String title = "Wallet Balance";
			writeAndUploadFile(byteArray, title, response);
		} else {

			if ("walletBalanceExcelDaily".equals(reportType)) {
				walletBalanceReportData = reportService.getWalletBalanceReportForDaily();
			} else if ("walletBalanceExcelWeekly".equals(reportType)) {
				walletBalanceReportData = reportService.getWalletBalanceReportForWeekly();
			} else if ("walletBalanceExcelMonthly".equals(reportType)) {
				walletBalanceReportData = reportService.getWalletBalanceReportForMonthly();
			}

			HSSFWorkbook workBook = new HSSFWorkbook();
			String workSheetName = "Wallet Balance";
			HSSFSheet workSheet = workBook.createSheet(workSheetName);

			// Header Font
			HSSFFont headerFont = workBook.createFont();
			headerFont.setFontHeightInPoints((short) 11);
			headerFont.setFontName("Calibri");

			HSSFFont rowFont = workBook.createFont();
			rowFont.setFontHeightInPoints((short) 11);
			rowFont.setFontName("Calibri");

			// Header Style
			HSSFCellStyle headerCellStyle = workBook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyle = workBook.createCellStyle();
			rowCellStyle.setFont(rowFont);
			HSSFFont rowFontTableHeaderBold1 = workBook.createFont();
			rowFontTableHeaderBold1.setFontHeightInPoints((short) 11);
			rowFontTableHeaderBold1.setFontName("Calibri");
			rowFontTableHeaderBold1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			HSSFCellStyle rowCellStyleTotal1 = workBook.createCellStyle();
			rowCellStyleTotal1.setFont(rowFontTableHeaderBold1);
			HSSFRow row1 = workSheet.createRow(0);
			HSSFCell nationalIdCell1 = row1.createCell(0);
			nationalIdCell1.setCellStyle(rowCellStyleTotal1);

			// Header
			HSSFRow headerRow = workSheet.createRow(1);
			HSSFCell headerCell = null;
			/*
			 * String[] thisIsAStringArray = { "Mobile Number", "Customer Name",
			 * "Customer Email ID", "Balance On wallet (EOD Balance)",
			 * "Date of Expiry", "Date of Creation", "Status" };
			 */
			for(int columnPosition = 0; columnPosition< 6; columnPosition++) {
				 workSheet.autoSizeColumn((short) (columnPosition));
	        }
			// Removed Date of Expiry
			String[] thisIsAStringArray = { "Mobile Number", "Customer Name", "Customer Email ID",
					"Balance On wallet (EOD Balance)", "Date of Creation", "Status" };
			for (int i = 0; i < thisIsAStringArray.length; i++) {
				headerCell = headerRow.createCell(i);
				headerCell.setCellValue(thisIsAStringArray[i]);
				HSSFFont rowFontTableHeaderBold = workBook.createFont();
				rowFontTableHeaderBold.setFontHeightInPoints((short) 11);
				rowFontTableHeaderBold.setFontName("Calibri");
				rowFontTableHeaderBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

				// Header Style
				HSSFCellStyle headerCellStyleTableHeader = workBook.createCellStyle();
				headerCellStyleTableHeader.setFont(headerFont);
				headerCellStyleTableHeader.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
				headerCellStyleTableHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

				// Row Style
				HSSFCellStyle rowCellStyleTotal = workBook.createCellStyle();
				rowCellStyleTotal.setFont(rowFontTableHeaderBold);
				headerCell.setCellStyle(rowCellStyleTotal);
			}
			// short rowNumber1 = (short) (workSheet.getLastRowNum() + 1);

			HSSFFont rowFontTableHeaderBold = workBook.createFont();
			rowFontTableHeaderBold.setFontHeightInPoints((short) 11);
			rowFontTableHeaderBold.setFontName("Calibri");
			rowFontTableHeaderBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

			// Header Style
			HSSFCellStyle headerCellStyleTotal = workBook.createCellStyle();
			headerCellStyleTotal.setFont(headerFont);
			headerCellStyleTotal.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyleTotal.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyleTotal = workBook.createCellStyle();
			rowCellStyleTotal.setFont(rowFontTableHeaderBold);

			HSSFCellStyle cellStyleForDate = workBook.createCellStyle();

			if (walletBalanceReportData != null && !walletBalanceReportData.isEmpty()) {
				for (CustomerModel customer : walletBalanceReportData) {

					short rowNumber = (short) (workSheet.getLastRowNum() + 1);

					HSSFRow row = workSheet.createRow(rowNumber);
					for(int columnPosition = 0; columnPosition< 6; columnPosition++) {
						 workSheet.autoSizeColumn((short) (columnPosition));
			        }
					// For Mobile No
					HSSFCell mobileCell = row.createCell(0);
					mobileCell.setCellStyle(rowCellStyle);
					mobileCell.setCellValue(customer.getMobile());

					// For Name
					HSSFCell customerNameCell = row.createCell(1);
					customerNameCell.setCellStyle(rowCellStyle);
					customerNameCell.setCellValue(customer.getName());

					// For Email ID
					HSSFCell emailIdCell = row.createCell(2);
					emailIdCell.setCellStyle(rowCellStyle);
					emailIdCell.setCellValue(customer.getEmail());

					// For Wallet Balance
					HSSFCell walletBalanceCell = row.createCell(3);
					walletBalanceCell.setCellStyle(rowCellStyle);
					// double balance =
					// attachment.getWalletBalance().getBalance();
					walletBalanceCell.setCellValue(customer.getWalletBalance().getBalance());

					// For Created Date
					HSSFCell createdDateCell = row.createCell(4);
					createdDateCell.setCellStyle(rowCellStyle);
					CreationHelper createHelper = workBook.getCreationHelper();
					cellStyleForDate.setDataFormat(createHelper.createDataFormat().getFormat("yyyy/MM/dd"));
					createdDateCell.setCellValue(customer.getCreatedDate());
					createdDateCell.setCellStyle(cellStyleForDate);

					// For Status
					HSSFCell statusCell = row.createCell(5);
					statusCell.setCellStyle(rowCellStyle);
					statusCell.setCellValue(customer.getCustomerStatus().getName());

				}
			}
			short roweachTablefooter = (short) (workSheet.getLastRowNum() + 1);
			HSSFRow eachTablefooter = workSheet.createRow(roweachTablefooter);

			HSSFFont rowFontTotalFooterBold = workBook.createFont();
			rowFontTotalFooterBold.setFontHeightInPoints((short) 11);
			rowFontTotalFooterBold.setFontName("Calibri");
			rowFontTotalFooterBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

			// Header Style
			HSSFCellStyle headerCellStyleTotalFooterBold = workBook.createCellStyle();
			headerCellStyleTotalFooterBold.setFont(headerFont);
			headerCellStyleTotalFooterBold.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyleTotalFooterBold.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

			// Row Style
			HSSFCellStyle rowCellStyleTotalFooterBold = workBook.createCellStyle();
			rowCellStyleTotalFooterBold.setFont(rowFontTotalFooterBold);

			HSSFCell rowEachTableCell0 = eachTablefooter.createCell(0);
			rowEachTableCell0.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell1 = eachTablefooter.createCell(1);
			rowEachTableCell1.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell2 = eachTablefooter.createCell(2);
			rowEachTableCell2.setCellStyle(rowCellStyleTotalFooterBold);

			HSSFCell rowEachTableCell3 = eachTablefooter.createCell(3);
			rowEachTableCell3.setCellStyle(rowCellStyleTotalFooterBold);
			short roweachTablefooterSpace = (short) (workSheet.getLastRowNum() + 1);
			HSSFRow row = workSheet.createRow(roweachTablefooterSpace);

			// For Label
			HSSFCell nationalIdCell = row.createCell(0);
			nationalIdCell.setCellStyle(rowCellStyle);

			Map<String, String> fileExportStatusMap = new HashMap<>();
			createExcelFile(workBook, workSheetName, fileExportStatusMap, response);

		}

	}

	
	@RequestMapping(value = "/walletMISReportData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void getWalletMISReportData() {
		List<WalletBalanceModel> walletMISReportData = reportService.getWalletMISReportData();
	}

	private List<CustomerTransactionCreditAndDebitDto> arrangedCustomers(
			List<CustomerTransactionCreditAndDebitDto> customerTransaction) {
		List<CustomerTransactionCreditAndDebitDto> customerList = new ArrayList<>();
		Set<Long> ids = new TreeSet<>();
		for (CustomerTransactionCreditAndDebitDto creditAndDebit : customerTransaction) {
			ids.add(creditAndDebit.getCustomerId());
		}

		for (Long id : ids) {
			Double debitTotalAmount = 0.0;
			Double creditTotalAmount = 0.0;
			int debitTransactionCount = 0;
			int creditTransactionCount = 0;
			CustomerTransactionCreditAndDebitDto debitCustomer = new CustomerTransactionCreditAndDebitDto();
			CustomerTransactionCreditAndDebitDto creditCustomer = new CustomerTransactionCreditAndDebitDto();

			for (CustomerTransactionCreditAndDebitDto creditAndDebit : customerTransaction) {
				if (id == creditAndDebit.getCustomerId()) {
					debitCustomer.setMobileNumber(creditAndDebit.getMobileNumber());
					debitCustomer.setEmailId(creditAndDebit.getEmailId());
					debitCustomer.setCreatedDate(creditAndDebit.getCreatedDate());
					creditCustomer.setMobileNumber(creditAndDebit.getMobileNumber());
					creditCustomer.setEmailId(creditAndDebit.getEmailId());
					creditCustomer.setCreatedDate(creditAndDebit.getCreatedDate());

					if ("debit".equalsIgnoreCase(creditAndDebit.getTransactionName())) {
						debitTotalAmount = debitTotalAmount + creditAndDebit.getTransactionAmount();
						debitTransactionCount++;
						debitCustomer.setTransactionName(creditAndDebit.getTransactionName());
					}
					if ("credit".equalsIgnoreCase(creditAndDebit.getTransactionName())) {
						creditTotalAmount = creditTotalAmount + creditAndDebit.getTransactionAmount();
						creditTransactionCount++;
						creditCustomer.setTransactionName(creditAndDebit.getTransactionName());
					}
				}
			}
			if (null != debitCustomer.getTransactionName()) {
				debitCustomer.setTransactionCount(debitTransactionCount);
				debitCustomer.setTransactionAmount(debitTotalAmount);
				customerList.add(debitCustomer);
			}
			if (null != creditCustomer.getTransactionName()) {
				creditCustomer.setTransactionCount(creditTransactionCount);
				creditCustomer.setTransactionAmount(creditTotalAmount);
				customerList.add(creditCustomer);
			}

		}

		return customerList;
	}

	public void writeAndUploadFile(ByteArrayOutputStream byteOutStream, String title, HttpServletResponse response)
			throws IOException {
		FileOutputStream outStream = null;
		try {
			String folderName = null;
			String fileName = null;
			if (title != null) {
				folderName = createFolder();
				response.setContentType("application/pdf");
				fileName = String.format("%s/%s.pdf", folderName, title);
				outStream = new FileOutputStream(fileName);
				byteOutStream.writeTo(outStream);
				outStream.close();
				byteOutStream.close();

			} else {
				folderName = createFolder();
				String reportName = "claimBytype";
				folderName = createFolder();
				response.setContentType("application/pdf");
				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", reportName);
				response.setHeader(headerKey, headerValue);
				fileName = String.format("%s/%s.pdf", folderName, "test");
				outStream = new FileOutputStream(fileName);

				byteOutStream.writeTo(outStream);

			}
			writeFileAsStream(response, fileName, folderName);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			outStream.close();
		}
	}

	public String createFolder() throws Exception {
		String folderName = String.format("%s/%s", getRepoFolder(), "DOCSEXPORT");
		File file = new File(folderName);
		if (!file.isDirectory()) {
			boolean folderCreated = file.mkdir();
			if (!folderCreated) {
				throw new Exception(String.format("%s - Folder creation failed", folderName));
			}
		}
		return folderName;
	}

	protected String getRepoFolder() {

		// return "C://template";// Resource.getResAppStr("RECOVERY_REPO");
		return "C:/";
	}

	public void writeFileAsStream(HttpServletResponse response, String fileName, String folderName) {
		OutputStream out = null;
		InputStream in = null;
		try {
			if (fileName.contains(".pdf")) {
				out = response.getOutputStream();
				if (fileName != null && !fileName.isEmpty()) {
					File file = new File(fileName);
					in = new FileInputStream(file);

					response.setHeader("Content-Disposition",
							"attachment;filename=\"" + ESAPI.encoder().encodeForURL(file.getName()) + "\"");
					response.setContentType(ESAPI.encoder().encodeForURL("application/pdf"));
					response.setContentLength((int) file.length());
					writeToOutputStream(in, out);

					/*
					 * response.reset();
					 * response.setContentType("application/pdf"); String
					 * disposition = "attachment; fileName=" + file.getName();
					 * response.setHeader("Content-Disposition", disposition);
					 * response.setContentLength((int) file.length());
					 * writeToOutputStream(in, out);
					 */
					// response.reset();

				}
			} else {
				out = response.getOutputStream();
				if (fileName != null && !fileName.isEmpty()) {
					File file = new File(fileName);
					in = new FileInputStream(file);
					response.setHeader("Content-Disposition",
							"attachment;filename=\"" + ESAPI.encoder().encodeForURL(file.getName()) + "\"");
					response.setContentType(ESAPI.encoder().encodeForURL("application/xls"));
					response.setContentLength((int) file.length());
					writeToOutputStream(in, out);
					// response.reset();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				FileUtils.deleteDirectory(folderName);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void writeToOutputStream(InputStream inputStream, OutputStream out) throws IOException {
		byte[] buf = new byte[1024];
		int count = 0;
		while ((count = inputStream.read(buf)) >= 0) {
			out.write(buf, 0, count);
		}
	}

	public void openPdf(ByteArrayOutputStream byteArray) {
		try {
			document = new Document(PageSize.A4);
			writer = PdfWriter.getInstance(document, byteArray);
			HeaderFooterPageEventForPdf event = new ReportController.HeaderFooterPageEventForPdf();
			writer.setPageEvent(event);
			document.open();
		} catch (DocumentException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	static class HeaderFooterPageEventForPdf extends PdfPageEventHelper {
		public void onStartPage(PdfWriter writer, Document document) {

		}

		public void onEndPage(PdfWriter writer, Document document) {
			Rectangle page = document.getPageSize();
			PdfPTable foot = new PdfPTable(1);
			Phrase p = new Phrase();
			PdfPCell footCell = new PdfPCell(p);
			footCell.setBorder(0);
			footCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			foot.addCell(footCell);
			foot.setTotalWidth(page.getWidth());
		}
	}

	private void createExcelFile(HSSFWorkbook workBook, String fileName, Map<String, String> fileExportStatusMap,
			HttpServletResponse response) {
		String folderName = null;
		FileOutputStream fileOut = null;
		try {

			folderName = createFolder();
			response.setContentType("application/xls");
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s.xls\"", fileName);
			response.setHeader(headerKey, headerValue);
			String xlsxFileName = String.format("%s/%s.xls", folderName, fileName);
			fileOut = new FileOutputStream(xlsxFileName);
			workBook.write(fileOut);
			fileOut.flush();
			fileOut.close();
			fileExportStatusMap.put(fileName + ".xls", "200");
			writeFileAsStream(response, xlsxFileName, folderName);
		} catch (Exception ex) {
			fileExportStatusMap.put(fileName + ".xls", "400" + " " + ex.getMessage());
		}
	}
	
	public PdfPCell getCell(String text, int alignment) {
	     PdfPCell cell = new PdfPCell(new Phrase(text));
	     cell.setPadding(0);
	     cell.setHorizontalAlignment(alignment);
	     cell.setBorder(PdfPCell.NO_BORDER);
	     return cell;
	 }
	
	@RequestMapping(value = "/transactionDetails", method = RequestMethod.GET)
	@ResponseBody
	private void customerDetails()
	{
		List<TransactionModel> customers = reportService.getCustomerTransactionReportData();
		List<CustomerTransactionDetailsDto> customerDetails = new ArrayList<>();
		for(TransactionModel model : customers)
		{
			CustomerTransactionDetailsDto detailsDto = new CustomerTransactionDetailsDto();
			detailsDto.setCustomerId(model.getCustomer().getId());
			detailsDto.setMobileNumber(model.getCustomer().getMobile());
			detailsDto.setTransactionAmount(model.getAmount());
			detailsDto.setTransactionType(model.getFundType().getName());
			detailsDto.setTransferType(model.getTransferType().getName());
			detailsDto.setVendorName(model.getVendorType().getName());
			detailsDto.setTransferStatus(model.getTransferStatus().getName());
			detailsDto.setWalletBalance(model.getCustomer().getWalletBalance().getBalance());
			detailsDto.setCommission(model.getCommission());
			customerDetails.add(detailsDto);
		}
		System.out.println("Size "+customerDetails.size());
	}
}
