package com.kouchan.util;


import java.util.Comparator;

import com.kouchan.dto.CustomerTransactionCreditAndDebitDto;

public class SortByCustomerId implements Comparator
{

	@Override
	public int compare(Object object1, Object object2) 
	{
		if (!((object1 instanceof CustomerTransactionCreditAndDebitDto)&& (object2 instanceof CustomerTransactionCreditAndDebitDto))) 
		{
			throw new ClassCastException();
		} 
		else 
		{
			CustomerTransactionCreditAndDebitDto customer1 = (CustomerTransactionCreditAndDebitDto)object1;
			CustomerTransactionCreditAndDebitDto customer2 = (CustomerTransactionCreditAndDebitDto)object2;
			Long a = customer1.getCustomerId();
			Long b = customer1.getCustomerId();
			
			return a.compareTo(b);
		}
	}
	
}
