package com.kouchan.util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class KouchanObjectFactory {

	public static final String LOADED_SERVLET_CONTEXT_ATTRIBUTE = "KouchanObjectFactory Loaded";

	@Autowired
	private static ApplicationContext instance;

	// Private constructor since it's a factory class
	private KouchanObjectFactory() {
	}

	/**
	 * Gets the ApplicationContext instance object held by
	 * LavanteObjectFactory.
	 * 
	 * @return an ApplicationContext object
	 * @throws NullPointerException
	 *             if the ApplicationContext instance is null
	 */
	 static ApplicationContext instance() {
		if (instance == null){
			throw new NullPointerException("instance is null, no one called setup() yet");
		}
		return instance;
	}

	/**
	 * Sets the instance object held by LavanteObjectFactory to the specified
	 * ApplicationContext.
	 * 
	 * @param acf
	 *            the ApplicationContext to set the instance object to
	 */
	private static synchronized void setApplicationContext(ApplicationContext ac) {
		instance = ac;
	}

	/**
	 * Indicate if the context has been setup or not.
	 * 
	 * @return true if the context has previously been setup; otherwise false
	 */
	public static boolean isSetup() {
		return (instance != null);
	}


	/**
	 * Sets the ApplicationContext instance held by LavanteObjectFactory to a
	 * new ApplicationContext iff the instance is null. 
	 * 
	 * @see LavanteBeanFactory
	 */
	public static synchronized void setup(ApplicationContext ac) {
		if (instance == null) {
			setApplicationContext(ac);
		}
	}

	/**
	 * Gets the bean associated with the specified interface.
	 */
	public  <T> T getBean(Class<T> clazz) {
		return instance().getBean(clazz);
	}

	/**
	 * Gets the bean associated with the specified bean name.
	 */
	public static  Object getBean(String beanName) {
		return instance().getBean(beanName);
	}
}
