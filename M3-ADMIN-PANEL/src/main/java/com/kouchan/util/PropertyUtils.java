package com.kouchan.util;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;


@Controller
public class PropertyUtils {
	//private Properties prop = null;
	//private static LavanteBundleMessageSource resource;
	

	
	public  String getPropertyValue( String key) {
		MessageSource messageSource = (MessageSource)KouchanObjectFactory.getBean("messageSource");
		return messageSource.getMessage(key, new Object [] {}, null);

	}
	public  String getText( String key, Object... args ) {
		MessageSource messageSource = (MessageSource)KouchanObjectFactory.getBean("messageSource");
		return messageSource.getMessage(key, args, null);

	}
	public  String getText( String key ) {
		MessageSource messageSource = (MessageSource)KouchanObjectFactory.getBean("messageSource");
		return messageSource.getMessage(key,  new Object [] {}, null);

	}

}
