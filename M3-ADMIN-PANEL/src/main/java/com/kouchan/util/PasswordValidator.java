/** * Copyright (c) 2017 Kouchan, Inc. All Rights Reserved*/
package com.kouchan.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * @author AMARESH S A
 */

public class PasswordValidator{

	  /*
	   * Pwd should be of six to fifteen characters in length
	   * Pwd should have at least one numeric character (from 0 to 9)
	   * Pwd should have at least one Non-alphanumeric character, such as @, !, $, #, %, -
	   */
	  private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[@#$!%-]).{8,15})";

	  private static Pattern pattern;
	  private static Matcher matcher;

      static  {
		  pattern = Pattern.compile(PASSWORD_PATTERN);
	  }

	  /**
	   * Validate password with regular expression
	   * @param password password for validation
	   * @return true for valid password, false for invalid password
	   */
	  public static synchronized boolean validate(final String password){
		  matcher = pattern.matcher(password);
		  return matcher.matches();
 	  }
	  
	  /**
	   * Check if password contains username
	   * @param password password provided by user
	   * @param username username of logged-in user
	   * @return true if password contains username
	   */
	  public static synchronized boolean containsUsername(String password, String userName){
		  return StringUtil.containsIgnoreCase(password, userName);
	  }
}