/** * Copyright (c) 2017 Kouchan, Inc. All Rights Reserved*/
package com.kouchan.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * @author AMARESH S A
 */

public class StringUtil extends org.apache.commons.lang.StringUtils {
	  public static final java.lang.String EMPTY = "";
	  
	public static String convertToTitleCase(String input) {
	    StringBuilder titleCase = new StringBuilder();
	    boolean nextTitleCase = true;

	    for (char c : input.toCharArray()) {
	        if (Character.isSpaceChar(c)) {
	            nextTitleCase = true;
	        } else if (nextTitleCase) {
	            c = Character.toTitleCase(c);
	            nextTitleCase = false;
	        }

	        titleCase.append(c);
	    }

	    return titleCase.toString();
	}
	
	
	public static boolean isNumeric(String input){
		if(input==null){
			return false;
		}
		Pattern p = Pattern.compile( "([0-9]*)" );
		Matcher m = p.matcher(input);
		
		return m.matches();
	}
	  /**
     * Return true if the specified string is null or ""; otherwise, return false.
     * 
     * @param s
     * @return true if the specified string is null or ""
     */
    public static boolean isNullOrEmpty( String s ) {
    	return ( s == null || s.length() == 0 );
    }
    
    /**
     * Return true if the specified string is null or trimmed is ""; otherwise, return false.
     * 
     * @param s
     * @return true if the specified string is null or trimmed is ""
     */
    public static boolean isNullOrEmptyTrimmed( String s ) {
    	return ( s == null || s.trim().length() == 0 );
    } 
	/**
     * @Purpose To find whether a string array contains a given input string
     * @param sourceString string to be found
     * @param stringArray string array
     * @return boolean true/false
     */
    public static boolean containsIgnoreCase( String sourceString, String[] stringArray ) {
    	
    	for (String string : stringArray) {
			
    		if( string != null && sourceString != null 
    				&& sourceString.trim().toLowerCase().indexOf(string.toLowerCase()) != -1 ){
    			return true;
    		}
		}
    	return false;
    }
    /**
     * @Purpose To find whether a given substring contains in 
     * 			the provided string or not with case-insensitive
     * @param sourceString source string
     * @param subStringToBeFound sub string to be found
     * @return boolean true/false
     */
    public static boolean containsIgnoreCase( String sourceString, String subStringToBeFound ) {
        if ( !( StringUtil.isNullOrEmptyTrimmed( sourceString ) 
        		|| StringUtil.isNullOrEmptyTrimmed( subStringToBeFound ) ) ) {
        	return sourceString.toLowerCase().contains( subStringToBeFound.toLowerCase() );
        } else {
        	return false;
        }
    }
    
    /**
     * Convert String to lower came case.
     * 
     * @param originalText
     * @return converted to lower camel case
     */
    public static String toLowerCamelCase( String originalText ) {
        if ( !StringUtil.isNotBlank( originalText ) ) {
            return originalText;
        }
        return originalText.substring( 0, 1 ).toLowerCase() + originalText.substring( 1 );
    }
   
    public static String arrayToCommaDelimitedString(Object [] stringArray){
    	return org.springframework.util.StringUtils.arrayToCommaDelimitedString(stringArray);
    }
}
