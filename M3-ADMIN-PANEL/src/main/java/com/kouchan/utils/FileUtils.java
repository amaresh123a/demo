package com.kouchan.utils;


import java.io.File;
import java.nio.file.Paths;



public class FileUtils {
	
	public static void deleteDirectory( String folderPath ) {
    	if ((folderPath!=null && !folderPath.isEmpty()) ) {
	    	File directory = new File ( folderPath );
	    	File[] files = directory.listFiles(); 
	    	for( int index = 0; index < files.length; ++index ) {
	    		files[index].delete();
	    	}
	    	directory.delete();
    	}
    }
	public static void deleteFile( String filePath ) {
    	if ((filePath!=null && !filePath.isEmpty())) {
    		
	    	File file;
			try {
				file= Paths.get(filePath).toFile();
				file.delete();	    
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    	
    	}
    }
    
}
