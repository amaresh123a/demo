package com.kouchan.utils;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Data {

    private Object items;

    @JsonInclude(value=JsonInclude.Include.NON_NULL)
    private String type;
    
    
    @JsonInclude(value=JsonInclude.Include.NON_NULL)
    private List<String> prePayStatusList;
    
    @JsonInclude(value=JsonInclude.Include.NON_NULL)
    private List<String> postPayStatusList;
    
    @JsonInclude(value=JsonInclude.Include.NON_NULL)
    

    public Object getItems (){
        return items;
    }

    public void setItems (Object items){
        this.items = items;
    }

    public String getType (){
        return type;
    }

    public void setType (String type){
        this.type = type;
    }


	public List<String> getPrePayStatusList() {
		return prePayStatusList;
	}

	public void setPrePayStatusList(List<String> prePayStatusList) {
		this.prePayStatusList = prePayStatusList;
	}

	public List<String> getPostPayStatusList() {
		return postPayStatusList;
	}

	public void setPostPayStatusList(List<String> postPayStatusList) {
		this.postPayStatusList = postPayStatusList;
	}


	@Override
    public String toString(){
        return "ClassPojo [items = "+items+", type = "+type+"]";
    }
}
