package com.kouchan.to;

import java.beans.Transient;

/*
 */


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the address database table.
 *
 */
@Entity
@Table(name = "address")
@NamedQuery(name = "AddressModel.findAll", query = "SELECT a FROM AddressModel a")
public class AddressModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/** The address 1. */
	@Column(name = "ADDRESS_1")
	private String address1;

	/** The address 2. */
	@Column(name = "ADDRESS_2")
	private String address2;

	/** The address 3. */
	@Column(name = "ADDRESS_3")
	private String address3;

	/** The city or distirct. */
	@Column(name = "CITY_OR_DISTIRCT")
	private String cityOrDistirct;

	/** The country. */
	@Column(name = "country")
	private String country;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The is deleted. */
	
	@Column(name = "IS_DELETED", columnDefinition = "TINYINT(1) default 0 ")
	
	private byte isDeleted;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The postal code. */
	@Column(name = "POSTAL_CODE")
	private String postalCode;

	/** The state. */
	@Column(name = "state")
	private String state;

	/** The address type. */
	// bi-directional many-to-one association to AddressType
	@ManyToOne
	@JoinColumn(name = "ADDRESS_TYPE_ID")
	private AddressTypeModel addressType;

	/** The customer. */
	// bi-directional many-to-one association to Customer
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private CustomerModel customer;

	/**
	 * Instantiates a new address.
	 */
	public AddressModel()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id)
	{
		this.id = id;
	}

	/**
	 * Gets the address 1.
	 *
	 * @return the address 1
	 */
	public String getAddress1()
	{
		return this.address1;
	}

	/**
	 * Sets the address 1.
	 *
	 * @param address1
	 *            the new address 1
	 */
	public void setAddress1(final String address1)
	{
		this.address1 = address1;
	}

	/**
	 * Gets the address 2.
	 *
	 * @return the address 2
	 */
	public String getAddress2()
	{
		return this.address2;
	}

	/**
	 * Sets the address 2.
	 *
	 * @param address2
	 *            the new address 2
	 */
	public void setAddress2(final String address2)
	{
		this.address2 = address2;
	}

	/**
	 * Gets the address 3.
	 *
	 * @return the address 3
	 */
	public String getAddress3()
	{
		return this.address3;
	}

	/**
	 * Sets the address 3.
	 *
	 * @param address3
	 *            the new address 3
	 */
	public void setAddress3(final String address3)
	{
		this.address3 = address3;
	}

	/**
	 * Gets the city or distirct.
	 *
	 * @return the city or distirct
	 */
	public String getCityOrDistirct()
	{
		return this.cityOrDistirct;
	}

	/**
	 * Sets the city or distirct.
	 *
	 * @param cityOrDistirct
	 *            the new city or distirct
	 */
	public void setCityOrDistirct(final String cityOrDistirct)
	{
		this.cityOrDistirct = cityOrDistirct;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry()
	{
		return this.country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country
	 *            the new country
	 */
	public void setCountry(final String country)
	{
		this.country = country;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the checks if is deleted.
	 *
	 * @return the checks if is deleted
	 */
	public byte getIsDeleted()
	{
		return this.isDeleted;
	}

	/**
	 * Sets the checks if is deleted.
	 *
	 * @param isDeleted
	 *            the new checks if is deleted
	 */
	@Transient
	public void setIsDeleted(final byte isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * Gets the postal code.
	 *
	 * @return the postal code
	 */
	public String getPostalCode()
	{
		return this.postalCode;
	}

	/**
	 * Sets the postal code.
	 *
	 * @param postalCode
	 *            the new postal code
	 */
	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState()
	{
		return this.state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state
	 *            the new state
	 */
	public void setState(final String state)
	{
		this.state = state;
	}

	/**
	 * Gets the address type.
	 *
	 * @return the address type
	 */
	public AddressTypeModel getAddressType()
	{
		return this.addressType;
	}

	/**
	 * Sets the address type.
	 *
	 * @param addressType
	 *            the new address type
	 */
	public void setAddressType(final AddressTypeModel addressType)
	{
		this.addressType = addressType;
	}

	/**
	 * Gets the customer.
	 *
	 * @return the customer
	 */
	public CustomerModel getCustomer()
	{
		return this.customer;
	}

	/**
	 * Sets the customer.
	 *
	 * @param customer
	 *            the new customer
	 */
	public void setCustomer(final CustomerModel customer)
	{
		this.customer = customer;
	}

}