/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the bank_detail database table.
 *
 */
@Entity
@Table(name = "bank_detail")
@NamedQuery(name = "BankDetailModel.findAll", query = "SELECT b FROM BankDetailModel b")
public class BankDetailModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	/** The account number. */
	@Column(name = "ACCOUNT_NUMBER")
	private String accountNumber;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The ifsc code. */
	@Column(name = "IFSC_CODE")
	private String ifscCode;

	/** The is deleted. */
	@Column(name = "IS_DELETED", columnDefinition = "TINYINT(1) default 0 ")
	private byte isDeleted;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The name. */
	@Column(name = "name")
	private String name;

	/** The customer. */
	// bi-directional many-to-one association to Customer
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private CustomerModel customer;

	/** The transaction histories. */
	// bi-directional many-to-one association to TransactionHistory
	@OneToMany(mappedBy = "bankDetail")
	private List<TransactionHistoryModel> transactionHistories;

	/**
	 * Instantiates a new bank detail.
	 */
	public BankDetailModel()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id)
	{
		this.id = id;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public String getAccountNumber()
	{
		return this.accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber
	 *            the new account number
	 */
	public void setAccountNumber(final String accountNumber)
	{
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the ifsc code.
	 *
	 * @return the ifsc code
	 */
	public String getIfscCode()
	{
		return this.ifscCode;
	}

	/**
	 * Sets the ifsc code.
	 *
	 * @param ifscCode
	 *            the new ifsc code
	 */
	public void setIfscCode(final String ifscCode)
	{
		this.ifscCode = ifscCode;
	}

	/**
	 * Gets the checks if is deleted.
	 *
	 * @return the checks if is deleted
	 */
	public byte getIsDeleted()
	{
		return this.isDeleted;
	}

	/**
	 * Sets the checks if is deleted.
	 *
	 * @param isDeleted
	 *            the new checks if is deleted
	 */
	public void setIsDeleted(final byte isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * Gets the customer.
	 *
	 * @return the customer
	 */
	public CustomerModel getCustomer()
	{
		return this.customer;
	}

	/**
	 * Sets the customer.
	 *
	 * @param customer
	 *            the new customer
	 */
	public void setCustomer(final CustomerModel customer)
	{
		this.customer = customer;
	}

	/**
	 * Gets the transaction histories.
	 *
	 * @return the transaction histories
	 */
	public List<TransactionHistoryModel> getTransactionHistories()
	{
		return this.transactionHistories;
	}

	/**
	 * Sets the transaction histories.
	 *
	 * @param transactionHistories
	 *            the new transaction histories
	 */
	public void setTransactionHistories(final List<TransactionHistoryModel> transactionHistories)
	{
		this.transactionHistories = transactionHistories;
	}

	/**
	 * Adds the transaction history.
	 *
	 * @param transactionHistory
	 *            the transaction history
	 * @return the transaction history
	 */
	public TransactionHistoryModel addTransactionHistory(final TransactionHistoryModel transactionHistory)
	{
		getTransactionHistories().add(transactionHistory);
		transactionHistory.setBankDetail(this);

		return transactionHistory;
	}

	/**
	 * Removes the transaction history.
	 *
	 * @param transactionHistory
	 *            the transaction history
	 * @return the transaction history
	 */
	public TransactionHistoryModel removeTransactionHistory(final TransactionHistoryModel transactionHistory)
	{
		getTransactionHistories().remove(transactionHistory);
		transactionHistory.setBankDetail(null);

		return transactionHistory;
	}

}