/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "keygenerator")
@NamedQuery(name = "KeyGeneratorModel.findAll", query = "SELECT k FROM KeyGeneratorModel k")
public class KeyGeneratorModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "unique_code")
	private Integer uniqueCode;

	@Column(name = "prefix")
	private String prefix;

	/**
	 * Instantiates a new customer.
	 */
	public KeyGeneratorModel()
	{
	}

	public Integer getUniqueCode()
	{
		return uniqueCode;
	}

	public void setUniqueCode(final Integer uniqueCode)
	{
		this.uniqueCode = uniqueCode;
	}

	public String getPrefix()
	{
		return prefix;
	}

	public void setPrefix(final String prefix)
	{
		this.prefix = prefix;
	}

}