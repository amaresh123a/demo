/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the currency_conversion database table.
 * 
 */
@Entity
@Table(name = "currency_conversion")
@NamedQuery(name = "CurrencyConversionModel.findAll", query = "SELECT c FROM CurrencyConversionModel c")
public class CurrencyConversionModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@Column(name = "id")
	private int id;

	/** The base currency. */
	@Column(name = "BASE_CURRENCY")
	private byte baseCurrency;

	/** The conversation factor. */
	@Column(name = "CONVERSATION_FACTOR")
	private double conversationFactor;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The currency symbol. */
	@Column(name = "CURRENCY_SYMBOL")
	private String currencySymbol;

	/** The currency type. */
	@Column(name = "CURRENCY_TYPE")
	private String currencyType;

	/** The is deleted. */
	@Column(name = "IS_DELETED", columnDefinition = "TINYINT(1) default 0 ")
	private byte isDeleted;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/**
	 * Instantiates a new currency conversion.
	 */
	public CurrencyConversionModel()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final int id)
	{
		this.id = id;
	}

	/**
	 * Gets the base currency.
	 *
	 * @return the base currency
	 */
	public byte getBaseCurrency()
	{
		return this.baseCurrency;
	}

	/**
	 * Sets the base currency.
	 *
	 * @param baseCurrency
	 *            the new base currency
	 */
	public void setBaseCurrency(final byte baseCurrency)
	{
		this.baseCurrency = baseCurrency;
	}

	/**
	 * Gets the conversation factor.
	 *
	 * @return the conversation factor
	 */
	public double getConversationFactor()
	{
		return this.conversationFactor;
	}

	/**
	 * Sets the conversation factor.
	 *
	 * @param conversationFactor
	 *            the new conversation factor
	 */
	public void setConversationFactor(final double conversationFactor)
	{
		this.conversationFactor = conversationFactor;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the currency symbol.
	 *
	 * @return the currency symbol
	 */
	public String getCurrencySymbol()
	{
		return this.currencySymbol;
	}

	/**
	 * Sets the currency symbol.
	 *
	 * @param currencySymbol
	 *            the new currency symbol
	 */
	public void setCurrencySymbol(final String currencySymbol)
	{
		this.currencySymbol = currencySymbol;
	}

	/**
	 * Gets the currency type.
	 *
	 * @return the currency type
	 */
	public String getCurrencyType()
	{
		return this.currencyType;
	}

	/**
	 * Sets the currency type.
	 *
	 * @param currencyType
	 *            the new currency type
	 */
	public void setCurrencyType(final String currencyType)
	{
		this.currencyType = currencyType;
	}

	/**
	 * Gets the checks if is deleted.
	 *
	 * @return the checks if is deleted
	 */
	public byte getIsDeleted()
	{
		return this.isDeleted;
	}

	/**
	 * Sets the checks if is deleted.
	 *
	 * @param isDeleted
	 *            the new checks if is deleted
	 */
	public void setIsDeleted(final byte isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

}