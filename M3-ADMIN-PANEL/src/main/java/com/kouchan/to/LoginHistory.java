package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The persistent class for the login_history database table.
 *
 */
@Entity
@Table(name = "login_history")
@NamedQuery(name = "LoginHistory.findAll", query = "SELECT l FROM LoginHistory l")
public class LoginHistory implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/** The login time. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LOGIN_TIME")
	@JsonFormat(locale = "en-IN", shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm", timezone = "GMT+5:30")
	private Date loginTime;

	/** The customer. */
	// bi-directional many-to-one association to Customer
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private CustomerModel customer;

	/**
	 * Instantiates a new login history.
	 */
	public LoginHistory()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id)
	{
		this.id = id;
	}

	/**
	 * Gets the login time.
	 *
	 * @return the login time
	 */
	public Date getLoginTime()
	{
		return this.loginTime;
	}

	/**
	 * Sets the login time.
	 *
	 * @param loginTime
	 *            the new login time
	 */
	public void setLoginTime(final Date loginTime)
	{
		this.loginTime = loginTime;
	}

	/**
	 * Gets the customer.
	 *
	 * @return the customer
	 */
	public CustomerModel getCustomer()
	{
		return this.customer;
	}

	/**
	 * Sets the customer.
	 *
	 * @param customer
	 *            the new customer
	 */
	public void setCustomer(final CustomerModel customer)
	{
		this.customer = customer;
	}

}