/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the commission database table.
 * 
 */
@Entity
@Table(name = "commission")
@NamedQuery(name = "CommissionModel.findAll", query = "SELECT c FROM CommissionModel c")
public class CommissionModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@Column(name = "id")
	private int id;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The end date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "END_DATE")
	private Date endDate;

	/** The is active. */
	@Column(name = "IS_ACTIVE")
	private byte isActive;
    
    /** The is percentage. */
    @Column(name="IS_PERCENTAGE", columnDefinition = "tinyint default false")
    private boolean isPercentage;

	/** The kouchan charges. */
	@Column(name = "KOUCHAN_CHARGES")
	private double kouchanCharges;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The range from. */
	@Column(name = "RANGE_FROM")
	private double rangeFrom;

	/** The range to. */
	@Column(name = "RANGE_TO")
	private double rangeTo;

	/** The start date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "START_DATE")
	private Date startDate;

	/** The vendor charges. */
	@Column(name = "VENDOR_CHARGES")
	private double vendorCharges;

	/** The customer type. */
	// bi-directional many-to-one association to CustomerType
	@ManyToOne
	@JoinColumn(name = "CUSTOMER_TYPE_ID")
	private CustomerTypeModel customerType;

	/** The transfer type. */
	// bi-directional many-to-one association to TransferType
	@ManyToOne
	@JoinColumn(name = "TRANSFER_TYPE_ID")
	private TransferTypeModel transferType;

	/**
	 * Instantiates a new commission.
	 */
	public CommissionModel()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final int id)
	{
		this.id = id;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate()
	{
		return this.endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate
	 *            the new end date
	 */
	public void setEndDate(final Date endDate)
	{
		this.endDate = endDate;
	}

	/**
	 * Gets the checks if is active.
	 *
	 * @return the checks if is active
	 */
	public byte getIsActive()
	{
		return this.isActive;
	}

	/**
	 * Sets the checks if is active.
	 *
	 * @param isActive
	 *            the new checks if is active
	 */
	public void setIsActive(final byte isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * Gets the kouchan charges.
	 *
	 * @return the kouchan charges
	 */
	public double getKouchanCharges()
	{
		return this.kouchanCharges;
	}

	/**
	 * Sets the kouchan charges.
	 *
	 * @param kouchanCharges
	 *            the new kouchan charges
	 */
	public void setKouchanCharges(final double kouchanCharges)
	{
		this.kouchanCharges = kouchanCharges;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * Gets the range from.
	 *
	 * @return the range from
	 */
	public double getRangeFrom()
	{
		return this.rangeFrom;
	}

	/**
	 * Sets the range from.
	 *
	 * @param rangeFrom
	 *            the new range from
	 */
	public void setRangeFrom(final double rangeFrom)
	{
		this.rangeFrom = rangeFrom;
	}

	/**
	 * Gets the range to.
	 *
	 * @return the range to
	 */
	public double getRangeTo()
	{
		return this.rangeTo;
	}

	/**
	 * Sets the range to.
	 *
	 * @param rangeTo
	 *            the new range to
	 */
	public void setRangeTo(final double rangeTo)
	{
		this.rangeTo = rangeTo;
	}

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate()
	{
		return this.startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate
	 *            the new start date
	 */
	public void setStartDate(final Date startDate)
	{
		this.startDate = startDate;
	}

	/**
	 * Gets the vendor charges.
	 *
	 * @return the vendor charges
	 */
	public double getVendorCharges()
	{
		return this.vendorCharges;
	}

	/**
	 * Sets the vendor charges.
	 *
	 * @param vendorCharges
	 *            the new vendor charges
	 */
	public void setVendorCharges(final double vendorCharges)
	{
		this.vendorCharges = vendorCharges;
	}

	/**
	 * Gets the customer type.
	 *
	 * @return the customer type
	 */
	public CustomerTypeModel getCustomerType()
	{
		return this.customerType;
	}

	/**
	 * Sets the customer type.
	 *
	 * @param customerType
	 *            the new customer type
	 */
	public void setCustomerType(final CustomerTypeModel customerType)
	{
		this.customerType = customerType;
	}

	/**
	 * Gets the transfer type.
	 *
	 * @return the transfer type
	 */
	public TransferTypeModel getTransferType()
	{
		return this.transferType;
	}

	/**
	 * Sets the transfer type.
	 *
	 * @param transferType
	 *            the new transfer type
	 */
	public void setTransferType(final TransferTypeModel transferType)
	{
		this.transferType = transferType;
	}

    /**
     * Checks if is percentage.
     *
     * @return true, if is percentage
     */
    public boolean isPercentage()
    {
        return isPercentage;
    }

    /**
     * Sets the percentage.
     *
     * @param isPercentage the new percentage
     */
    public void setPercentage(boolean isPercentage)
    {
        this.isPercentage = isPercentage;
    }
    
    

}