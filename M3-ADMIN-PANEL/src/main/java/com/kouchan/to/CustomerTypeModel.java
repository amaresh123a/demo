/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the customer_type database table.
 *
 */
@Entity
@Table(name = "customer_type")
@NamedQuery(name = "CustomerTypeModel.findAll", query = "SELECT c FROM CustomerTypeModel c")
public class CustomerTypeModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@Column(name = "id")
	private Long id;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The name. */
	@Column(name = "name")
	private String name;

	/** The commissions. */
	// bi-directional many-to-one association to Commission
	@OneToMany(mappedBy = "customerType")
	private List<CommissionModel> commissions;

	/** The customers. */
	// bi-directional many-to-one association to Customer
	@OneToMany(mappedBy = "customerType")
	private List<CustomerModel> customers;

	// bi-directional many-to-one association to Customer
	@OneToMany(mappedBy = "accountType")
	private List<CustomerModel> listOfAccTypeCustomer;

	/**
	 * Instantiates a new customer type.
	 */
	public CustomerTypeModel()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id)
	{
		this.id = id;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * Gets the commissions.
	 *
	 * @return the commissions
	 */
	public List<CommissionModel> getCommissions()
	{
		return this.commissions;
	}

	/**
	 * Sets the commissions.
	 *
	 * @param commissions
	 *            the new commissions
	 */
	public void setCommissions(final List<CommissionModel> commissions)
	{
		this.commissions = commissions;
	}

	/**
	 * Adds the commission.
	 *
	 * @param commission
	 *            the commission
	 * @return the commission
	 */
	public CommissionModel addCommission(final CommissionModel commission)
	{
		getCommissions().add(commission);
		commission.setCustomerType(this);

		return commission;
	}

	/**
	 * Removes the commission.
	 *
	 * @param commission
	 *            the commission
	 * @return the commission
	 */
	public CommissionModel removeCommission(final CommissionModel commission)
	{
		getCommissions().remove(commission);
		commission.setCustomerType(null);

		return commission;
	}

	/**
	 * Gets the customers.
	 *
	 * @return the customers
	 */
	public List<CustomerModel> getCustomers()
	{
		return this.customers;
	}

	/**
	 * Sets the customers.
	 *
	 * @param customers
	 *            the new customers
	 */
	public void setCustomers(final List<CustomerModel> customers)
	{
		this.customers = customers;
	}

	public List<CustomerModel> getListOfAccTypeCustomer()
	{
		return this.listOfAccTypeCustomer;
	}

	public void setListOfAccTypeCustomer(final List<CustomerModel> listOfAccTypeCustomer)
	{
		this.listOfAccTypeCustomer = listOfAccTypeCustomer;
	}

	/**
	 * Adds the customer.
	 *
	 * @param customer
	 *            the customer
	 * @return the customer
	 */
	public CustomerModel addCustomer(final CustomerModel customer)
	{
		getCustomers().add(customer);
		customer.setCustomerType(this);

		return customer;
	}

	/**
	 * Removes the customer.
	 *
	 * @param customer
	 *            the customer
	 * @return the customer
	 */
	public CustomerModel removeCustomer(final CustomerModel customer)
	{
		getCustomers().remove(customer);
		customer.setCustomerType(null);

		return customer;
	}

	public CustomerModel addListOfAccTypeCustomer(final CustomerModel customer)
	{
		getListOfAccTypeCustomer().add(customer);
		customer.setCustomerType(this);

		return customer;
	}

	/**
	 * Removes the customer.
	 *
	 * @param customer
	 *            the customer
	 * @return the customer
	 */
	public CustomerModel removeListOfAccTypeCustomer(final CustomerModel customer)
	{
		getListOfAccTypeCustomer().remove(customer);
		customer.setCustomerType(null);

		return customer;
	}

}