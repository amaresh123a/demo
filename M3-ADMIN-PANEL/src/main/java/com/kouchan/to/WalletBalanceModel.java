/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the wallet_balance database table.
 *
 */
@Entity
@Table(name = "wallet_balance")
@NamedQuery(name = "WalletBalanceModel.findAll", query = "SELECT w FROM WalletBalanceModel w")
public class WalletBalanceModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/** The balance. */
	private double balance;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@CreatedDate
	@JsonFormat(locale = "en-IN", shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm", timezone = "GMT+5:30")
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The is deleted. */
	@Column(name = "IS_DELETED", columnDefinition = "TINYINT(1) default 0 ")
	private byte isDeleted;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The customers. */
	// TODO: Make One to One relationship with customer table
	// bi-directional one-to-one association to Customer
	@OneToOne(mappedBy = "walletBalance")
	private CustomerModel customers;

	/**
	 * Instantiates a new wallet balance.
	 */
	public WalletBalanceModel()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id)
	{
		this.id = id;
	}

	/**
	 * Gets the balance.
	 *
	 * @return the balance
	 */
	public double getBalance()
	{
		return this.balance;
	}

	/**
	 * Sets the balance.
	 *
	 * @param balance
	 *            the new balance
	 */
	public void setBalance(final double balance)
	{
		this.balance = balance;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the checks if is deleted.
	 *
	 * @return the checks if is deleted
	 */
	public byte getIsDeleted()
	{
		return this.isDeleted;
	}

	/**
	 * Sets the checks if is deleted.
	 *
	 * @param isDeleted
	 *            the new checks if is deleted
	 */
	public void setIsDeleted(final byte isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	public CustomerModel getCustomers()
	{
		return customers;
	}

	public void setCustomers(final CustomerModel customers)
	{
		this.customers = customers;
	}

	/**
	 * Adds the customer.
	 *
	 * @param customer
	 *            the customer
	 * @return the customer
	 */
	public CustomerModel addCustomer(final CustomerModel customer)
	{
		this.customers = customer;
		customer.setWalletBalance(this);

		return customer;
	}

	/**
	 * Removes the customer.
	 *
	 * @param customer
	 *            the customer
	 * @return the customer
	 */
	public CustomerModel removeCustomer(final CustomerModel customer)
	{
		this.customers.setWalletBalance(null);
		this.customers = null;
		return customer;
	}

}