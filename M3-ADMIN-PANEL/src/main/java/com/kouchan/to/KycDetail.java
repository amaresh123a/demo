/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the kyc_detail database table.
 *
 */
@Entity
@Table(name = "kyc_detail")
@NamedQuery(name = "KycDetail.findAll", query = "SELECT k FROM KycDetail k")
public class KycDetail implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The is deleted. */
	@Column(name = "IS_DELETED", columnDefinition = "TINYINT(1) default 0 ")
	private byte isDeleted;

	/** The kyc avatar. */
	@Column(name = "KYC_AVATAR")
	private String kycAvatar;

	/** The kyc number. */
	@Column(name = "KYC_NUMBER")
	private String kycNumber;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The customer. */
	// bi-directional many-to-one association to Customer
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private CustomerModel customer;

	/** The kyc document type. */
	// bi-directional many-to-one association to KycDocumentType
	@ManyToOne
	@JoinColumn(name = "KYC_DOCUMENT_TYPE_ID")
	private KycDocumentTypeModel kycDocumentType;

	/**
	 * Instantiates a new kyc detail.
	 */
	public KycDetail()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id)
	{
		this.id = id;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the checks if is deleted.
	 *
	 * @return the checks if is deleted
	 */
	public byte getIsDeleted()
	{
		return this.isDeleted;
	}

	/**
	 * Sets the checks if is deleted.
	 *
	 * @param isDeleted
	 *            the new checks if is deleted
	 */
	public void setIsDeleted(final byte isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Gets the kyc avatar.
	 *
	 * @return the kyc avatar
	 */
	public String getKycAvatar()
	{
		return this.kycAvatar;
	}

	/**
	 * Sets the kyc avatar.
	 *
	 * @param kycAvatar
	 *            the new kyc avatar
	 */
	public void setKycAvatar(final String kycAvatar)
	{
		this.kycAvatar = kycAvatar;
	}

	/**
	 * Gets the kyc number.
	 *
	 * @return the kyc number
	 */
	public String getKycNumber()
	{
		return this.kycNumber;
	}

	/**
	 * Sets the kyc number.
	 *
	 * @param kycNumber
	 *            the new kyc number
	 */
	public void setKycNumber(final String kycNumber)
	{
		this.kycNumber = kycNumber;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * Gets the customer.
	 *
	 * @return the customer
	 */
	public CustomerModel getCustomer()
	{
		return this.customer;
	}

	/**
	 * Sets the customer.
	 *
	 * @param customer
	 *            the new customer
	 */
	public void setCustomer(final CustomerModel customer)
	{
		this.customer = customer;
	}

	/**
	 * Gets the kyc document type.
	 *
	 * @return the kyc document type
	 */
	public KycDocumentTypeModel getKycDocumentType()
	{
		return this.kycDocumentType;
	}

	/**
	 * Sets the kyc document type.
	 *
	 * @param kycDocumentType
	 *            the new kyc document type
	 */
	public void setKycDocumentType(final KycDocumentTypeModel kycDocumentType)
	{
		this.kycDocumentType = kycDocumentType;
	}

}