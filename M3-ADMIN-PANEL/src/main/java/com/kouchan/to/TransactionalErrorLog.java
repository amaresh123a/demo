package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

// TODO: Auto-generated Javadoc
/**
 * The Class TransactionalErrorLog.
 */
@Entity
@Table(name = "TRANSACTIONAL_ERROR_LOG")
public class TransactionalErrorLog implements Serializable
{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/** The transfer response. */
	@Column(name = "TRANSACTIONAL_RESPONSE", columnDefinition = "TEXT")
	private String transferResponse;

	/** The exception message. */
	@Column(name = "TRANSACTIONAL_EXCEPTION", columnDefinition = "TEXT")
	private String exceptionMessage;

	/** The created date. */
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	public long getId()
	{
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final long id)
	{
		this.id = id;
	}

	/**
	 * Gets the transfer response.
	 *
	 * @return the transfer response
	 */
	public String getTransferResponse()
	{
		return transferResponse;
	}

	/**
	 * Sets the transfer response.
	 *
	 * @param transferResponse
	 *            the new transfer response
	 */
	public void setTransferResponse(final String transferResponse)
	{
		this.transferResponse = transferResponse;
	}

	/**
	 * Gets the exception message.
	 *
	 * @return the exception message
	 */
	public String getExceptionMessage()
	{
		return exceptionMessage;
	}

	/**
	 * Sets the exception message.
	 *
	 * @param exceptionMessage
	 *            the new exception message
	 */
	public void setExceptionMessage(final String exceptionMessage)
	{
		this.exceptionMessage = exceptionMessage;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

}
