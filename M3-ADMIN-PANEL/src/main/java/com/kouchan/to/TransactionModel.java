/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the transaction database table.
 *
 */
@Entity
@Table(name = "transaction")
@NamedQuery(name = "TransactionModel.findAll", query = "SELECT t FROM TransactionModel t")
public class TransactionModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	/** The amount. */
	private double amount;

	
	@Column(name = "utility_commission")
	 private double commission;
	
	
	/** The cgst. */
	private double cgst;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The igst. */
	private double igst;

	/** The kouchan reference number. */
	@Column(name = "KOUCHAN_REFERENCE_NUMBER")
	private String kouchanReferenceNumber;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The service charge. */
	@Column(name = "SERVICE_CHARGE")
	private double serviceCharge;

	/** The sgst. */
	private double sgst;

	/** The vendor or group reference number. */
	@Column(name = "VENDOR_OR_GROUP_REFERENCE_NUMBER")
	private String vendorOrGroupReferenceNumber;

	/** The customer. */
	// bi-directional many-to-one association to Customer
	@ManyToOne
	@JoinColumn(name = "customer_id")
	@JsonIgnore
	private CustomerModel customer;

	/** The fund type. */
	// bi-directional many-to-one association to FundType
	@ManyToOne
	@JoinColumn(name = "FUND_TYPE_ID")
	private FundType fundType;

	/** The transfer status. */
	// bi-directional many-to-one association to TransferStatus
	@ManyToOne
	@JoinColumn(name = "TRANSACTION_STATUS_ID")
	private TransferStatusModel transferStatus;

	/** The transfer type. */
	// bi-directional many-to-one association to TransferType
	@ManyToOne
	@JoinColumn(name = "TRANSFER_TYPE_ID")
	private TransferTypeModel transferType;

	/** The vendor type. */
	// bi-directional many-to-one association to VendorType
	@ManyToOne
	@JoinColumn(name = "VENDOR_TYPE_ID")
	private VendorTypeModel vendorType;

	/** The total amount. */
	@Transient
	private double totalAmount;

	/** The bank detail. */
	@Transient
	private BankDetailModel bankDetail;

	@Column(name = "DETAILS")
	private String details;

	/**
	 * Instantiates a new transaction.
	 */
	public TransactionModel()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId()
	{
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final long id)
	{
		this.id = id;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public double getAmount()
	{
		return this.amount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param amount
	 *            the new amount
	 */
	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	/**
	 * Gets the cgst.
	 *
	 * @return the cgst
	 */
	public double getCgst()
	{
		return this.cgst;
	}

	/**
	 * Sets the cgst.
	 *
	 * @param cgst
	 *            the new cgst
	 */
	public void setCgst(final double cgst)
	{
		this.cgst = cgst;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the igst.
	 *
	 * @return the igst
	 */
	public double getIgst()
	{
		return this.igst;
	}

	/**
	 * Sets the igst.
	 *
	 * @param igst
	 *            the new igst
	 */
	public void setIgst(final double igst)
	{
		this.igst = igst;
	}

	/**
	 * Gets the kouchan reference number.
	 *
	 * @return the kouchan reference number
	 */
	public String getKouchanReferenceNumber()
	{
		return this.kouchanReferenceNumber;
	}

	/**
	 * Sets the kouchan reference number.
	 *
	 * @param kouchanReferenceNumber
	 *            the new kouchan reference number
	 */
	public void setKouchanReferenceNumber(final String kouchanReferenceNumber)
	{
		this.kouchanReferenceNumber = kouchanReferenceNumber;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * Gets the service charge.
	 *
	 * @return the service charge
	 */
	public double getServiceCharge()
	{
		return this.serviceCharge;
	}

	/**
	 * Sets the service charge.
	 *
	 * @param serviceCharge
	 *            the new service charge
	 */
	public void setServiceCharge(final double serviceCharge)
	{
		this.serviceCharge = serviceCharge;
	}

	/**
	 * Gets the sgst.
	 *
	 * @return the sgst
	 */
	public double getSgst()
	{
		return this.sgst;
	}

	/**
	 * Sets the sgst.
	 *
	 * @param sgst
	 *            the new sgst
	 */
	public void setSgst(final double sgst)
	{
		this.sgst = sgst;
	}

	/**
	 * Gets the vendor or group reference number.
	 *
	 * @return the vendor or group reference number
	 */
	public String getVendorOrGroupReferenceNumber()
	{
		return this.vendorOrGroupReferenceNumber;
	}

	/**
	 * Sets the vendor or group reference number.
	 *
	 * @param vendorOrGroupReferenceNumber
	 *            the new vendor or group reference number
	 */
	public void setVendorOrGroupReferenceNumber(final String vendorOrGroupReferenceNumber)
	{
		this.vendorOrGroupReferenceNumber = vendorOrGroupReferenceNumber;
	}

	/**
	 * Gets the customer.
	 *
	 * @return the customer
	 */
	public CustomerModel getCustomer()
	{
		return this.customer;
	}

	/**
	 * Sets the customer.
	 *
	 * @param customer
	 *            the new customer
	 */
	public void setCustomer(final CustomerModel customer)
	{
		this.customer = customer;
	}

	/**
	 * Gets the fund type.
	 *
	 * @return the fund type
	 */
	public FundType getFundType()
	{
		return this.fundType;
	}

	/**
	 * Sets the fund type.
	 *
	 * @param fundType
	 *            the new fund type
	 */
	public void setFundType(final FundType fundType)
	{
		this.fundType = fundType;
	}

	/**
	 * Gets the transfer status.
	 *
	 * @return the transfer status
	 */
	public TransferStatusModel getTransferStatus()
	{
		return this.transferStatus;
	}

	/**
	 * Sets the transfer status.
	 *
	 * @param transferStatus
	 *            the new transfer status
	 */
	public void setTransferStatus(final TransferStatusModel transferStatus)
	{
		this.transferStatus = transferStatus;
	}

	/**
	 * Gets the transfer type.
	 *
	 * @return the transfer type
	 */
	public TransferTypeModel getTransferType()
	{
		return this.transferType;
	}

	/**
	 * Sets the transfer type.
	 *
	 * @param transferType
	 *            the new transfer type
	 */
	public void setTransferType(final TransferTypeModel transferType)
	{
		this.transferType = transferType;
	}

	/**
	 * Gets the vendor type.
	 *
	 * @return the vendor type
	 */
	public VendorTypeModel getVendorType()
	{
		return this.vendorType;
	}

	/**
	 * Sets the vendor type.
	 *
	 * @param vendorType
	 *            the new vendor type
	 */
	public void setVendorType(final VendorTypeModel vendorType)
	{
		this.vendorType = vendorType;
	}

	/**
	 * Gets the total amount.
	 *
	 * @return the total amount
	 */
	public double getTotalAmount()
	{
		return amount + cgst + igst + sgst + serviceCharge;
	}

	/**
	 * Gets the bank detail.
	 *
	 * @return the bank detail
	 */
	public BankDetailModel getBankDetail()
	{
		return bankDetail;
	}

	/**
	 * Sets the bank detail.
	 *
	 * @param bankDetail
	 *            the new bank detail
	 */
	public void setBankDetail(final BankDetailModel bankDetail)
	{
		this.bankDetail = bankDetail;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */

	public void setTotalAmount(final double totalAmount)
	{
		this.totalAmount = totalAmount;
	}

	public String getDetails()
	{
		return details;
	}

	public void setDetails(final String details)
	{
		this.details = details;
	}

	
	
	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	@Override
	public String toString()
	{
		return "TransactionModel [id=" + id + ", amount=" + amount + ", cgst=" + cgst + ", createdBy=" + createdBy
				+ ", createdDate=" + createdDate + ", igst=" + igst + ", kouchanReferenceNumber="
				+ kouchanReferenceNumber + ", modifiedBy=" + modifiedBy + ", modifiedDate=" + modifiedDate
				+ ", serviceCharge=" + serviceCharge + ", sgst=" + sgst + ", vendorOrGroupReferenceNumber="
				+ vendorOrGroupReferenceNumber + ", customer=" + customer + ", fundType=" + fundType
				+ ", transferStatus=" + transferStatus + ", transferType=" + transferType + ", vendorType=" + vendorType
				+ ", totalAmount=" + getTotalAmount() + ", bankDetail=" + bankDetail + "]";
	}

}