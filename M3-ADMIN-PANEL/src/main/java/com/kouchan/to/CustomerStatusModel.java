/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the customer_status database table.
 *
 */
@Entity
@Table(name = "customer_status")
@NamedQuery(name = "CustomerStatusModel.findAll", query = "SELECT c FROM CustomerStatusModel c")
public class CustomerStatusModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@Column(name = "id")
	private Long id;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The description. */
	@Column(name = "description")
	private String description;

	/** The is deleted. */
	@Column(name = "IS_DELETED", columnDefinition = "TINYINT(1) default 0 ")
	private byte isDeleted;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The name. */
	@Column(name = "name")
	private String name;

	/** The customers. */
	// TODO: To make one to one bidrectional relationship with customer
	// bi-directional many-to-one association to Customer
	@OneToMany(mappedBy = "customerStatus")
	private List<CustomerModel> customers;

	/**
	 * Instantiates a new customer status.
	 */
	public CustomerStatusModel()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id)
	{
		this.id = id;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription()
	{
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description
	 *            the new description
	 */
	public void setDescription(final String description)
	{
		this.description = description;
	}

	/**
	 * Gets the checks if is deleted.
	 *
	 * @return the checks if is deleted
	 */
	public byte getIsDeleted()
	{
		return this.isDeleted;
	}

	/**
	 * Sets the checks if is deleted.
	 *
	 * @param isDeleted
	 *            the new checks if is deleted
	 */
	public void setIsDeleted(final byte isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * Gets the customers.
	 *
	 * @return the customers
	 */
	public List<CustomerModel> getCustomers()
	{
		return this.customers;
	}

	/**
	 * Sets the customers.
	 *
	 * @param customers
	 *            the new customers
	 */
	public void setCustomers(final List<CustomerModel> customers)
	{
		this.customers = customers;
	}

	/**
	 * Adds the customer.
	 *
	 * @param customer
	 *            the customer
	 * @return the customer
	 */
	public CustomerModel addCustomer(final CustomerModel customer)
	{
		getCustomers().add(customer);
		customer.setCustomerStatus(this);

		return customer;
	}

	/**
	 * Removes the customer.
	 *
	 * @param customer
	 *            the customer
	 * @return the customer
	 */
	public CustomerModel removeCustomer(final CustomerModel customer)
	{
		getCustomers().remove(customer);
		customer.setCustomerStatus(null);

		return customer;
	}

}