/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the transfer_status database table.
 *
 */
@Entity
@Table(name = "transfer_status")
@NamedQuery(name = "TransferStatusModel.findAll", query = "SELECT t FROM TransferStatusModel t")
public class TransferStatusModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	private Long id;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The description. */
	private String description;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The name. */
	private String name;

	/** The transactions. */
	// bi-directional many-to-one association to Transaction
	@OneToMany(mappedBy = "transferStatus")
	private List<TransactionModel> transactions;

	/** The transaction histories. */
	// bi-directional many-to-one association to TransactionHistory
	@OneToMany(mappedBy = "transferStatus")
	private List<TransactionHistoryModel> transactionHistories;

	/** The vendor type. */
	// bi-directional many-to-one association to VendorType
	@ManyToOne
	@JoinColumn(name = "VENDOR_ID")
	private VendorTypeModel vendorType;

	/**
	 * Instantiates a new transfer status.
	 */
	public TransferStatusModel()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id)
	{
		this.id = id;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription()
	{
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description
	 *            the new description
	 */
	public void setDescription(final String description)
	{
		this.description = description;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * Gets the transactions.
	 *
	 * @return the transactions
	 */
	public List<TransactionModel> getTransactions()
	{
		return this.transactions;
	}

	/**
	 * Sets the transactions.
	 *
	 * @param transactions
	 *            the new transactions
	 */
	public void setTransactions(final List<TransactionModel> transactions)
	{
		this.transactions = transactions;
	}

	/**
	 * Adds the transaction.
	 *
	 * @param transaction
	 *            the transaction
	 * @return the transaction
	 */
	public TransactionModel addTransaction(final TransactionModel transaction)
	{
		getTransactions().add(transaction);
		transaction.setTransferStatus(this);

		return transaction;
	}

	/**
	 * Removes the transaction.
	 *
	 * @param transaction
	 *            the transaction
	 * @return the transaction
	 */
	public TransactionModel removeTransaction(final TransactionModel transaction)
	{
		getTransactions().remove(transaction);
		transaction.setTransferStatus(null);

		return transaction;
	}

	/**
	 * Gets the transaction histories.
	 *
	 * @return the transaction histories
	 */
	public List<TransactionHistoryModel> getTransactionHistories()
	{
		return this.transactionHistories;
	}

	/**
	 * Sets the transaction histories.
	 *
	 * @param transactionHistories
	 *            the new transaction histories
	 */
	public void setTransactionHistories(final List<TransactionHistoryModel> transactionHistories)
	{
		this.transactionHistories = transactionHistories;
	}

	/**
	 * Adds the transaction history.
	 *
	 * @param transactionHistory
	 *            the transaction history
	 * @return the transaction history
	 */
	public TransactionHistoryModel addTransactionHistory(final TransactionHistoryModel transactionHistory)
	{
		getTransactionHistories().add(transactionHistory);
		transactionHistory.setTransferStatus(this);

		return transactionHistory;
	}

	/**
	 * Removes the transaction history.
	 *
	 * @param transactionHistory
	 *            the transaction history
	 * @return the transaction history
	 */
	public TransactionHistoryModel removeTransactionHistory(final TransactionHistoryModel transactionHistory)
	{
		getTransactionHistories().remove(transactionHistory);
		transactionHistory.setTransferStatus(null);

		return transactionHistory;
	}

	/**
	 * Gets the vendor type.
	 *
	 * @return the vendor type
	 */
	public VendorTypeModel getVendorType()
	{
		return this.vendorType;
	}

	/**
	 * Sets the vendor type.
	 *
	 * @param vendorType
	 *            the new vendor type
	 */
	public void setVendorType(final VendorTypeModel vendorType)
	{
		this.vendorType = vendorType;
	}

}