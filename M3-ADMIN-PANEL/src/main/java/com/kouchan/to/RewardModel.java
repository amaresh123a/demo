/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the commission database table.
 * 
 */
@Entity
@Table(name = "reward")
@NamedQuery(name = "RewardModel.findAll", query = "SELECT r FROM RewardModel r")
public class RewardModel implements Serializable
{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @Column(name = "ID")
    private long id;

    /** The created by. */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** The created date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATED_DATE")
    private Date createdDate;

    /** The end date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "END_DATE")
    private Date endDate;

    /** The is active. */
    @Column(name = "IS_ACTIVE")
    private byte isActive;

    /** The reward amount. */
    @Column(name = "AMOUNT")
    private double amount;

    /** The modified by. */
    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    /** The modified date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    /** The start date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "START_DATE")
    private Date startDate;
    
    /** The action. */
    @Column(name = "ACTION")
    private String action;

    /** The transfer type. */
    // bi-directional many-to-one association to TransferType
    @ManyToOne
    @JoinColumn(name = "CUSTOMER_ID")
    private CustomerModel customerModel;

    /**
     * Instantiates a new commission.
     */
    public RewardModel()
    {
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public long getId()
    {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(final long id)
    {
        this.id = id;
    }

    /**
     * Gets the created by.
     *
     * @return the created by
     */
    public String getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Sets the created by.
     *
     * @param createdBy
     *            the new created by
     */
    public void setCreatedBy(final String createdBy)
    {
        this.createdBy = createdBy;
    }

    /**
     * Gets the created date.
     *
     * @return the created date
     */
    public Date getCreatedDate()
    {
        return this.createdDate;
    }

    /**
     * Sets the created date.
     *
     * @param createdDate
     *            the new created date
     */
    public void setCreatedDate(final Date createdDate)
    {
        this.createdDate = createdDate;
    }

    /**
     * Gets the end date.
     *
     * @return the end date
     */
    public Date getEndDate()
    {
        return this.endDate;
    }

    /**
     * Sets the end date.
     *
     * @param endDate
     *            the new end date
     */
    public void setEndDate(final Date endDate)
    {
        this.endDate = endDate;
    }

    /**
     * Gets the checks if is active.
     *
     * @return the checks if is active
     */
    public byte getIsActive()
    {
        return this.isActive;
    }

    /**
     * Sets the checks if is active.
     *
     * @param isActive
     *            the new checks if is active
     */
    public void setIsActive(final byte isActive)
    {
        this.isActive = isActive;
    }

    /**
     * Gets the modified by.
     *
     * @return the modified by
     */
    public String getModifiedBy()
    {
        return this.modifiedBy;
    }

    /**
     * Sets the modified by.
     *
     * @param modifiedBy
     *            the new modified by
     */
    public void setModifiedBy(final String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    /**
     * Gets the modified date.
     *
     * @return the modified date
     */
    public Date getModifiedDate()
    {
        return this.modifiedDate;
    }

    /**
     * Sets the modified date.
     *
     * @param modifiedDate
     *            the new modified date
     */
    public void setModifiedDate(final Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public Date getStartDate()
    {
        return this.startDate;
    }

    /**
     * Sets the start date.
     *
     * @param startDate
     *            the new start date
     */
    public void setStartDate(final Date startDate)
    {
        this.startDate = startDate;
    }

    /**
     * Gets the amount.
     *
     * @return the amount
     */
    public double getAmount()
    {
        return amount;
    }

    /**
     * Sets the amount.
     *
     * @param amount
     *            the new amount
     */
    public void setAmount(double amount)
    {
        this.amount = amount;
    }

    /**
     * Gets the customer model.
     *
     * @return the customer model
     */
    public CustomerModel getCustomerModel()
    {
        return customerModel;
    }

    /**
     * Sets the customer model.
     *
     * @param customerModel
     *            the new customer model
     */
    public void setCustomerModel(CustomerModel customerModel)
    {
        this.customerModel = customerModel;
    }

    /**
     * Gets the action.
     *
     * @return the action
     */
    public String getAction()
    {
        return action;
    }

    /**
     * Sets the action.
     *
     * @param action the new action
     */
    public void setAction(String action)
    {
        this.action = action;
    }
    
    

}