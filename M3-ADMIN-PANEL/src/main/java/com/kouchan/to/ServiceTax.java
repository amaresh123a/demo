/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the service_tax database table.
 * 
 */
@Entity
@Table(name = "service_tax")
@NamedQuery(name = "ServiceTax.findAll", query = "SELECT s FROM ServiceTax s")
public class ServiceTax implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	private String id;

	/** The name. */
	private String name;

	/** The rate. */
	private double rate;

	/**
	 * Instantiates a new service tax.
	 */
	public ServiceTax()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final String id)
	{
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * Gets the rate.
	 *
	 * @return the rate
	 */
	public double getRate()
	{
		return this.rate;
	}

	/**
	 * Sets the rate.
	 *
	 * @param rate
	 *            the new rate
	 */
	public void setRate(final double rate)
	{
		this.rate = rate;
	}

}