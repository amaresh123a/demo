/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the transfer_type database table.
 *
 */
@Entity
@Table(name = "transfer_type")
@NamedQuery(name = "TransferTypeModel.findAll", query = "SELECT t FROM TransferTypeModel t")
public class TransferTypeModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@Column(name = "id")
	private Long id;

	/** The business process time. */
	@Temporal(TemporalType.DATE)
	@Column(name = "BUSINESS_PROCESS_TIME")
	private Date businessProcessTime;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The name. */
	@Column(name = "name")
	private String name;

	/** The commissions. */
	// bi-directional many-to-one association to Commission
	@OneToMany(mappedBy = "transferType")
	private List<CommissionModel> commissions;

	/** The transactions. */
	// bi-directional many-to-one association to Transaction
	@OneToMany(mappedBy = "transferType")
	private List<TransactionModel> transactions;

	/** The vendor type. */
	// bi-directional many-to-one association to VendorType
	@ManyToOne
	@JoinColumn(name = "VENDOR_TYPE_ID")
	private VendorTypeModel vendorType;

	/**
	 * Instantiates a new transfer type.
	 */
	public TransferTypeModel()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id)
	{
		this.id = id;
	}

	/**
	 * Gets the business process time.
	 *
	 * @return the business process time
	 */
	public Date getBusinessProcessTime()
	{
		return this.businessProcessTime;
	}

	/**
	 * Sets the business process time.
	 *
	 * @param businessProcessTime
	 *            the new business process time
	 */
	public void setBusinessProcessTime(final Date businessProcessTime)
	{
		this.businessProcessTime = businessProcessTime;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * Gets the commissions.
	 *
	 * @return the commissions
	 */
	public List<CommissionModel> getCommissions()
	{
		return this.commissions;
	}

	/**
	 * Sets the commissions.
	 *
	 * @param commissions
	 *            the new commissions
	 */
	public void setCommissions(final List<CommissionModel> commissions)
	{
		this.commissions = commissions;
	}

	/**
	 * Adds the commission.
	 *
	 * @param commission
	 *            the commission
	 * @return the commission
	 */
	public CommissionModel addCommission(final CommissionModel commission)
	{
		getCommissions().add(commission);
		commission.setTransferType(this);

		return commission;
	}

	/**
	 * Removes the commission.
	 *
	 * @param commission
	 *            the commission
	 * @return the commission
	 */
	public CommissionModel removeCommission(final CommissionModel commission)
	{
		getCommissions().remove(commission);
		commission.setTransferType(null);

		return commission;
	}

	/**
	 * Gets the transactions.
	 *
	 * @return the transactions
	 */
	public List<TransactionModel> getTransactions()
	{
		return this.transactions;
	}

	/**
	 * Sets the transactions.
	 *
	 * @param transactions
	 *            the new transactions
	 */
	public void setTransactions(final List<TransactionModel> transactions)
	{
		this.transactions = transactions;
	}

	/**
	 * Adds the transaction.
	 *
	 * @param transaction
	 *            the transaction
	 * @return the transaction
	 */
	public TransactionModel addTransaction(final TransactionModel transaction)
	{
		getTransactions().add(transaction);
		transaction.setTransferType(this);

		return transaction;
	}

	/**
	 * Removes the transaction.
	 *
	 * @param transaction
	 *            the transaction
	 * @return the transaction
	 */
	public TransactionModel removeTransaction(final TransactionModel transaction)
	{
		getTransactions().remove(transaction);
		transaction.setTransferType(null);

		return transaction;
	}

	/**
	 * Gets the vendor type.
	 *
	 * @return the vendor type
	 */
	public VendorTypeModel getVendorType()
	{
		return this.vendorType;
	}

	/**
	 * Sets the vendor type.
	 *
	 * @param vendorType
	 *            the new vendor type
	 */
	public void setVendorType(final VendorTypeModel vendorType)
	{
		this.vendorType = vendorType;
	}

}