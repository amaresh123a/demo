/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the customer_unregistered database table.
 *
 */
@Entity
@Table(name = "customer_unregistered")
@NamedQuery(name = "CustomerUnregistered.findAll", query = "SELECT c FROM CustomerUnregistered c")
public class CustomerUnregistered implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The amount. */
	@Column(name = "amount")
	private double amount;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	/** The mobile. */
	@Column(name = "mobile")
	private String mobile;

	/** The name. */
	@Column(name = "name")
	private String name;

	/** The registered date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "registered_date")
	private Date registeredDate;

	/**
	 * Instantiates a new customer unregistered.
	 */
	public CustomerUnregistered()
	{
	}

	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public double getAmount()
	{
		return this.amount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param amount
	 *            the new amount
	 */
	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id)
	{
		this.id = id;
	}

	/**
	 * Gets the mobile.
	 *
	 * @return the mobile
	 */
	public String getMobile()
	{
		return this.mobile;
	}

	/**
	 * Sets the mobile.
	 *
	 * @param mobile
	 *            the new mobile
	 */
	public void setMobile(final String mobile)
	{
		this.mobile = mobile;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * Gets the registered date.
	 *
	 * @return the registered date
	 */
	public Date getRegisteredDate()
	{
		return this.registeredDate;
	}

	/**
	 * Sets the registered date.
	 *
	 * @param registeredDate
	 *            the new registered date
	 */
	public void setRegisteredDate(final Date registeredDate)
	{
		this.registeredDate = registeredDate;
	}

}