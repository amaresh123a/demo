/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the contact_type database table.
 *
 */
@Entity
@Table(name = "contact_type")
@NamedQuery(name = "ContactTypeModel.findAll", query = "SELECT c FROM ContactTypeModel c")
public class ContactTypeModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@Column(name = "id")
	private Long id;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The name. */
	@Column(name = "name")
	private String name;

	/** The contact details. */
	// bi-directional many-to-one association to ContactDetail
	@OneToMany(mappedBy = "contactType")
	private List<ContactDetailModel> contactDetails;

	/**
	 * Instantiates a new contact type.
	 */
	public ContactTypeModel()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id)
	{
		this.id = id;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * Gets the contact details.
	 *
	 * @return the contact details
	 */
	public List<ContactDetailModel> getContactDetails()
	{
		return this.contactDetails;
	}

	/**
	 * Sets the contact details.
	 *
	 * @param contactDetails
	 *            the new contact details
	 */
	public void setContactDetails(final List<ContactDetailModel> contactDetails)
	{
		this.contactDetails = contactDetails;
	}

	/**
	 * Adds the contact detail.
	 *
	 * @param contactDetail
	 *            the contact detail
	 * @return the contact detail
	 */
	public ContactDetailModel addContactDetail(final ContactDetailModel contactDetail)
	{
		getContactDetails().add(contactDetail);
		contactDetail.setContactType(this);

		return contactDetail;
	}

	/**
	 * Removes the contact detail.
	 *
	 * @param contactDetail
	 *            the contact detail
	 * @return the contact detail
	 */
	public ContactDetailModel removeContactDetail(final ContactDetailModel contactDetail)
	{
		getContactDetails().remove(contactDetail);
		contactDetail.setContactType(null);

		return contactDetail;
	}

}