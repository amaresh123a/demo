package com.kouchan.to;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the customer database table.
 *
 */
@Entity
@Table(name = "customer")
@NamedQuery(name = "CustomerModel.findAll", query = "SELECT c FROM CustomerModel c")
public class CustomerModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/** The avatar. */
	@Column(name = "avatar")
	private String avatar;

	
	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@CreatedDate
	@Column(name = "CREATED_DATE")
	@JsonFormat(locale = "en-IN", shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm", timezone = "GMT+5:30")
	private Date createdDate;

	/** The email. */
	@Column(name = "email")
	private String email;

    /** The date of birth. */
    @Column(name = "DATE_OF_BIRTH")
    @JsonFormat(locale = "en-IN", shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm", timezone = "GMT+5:30")
    private Date dateOfBirth;

    /** The gender. */
    @Column(name = "GENDER")
    private String gender;

	/** The encryption type. */
	@Column(name = "ENCRYPTION_TYPE")
	private String encryptionType;

	/** The fcm token. */
	@Column(name = "FCM_TOKEN")
	private String fcmToken;

	/** The is deleted. */
	@Column(name = "IS_DELETED", columnDefinition = "TINYINT(1) default 0 ")
	private boolean isDeleted;

	/** The mobile. */
	@Column(name = "mobile")
	private String mobile;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The name. */
	@Column(name = "name")
	private String name;

	/** The password. */
	@Column(name = "password")
	private String password;

    /** The transaction count. */
    @Column(name = "TRANSACTION_COUNT")
    private long transactionCount;

    /** The customer referrer ID. */
    @ManyToOne
    @JoinColumn(name = "CUSTOMER_REFERRER")
    private CustomerModel customerReferrer;

    /** The addresses. */
    // bi-directional many-to-one association to Address
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    private List<AddressModel> addresses = new ArrayList<AddressModel>();

	/** The bank details. */
	// bi-directional many-to-one association to BankDetail
	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<BankDetailModel> bankDetails = new ArrayList<BankDetailModel>();

	/** The contact details. */
	// bi-directional many-to-one association to ContactDetail
	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
	private List<ContactDetailModel> contactDetails = new ArrayList<>();

	/** The customer status. */
	// bi-directional many-to-one association to CustomerStatus
	@ManyToOne
	@JoinColumn(name = "CUSTOMER_STATUS_ID")
	private CustomerStatusModel customerStatus;

	/** The customer type. */
	// bi-directional many-to-one association to CustomerType
	@ManyToOne
	@JoinColumn(name = "CUSTOMER_TYPE_ID")
	private CustomerTypeModel customerType;

    /** The account type. */
    @ManyToOne
    @JoinColumn(name = "account_type_id")
    private CustomerTypeModel accountType;

	/** The wallet balance. */
	// bi-directional many-to-one association to WalletBalance
	@OneToOne
	@JoinColumn(name = "WALLET_BALANCE_ID")
	private WalletBalanceModel walletBalance;

	/** The kyc details. */
	// bi-directional many-to-one association to KycDetail
	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
	private List<KycDetail> kycDetails;

	/** The login histories. */
	// bi-directional many-to-one association to LoginHistory
	@OneToMany(mappedBy = "customer")
	private List<LoginHistory> loginHistories;

	/** The transactions. */
	// bi-directional many-to-one association to Transaction
	@OneToMany(mappedBy = "customer")
	@JsonIgnore
	private List<TransactionModel> transactions;

    /**
     * Instantiates a new customer.
     */
    public CustomerModel()
    {
    }

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id)
	{
		this.id = id;
	}

	/**
	 * Gets the avatar.
	 *
	 * @return the avatar
	 */
	public String getAvatar()
	{
		return this.avatar;
	}

	/**
	 * Sets the avatar.
	 *
	 * @param avatar
	 *            the new avatar
	 */
	public void setAvatar(final String avatar)
	{
		this.avatar = avatar;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail()
	{
		return this.email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the new email
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

    /**
     * Gets the date of birth.
     *
     * @return the date of birth
     */
    public Date getDateOfBirth()
    {
        return dateOfBirth;
    }

    /**
     * Sets the date of birth.
     *
     * @param dateOfBirth
     *            the new date of birth
     */
    public void setDateOfBirth(final Date dateOfBirth)
    {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Gets the gender.
     *
     * @return the gender
     */
    public String getGender()
    {
        return gender;
    }

    /**
     * Sets the gender.
     *
     * @param gender
     *            the new gender
     */
    public void setGender(final String gender)
    {
        this.gender = gender;
    }

    /**
     * Sets the deleted.
     *
     * @param isDeleted
     *            the new deleted
     */
    public void setDeleted(final boolean isDeleted)
    {
        this.isDeleted = isDeleted;
    }

	/**
	 * Gets the encryption type.
	 *
	 * @return the encryption type
	 */
	public String getEncryptionType()
	{
		return this.encryptionType;
	}

	/**
	 * Sets the encryption type.
	 *
	 * @param encryptionType
	 *            the new encryption type
	 */
	public void setEncryptionType(final String encryptionType)
	{
		this.encryptionType = encryptionType;
	}

	/**
	 * Gets the fcm token.
	 *
	 * @return the fcm token
	 */
	public String getFcmToken()
	{
		return this.fcmToken;
	}

	/**
	 * Sets the fcm token.
	 *
	 * @param fcmToken
	 *            the new fcm token
	 */
	public void setFcmToken(final String fcmToken)
	{
		this.fcmToken = fcmToken;
	}

	/**
	 * Gets the checks if is deleted.
	 *
	 * @return the checks if is deleted
	 */
	public boolean getIsDeleted()
	{
		return this.isDeleted;
	}

	/**
	 * Sets the checks if is deleted.
	 *
	 * @param isDeleted
	 *            the new checks if is deleted
	 */
	public void setIsDeleted(final boolean isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Gets the mobile.
	 *
	 * @return the mobile
	 */
	public String getMobile()
	{
		return this.mobile;
	}

	/**
	 * Sets the mobile.
	 *
	 * @param mobile
	 *            the new mobile
	 */
	public void setMobile(final String mobile)
	{
		this.mobile = mobile;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}


	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword()
	{
		return this.password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(final String password)
	{
		this.password = password;
	}

	/**
	 * Gets the addresses.
	 *
	 * @return the addresses
	 */
	public List<AddressModel> getAddresses()
	{
		return this.addresses;
	}

	/**
	 * Sets the addresses.
	 *
	 * @param addresses
	 *            the new addresses
	 */
	public void setAddresses(final List<AddressModel> addresses)
	{
		this.addresses = addresses;
	}

	/**
	 * Adds the address.
	 *
	 * @param address
	 *            the address
	 * @return the address
	 */
	public AddressModel addAddress(final AddressModel address)
	{
		getAddresses().add(address);
		address.setCustomer(this);

		return address;
	}

	/**
	 * Removes the address.
	 *
	 * @param address
	 *            the address
	 * @return the address
	 */
	public AddressModel removeAddress(final AddressModel address)
	{
		getAddresses().remove(address);
		address.setCustomer(null);

		return address;
	}

	/**
	 * Gets the bank details.
	 *
	 * @return the bank details
	 */
	public List<BankDetailModel> getBankDetails()
	{
		return this.bankDetails;
	}

	/**
	 * Sets the bank details.
	 *
	 * @param bankDetails
	 *            the new bank details
	 */
	public void setBankDetails(final List<BankDetailModel> bankDetails)
	{
		this.bankDetails = bankDetails;
	}

	/**
	 * Adds the bank detail.
	 *
	 * @param bankDetail
	 *            the bank detail
	 * @return the bank detail
	 */
	public BankDetailModel addBankDetail(final BankDetailModel bankDetail)
	{
		getBankDetails().add(bankDetail);
		bankDetail.setCustomer(this);

		return bankDetail;
	}

	/**
	 * Removes the bank detail.
	 *
	 * @param bankDetail
	 *            the bank detail
	 * @return the bank detail
	 */
	public BankDetailModel removeBankDetail(final BankDetailModel bankDetail)
	{
		getBankDetails().remove(bankDetail);
		bankDetail.setCustomer(null);

		return bankDetail;
	}

	/**
	 * Gets the contact details.
	 *
	 * @return the contact details
	 */
	public List<ContactDetailModel> getContactDetails()
	{
		return this.contactDetails;
	}

	/**
	 * Sets the contact details.
	 *
	 * @param contactDetails
	 *            the new contact details
	 */
	public void setContactDetails(final List<ContactDetailModel> contactDetails)
	{
		this.contactDetails = contactDetails;
	}

	/**
	 * Adds the contact detail.
	 *
	 * @param contactDetail
	 *            the contact detail
	 * @return the contact detail
	 */
	public ContactDetailModel addContactDetail(final ContactDetailModel contactDetail)
	{
		getContactDetails().add(contactDetail);
		contactDetail.setCustomer(this);

		return contactDetail;
	}

	/**
	 * Removes the contact detail.
	 *
	 * @param contactDetail
	 *            the contact detail
	 * @return the contact detail
	 */
	public ContactDetailModel removeContactDetail(final ContactDetailModel contactDetail)
	{
		getContactDetails().remove(contactDetail);
		contactDetail.setCustomer(null);

		return contactDetail;
	}

	/**
	 * Gets the customer status.
	 *
	 * @return the customer status
	 */
	public CustomerStatusModel getCustomerStatus()
	{
		return this.customerStatus;
	}

	/**
	 * Sets the customer status.
	 *
	 * @param customerStatus
	 *            the new customer status
	 */
	public void setCustomerStatus(final CustomerStatusModel customerStatus)
	{
		this.customerStatus = customerStatus;
	}

	/**
	 * Gets the customer type.
	 *
	 * @return the customer type
	 */
	public CustomerTypeModel getCustomerType()
	{
		return this.customerType;
	}

	/**
	 * Sets the customer type.
	 *
	 * @param customerType
	 *            the new customer type
	 */
	public void setCustomerType(final CustomerTypeModel customerType)
	{
		this.customerType = customerType;
	}

    /**
     * Gets the account type.
     *
     * @return the account type
     */
    public CustomerTypeModel getAccountType()
    {
        return this.accountType;
    }

    /**
     * Sets the account type.
     *
     * @param accountType
     *            the new account type
     */
    public void setAccountType(final CustomerTypeModel accountType)
    {
        this.accountType = accountType;
    }

	/**
	 * Gets the wallet balance.
	 *
	 * @return the wallet balance
	 */
	public WalletBalanceModel getWalletBalance()
	{
		return this.walletBalance;
	}

	/**
	 * Sets the wallet balance.
	 *
	 * @param walletBalance
	 *            the new wallet balance
	 */
	public void setWalletBalance(final WalletBalanceModel walletBalance)
	{
		this.walletBalance = walletBalance;
	}

	/**
	 * Gets the kyc details.
	 *
	 * @return the kyc details
	 */
	public List<KycDetail> getKycDetails()
	{
		return this.kycDetails;
	}

	/**
	 * Sets the kyc details.
	 *
	 * @param kycDetails
	 *            the new kyc details
	 */
	public void setKycDetails(final List<KycDetail> kycDetails)
	{
		this.kycDetails = kycDetails;
	}

	/**
	 * Adds the kyc detail.
	 *
	 * @param kycDetail
	 *            the kyc detail
	 * @return the kyc detail
	 */
	public KycDetail addKycDetail(final KycDetail kycDetail)
	{
		getKycDetails().add(kycDetail);
		kycDetail.setCustomer(this);

		return kycDetail;
	}

	/**
	 * Removes the kyc detail.
	 *
	 * @param kycDetail
	 *            the kyc detail
	 * @return the kyc detail
	 */
	public KycDetail removeKycDetail(final KycDetail kycDetail)
	{
		getKycDetails().remove(kycDetail);
		kycDetail.setCustomer(null);

		return kycDetail;
	}

	/**
	 * Gets the login histories.
	 *
	 * @return the login histories
	 */
	public List<LoginHistory> getLoginHistories()
	{
		return this.loginHistories;
	}

	/**
	 * Sets the login histories.
	 *
	 * @param loginHistories
	 *            the new login histories
	 */
	public void setLoginHistories(final List<LoginHistory> loginHistories)
	{
		this.loginHistories = loginHistories;
	}

	/**
	 * Adds the login history.
	 *
	 * @param loginHistory
	 *            the login history
	 * @return the login history
	 */
	public LoginHistory addLoginHistory(final LoginHistory loginHistory)
	{
		getLoginHistories().add(loginHistory);
		loginHistory.setCustomer(this);

		return loginHistory;
	}

	/**
	 * Removes the login history.
	 *
	 * @param loginHistory
	 *            the login history
	 * @return the login history
	 */
	public LoginHistory removeLoginHistory(final LoginHistory loginHistory)
	{
		getLoginHistories().remove(loginHistory);
		loginHistory.setCustomer(null);

		return loginHistory;
	}

	/**
	 * Gets the transactions.
	 *
	 * @return the transactions
	 */
	public List<TransactionModel> getTransactions()
	{
		return this.transactions;
	}

	/**
	 * Sets the transactions.
	 *
	 * @param transactions
	 *            the new transactions
	 */
	public void setTransactions(final List<TransactionModel> transactions)
	{
		this.transactions = transactions;
	}

	/**
	 * Adds the transaction.
	 *
	 * @param transaction
	 *            the transaction
	 * @return the transaction
	 */
	public TransactionModel addTransaction(final TransactionModel transaction)
	{
		getTransactions().add(transaction);
		transaction.setCustomer(this);

		return transaction;
	}

	/**
	 * Removes the transaction.
	 *
	 * @param transaction
	 *            the transaction
	 * @return the transaction
	 */
	public TransactionModel removeTransaction(final TransactionModel transaction)
	{
		getTransactions().remove(transaction);
		transaction.setCustomer(null);

		return transaction;
	}

    /**
     * Gets the transaction count.
     *
     * @return the transaction count
     */
    public long getTransactionCount()
    {
        return transactionCount;
    }

    /**
     * Sets the transaction count.
     *
     * @param transactionCount
     *            the new transaction count
     */
    public void setTransactionCount(long transactionCount)
    {
        this.transactionCount = transactionCount;
    }

    /**
     * Gets the customer referrer.
     *
     * @return the customer referrer 
     */
    public CustomerModel getCustomerReferrer()
    {
        return customerReferrer;
    }

    /**
     * Sets the customer referrer.
     *
     * @param customerReferrer
     *            the new customer referrer
     */
    public void setCustomerReferrer(CustomerModel customerReferrer)
    {
        this.customerReferrer= customerReferrer;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


}