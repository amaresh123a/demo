/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the vendor_type database table.
 *
 */
@Entity
@Table(name = "vendor_type")
@NamedQuery(name = "VendorTypeModel.findAll", query = "SELECT v FROM VendorTypeModel v")
public class VendorTypeModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@Column(name = "id")
	private Long id;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The name. */
	@Column(name = "name")
	private String name;

	/** The transactions. */
	// bi-directional many-to-one association to Transaction
	@OneToMany(mappedBy = "vendorType")
	private List<TransactionModel> transactions;

	/** The transfer statuses. */
	// bi-directional many-to-one association to TransferStatus
	@OneToMany(mappedBy = "vendorType")
	private List<TransferStatusModel> transferStatuses;

	/** The transfer types. */
	// bi-directional many-to-one association to TransferType
	@OneToMany(mappedBy = "vendorType")
	private List<TransferTypeModel> transferTypes;

	/**
	 * Instantiates a new vendor type.
	 */
	public VendorTypeModel()
	{
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId()
	{
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id)
	{
		this.id = id;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * Gets the transactions.
	 *
	 * @return the transactions
	 */
	public List<TransactionModel> getTransactions()
	{
		return this.transactions;
	}

	/**
	 * Sets the transactions.
	 *
	 * @param transactions
	 *            the new transactions
	 */
	public void setTransactions(final List<TransactionModel> transactions)
	{
		this.transactions = transactions;
	}

	/**
	 * Adds the transaction.
	 *
	 * @param transaction
	 *            the transaction
	 * @return the transaction
	 */
	public TransactionModel addTransaction(final TransactionModel transaction)
	{
		getTransactions().add(transaction);
		transaction.setVendorType(this);

		return transaction;
	}

	/**
	 * Removes the transaction.
	 *
	 * @param transaction
	 *            the transaction
	 * @return the transaction
	 */
	public TransactionModel removeTransaction(final TransactionModel transaction)
	{
		getTransactions().remove(transaction);
		transaction.setVendorType(null);

		return transaction;
	}

	/**
	 * Gets the transfer statuses.
	 *
	 * @return the transfer statuses
	 */
	public List<TransferStatusModel> getTransferStatuses()
	{
		return this.transferStatuses;
	}

	/**
	 * Sets the transfer statuses.
	 *
	 * @param transferStatuses
	 *            the new transfer statuses
	 */
	public void setTransferStatuses(final List<TransferStatusModel> transferStatuses)
	{
		this.transferStatuses = transferStatuses;
	}

	/**
	 * Adds the transfer status.
	 *
	 * @param transferStatus
	 *            the transfer status
	 * @return the transfer status
	 */
	public TransferStatusModel addTransferStatus(final TransferStatusModel transferStatus)
	{
		getTransferStatuses().add(transferStatus);
		transferStatus.setVendorType(this);

		return transferStatus;
	}

	/**
	 * Removes the transfer status.
	 *
	 * @param transferStatus
	 *            the transfer status
	 * @return the transfer status
	 */
	public TransferStatusModel removeTransferStatus(final TransferStatusModel transferStatus)
	{
		getTransferStatuses().remove(transferStatus);
		transferStatus.setVendorType(null);

		return transferStatus;
	}

	/**
	 * Gets the transfer types.
	 *
	 * @return the transfer types
	 */
	public List<TransferTypeModel> getTransferTypes()
	{
		return this.transferTypes;
	}

	/**
	 * Sets the transfer types.
	 *
	 * @param transferTypes
	 *            the new transfer types
	 */
	public void setTransferTypes(final List<TransferTypeModel> transferTypes)
	{
		this.transferTypes = transferTypes;
	}

	/**
	 * Adds the transfer type.
	 *
	 * @param transferType
	 *            the transfer type
	 * @return the transfer type
	 */
	public TransferTypeModel addTransferType(final TransferTypeModel transferType)
	{
		getTransferTypes().add(transferType);
		transferType.setVendorType(this);

		return transferType;
	}

	/**
	 * Removes the transfer type.
	 *
	 * @param transferType
	 *            the transfer type
	 * @return the transfer type
	 */
	public TransferTypeModel removeTransferType(final TransferTypeModel transferType)
	{
		getTransferTypes().remove(transferType);
		transferType.setVendorType(null);

		return transferType;
	}

}