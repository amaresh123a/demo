/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the user_group_members_map database table.
 *
 */
@Entity
@Table(name = "user_group_members_map")
@NamedQuery(name = "UserGroupMembersMap.findAll", query = "SELECT u FROM UserGroupMembersMap u")
@IdClass(value = UserGroupMapKey.class)
public class UserGroupMembersMap implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

    /** The id. */

    @Id
    @Column(name = "group_id")
    private Long groupId;

    @Id
    @Column(name = "group_member_id")
    private Long groupMemberId;

    /** The created by. */
    @Column(name = "CREATED_BY")
    private String createdBy;

	/** The created date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The group member mobile. */
	@Column(name = "GROUP_MEMBER_MOBILE")
	private String groupMemberMobile;

	/** The group member name. */
	@Column(name = "GROUP_MEMBER_NAME")
	private String groupMemberName;

	/** The is deleted. */
	@Column(name = "IS_DELETED", columnDefinition = "TINYINT(1) default 0 ")
	private byte isDeleted;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

    /**
     * Instantiates a new user group members map.
     */
    public UserGroupMembersMap()
    {
    }

    public Long getGroupId()
    {
        return groupId;
    }

    public void setGroupId(final Long groupId)
    {
        this.groupId = groupId;
    }

    public Long getGroupMemberId()
    {
        return groupMemberId;
    }

    public void setGroupMemberId(final Long groupMemberId)
    {
        this.groupMemberId = groupMemberId;
    }

    /**
     * Gets the created by.
     *
     * @return the created by
     */
    public String getCreatedBy()
    {
        return this.createdBy;
    }

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the group member mobile.
	 *
	 * @return the group member mobile
	 */
	public String getGroupMemberMobile()
	{
		return this.groupMemberMobile;
	}

	/**
	 * Sets the group member mobile.
	 *
	 * @param groupMemberMobile
	 *            the new group member mobile
	 */
	public void setGroupMemberMobile(final String groupMemberMobile)
	{
		this.groupMemberMobile = groupMemberMobile;
	}

	/**
	 * Gets the group member name.
	 *
	 * @return the group member name
	 */
	public String getGroupMemberName()
	{
		return this.groupMemberName;
	}

	/**
	 * Sets the group member name.
	 *
	 * @param groupMemberName
	 *            the new group member name
	 */
	public void setGroupMemberName(final String groupMemberName)
	{
		this.groupMemberName = groupMemberName;
	}

	/**
	 * Gets the checks if is deleted.
	 *
	 * @return the checks if is deleted
	 */
	public byte getIsDeleted()
	{
		return this.isDeleted;
	}

	/**
	 * Sets the checks if is deleted.
	 *
	 * @param isDeleted
	 *            the new checks if is deleted
	 */
	public void setIsDeleted(final byte isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

}