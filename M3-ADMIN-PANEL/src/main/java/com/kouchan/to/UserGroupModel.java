/*
 * Copy right @ Kouchan India Pvt Ltd (2017- 2018)
 */
package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the user_group database table.
 *
 */
@Entity
@Table(name = "user_group")
@NamedQuery(name = "UserGroupModel.findAll", query = "SELECT u FROM UserGroupModel u")
public class UserGroupModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long groupId;

	@Column(name = "owner_id")
	private Long groupOwnerId;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@JsonFormat(locale = "en-IN", shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+5:30")
	private Date createdDate;

	/** The is deleted. */
	@Column(name = "IS_DELETED", columnDefinition = "TINYINT(1) default 0 ")
	private byte isDeleted;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The name. */
	@Column(name = "name")
	private String name;

	/**
	 * Instantiates a new user group.
	 */
	public UserGroupModel()
	{
	}

	public Long getGroupId()
	{
		return groupId;
	}

	public void setGroupId(final Long groupId)
	{
		this.groupId = groupId;
	}

	public Long getGroupOwnerId()
	{
		return groupOwnerId;
	}

	public void setGroupOwnerId(final Long groupOwnerId)
	{
		this.groupOwnerId = groupOwnerId;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(final Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the checks if is deleted.
	 *
	 * @return the checks if is deleted
	 */
	public byte getIsDeleted()
	{
		return this.isDeleted;
	}

	/**
	 * Sets the checks if is deleted.
	 *
	 * @param isDeleted
	 *            the new checks if is deleted
	 */
	public void setIsDeleted(final byte isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(final String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(final Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

}
