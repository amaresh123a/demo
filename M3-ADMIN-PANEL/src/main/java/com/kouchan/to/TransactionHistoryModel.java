package com.kouchan.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the transaction_history database table.
 * 
 */
@Entity
@Table(name = "transaction_history")
@NamedQuery(name = "TransactionHistoryModel.findAll", query = "SELECT t FROM TransactionHistoryModel t")
public class TransactionHistoryModel implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The created by. */
	@Column(name = "CREATED_BY")
	private String createdBy;

	/** The created date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	/** The kouchan reference number. */
	@Column(name = "KOUCHAN_REFERENCE_NUMBER")
	private String kouchanReferenceNumber;

	/** The modified by. */
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	/** The modified date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	/** The customer. */
	// bi-directional many-to-one association to Customer
	@ManyToOne
	@JoinColumn(name = "BANK_DETAIL_ID")
	private BankDetailModel bankDetail;

	/** The transfer status. */
	// bi-directional many-to-one association to TransferStatus
	@ManyToOne
	@JoinColumn(name = "TRANSACTION_STATUS_ID")
	private TransferStatusModel transferStatus;

	/**
	 * Instantiates a new transaction history.
	 */
	public TransactionHistoryModel()
	{
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy()
	{
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate()
	{
		return this.createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId()
	{
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(long id)
	{
		this.id = id;
	}

	/**
	 * Gets the kouchan reference number.
	 *
	 * @return the kouchan reference number
	 */
	public String getKouchanReferenceNumber()
	{
		return this.kouchanReferenceNumber;
	}

	/**
	 * Sets the kouchan reference number.
	 *
	 * @param kouchanReferenceNumber
	 *            the new kouchan reference number
	 */
	public void setKouchanReferenceNumber(String kouchanReferenceNumber)
	{
		this.kouchanReferenceNumber = kouchanReferenceNumber;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	public String getModifiedBy()
	{
		return this.modifiedBy;
	}

	/**
	 * Sets the modified by.
	 *
	 * @param modifiedBy
	 *            the new modified by
	 */
	public void setModifiedBy(String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate()
	{
		return this.modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate
	 *            the new modified date
	 */
	public void setModifiedDate(Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * Gets the bank detail.
	 *
	 * @return the bank detail
	 */
	public BankDetailModel getBankDetail()
	{
		return bankDetail;
	}

	/**
	 * Sets the bank detail.
	 *
	 * @param bankDetail
	 *            the new bank detail
	 */
	public void setBankDetail(BankDetailModel bankDetail)
	{
		this.bankDetail = bankDetail;
	}

	/**
	 * Gets the transfer status.
	 *
	 * @return the transfer status
	 */
	public TransferStatusModel getTransferStatus()
	{
		return this.transferStatus;
	}

	/**
	 * Sets the transfer status.
	 *
	 * @param transferStatus
	 *            the new transfer status
	 */
	public void setTransferStatus(TransferStatusModel transferStatus)
	{
		this.transferStatus = transferStatus;
	}

}