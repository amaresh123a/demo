package com.kouchan.to;

import java.io.Serializable;
import java.util.Objects;

public class UserGroupMapKey implements Serializable
{
    private Long groupId;

    private Long groupMemberId;

    public UserGroupMapKey()
    {

    }

    public UserGroupMapKey(final Long groupId, final Long groupMemberId)
    {
        this.groupId = groupId;
        this.groupMemberId = groupMemberId;
    }

    public Long getGroupId()
    {
        return groupId;
    }

    public void setGroupId(final Long groupId)
    {
        this.groupId = groupId;
    }

    public Long getGroupMemberId()
    {
        return groupMemberId;
    }

    public void setGroupMemberId(final Long groupMemberId)
    {
        this.groupMemberId = groupMemberId;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final UserGroupMapKey that = (UserGroupMapKey) o;
        return Objects.equals(groupId, that.groupId) && Objects.equals(groupMemberId, that.groupMemberId);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(groupId, groupMemberId);
    }

}
