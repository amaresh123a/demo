package com.kouchan.to;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "sms")
@NamedQuery(name = "SmsModel.findAll", query = "SELECT s FROM SmsModel s")
public class SmsModel
{
	@Id
	@Column(name = "id")
	private Long id;
	@Column(name = "name")
	private String name;
	@Column(name = "format")
	private String format;

	public Long getId()
	{
		return id;
	}

	public void setId(final Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public String getFormat()
	{
		return format;
	}

	public void setFormat(final String format)
	{
		this.format = format;
	}

}
