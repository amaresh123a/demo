<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Kouchan India!</title>

<!-- Bootstrap -->
<link href="./resources/vendors/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Font Awesome -->
<link href="./resources/vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!-- NProgress -->
<link href="./resources/vendors/nprogress/nprogress.css"
	rel="stylesheet">
<!-- Animate.css -->
<link href="./resources/vendors/animate.css/animate.min.css"
	rel="stylesheet">
<script type="text/javascript">
        window.history.forward();
        function noBack()
        {
            window.history.forward();
        }
        $(document).ready(function() {
            function disableBack() { window.history.forward() }

            window.onload = disableBack();
            window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
        });
        
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
</script>
<!-- Custom Theme Style -->
<link href="./resources/build/css/custom.min.css" rel="stylesheet">
</head>

<body class="login" onLoad="noBack();" onpageshow="if (event.persisted) noBack();" onUnload="">
	<div>
		<a class="hiddenanchor" id="signup"></a> <a class="hiddenanchor"
			id="signin"></a>

		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
				<c:url value="/login" var="loginVar"/>
					<form id="appointment-form" action="${loginVar}" method="POST">
						<h1>Login in to Kouchan</h1>
						<div>
							<input type="text" class="form-control"
								placeholder="user name" name="custom_username" required="" />
						</div>
						<div>
							<input type="password" name="custom_password"
								class="form-control" placeholder="Password" required="" />
						</div>
						<sec:csrfInput />

						<c:if test="${param.logout != null }">
							<p>You have successfully been logged out.</p>
						</c:if>

						<c:if test="${param.error != null }">
							<p style="color: red;">Invalid Username and Password.</p>
						</c:if>
						<div>
							<button type="submit" class="btn btn-default submit">Log in</button>
							 <a class="reset_pass" href="#">Lost your password?</a>
						</div>

						<div class="clearfix"></div>

						<div class="separator">

							<div class="clearfix"></div>
							<br />

							<div>
								<p>�2017 and �2018 All Rights Reserved for Kouchan India. Privacy and Terms</p>
							</div>
						</div>
					</form>
				</section>
			</div>

		</div>
	</div>
</body>
</html>
