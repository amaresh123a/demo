<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<body>
	<div class="container">
		<div class="row">
			<div class="">

				<c:url value="/createUser" var="createUser"/>
				<form class="form-horizontal" action="${createUser}" method="post">
				  <div class="form-group">
				    <label class="control-label col-sm-2" for="userName">User Name:</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" id="userName" name="userName" placeholder="Enter User Name" required="required">
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="control-label col-sm-2" for="pwd">Role:</label>
				    <div class="col-sm-10"> 
					      <select id="role" name="role">
					       <option value="ROLE_CUSTOMER_SERVICE_AGENT">Customer Service Agent</option>
					        <option value="ROLE_CUSTOMER_REPORT_AGENT">Customer Report Agent</option>
						   <option value="ROLE_ADMIN">Admin</option>
						    <option value="ROLE_SUPER_ADMIN">Super Admin</option>
						</select>
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="control-label col-sm-2" for="password">Password:</label>
				    <div class="col-sm-10"> 
				      <input type="password" class="form-control" id="password" name="password" placeholder="Enter password" required pattern="[a-zA-Z0-9]+">
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="control-label col-sm-2" for="confirmPassword">Confirm Password:</label>
				    <div class="col-sm-10"> 
				      <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Enter Confirm Password" required pattern="[a-zA-Z0-9]+" >
				    </div>
				  </div>
				  <div class="form-group"> 
				    <div class="col-sm-offset-2 col-sm-10">
				      <!-- <button type="submit" id="create-user-btn" class="btn btn-default">Submit</button> -->
				       <p id="create-user-btn" class="btn btn-primary">Submit</p>
				      <p id="create-user-cancel-btn" class="btn btn-danger">Cancel</p>
				    </div>
				  </div>
				  <div class="form-group"> 
				    <div class="col-sm-offset-2 col-sm-10">
				      <p id="errorMessage" style="color: red"></p>
				    </div>
				  </div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
