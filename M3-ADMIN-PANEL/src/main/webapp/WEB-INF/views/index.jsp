<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript">
	window.history.forward();
	function noBack() {
		window.history.forward();
	}
	$(document).ready(function() {
		function disableBack() {
			window.history.forward()
		}

		window.onload = disableBack();
		window.onpageshow = function(evt) {
			if (evt.persisted)
				disableBack()
		}
	});
</script>
<title>Kouchan India |</title>

<link href="../resources/vendors/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<link href="../resources/vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="../resources/vendors/bootstrap-daterangepicker/daterangepicker.css"
	rel="stylesheet">

<!-- Custom Theme Style -->
<link href="../resources/build/css/custom.min.css" rel="stylesheet">

<script type="text/javascript"
	src="../resources/vendors/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="../resources/vendors/bootstrap/bootstrap.min.js"></script>

<!-- <link href="../resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"> -->

<link
	href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"
	rel="stylesheet">
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
</head>

<body class="nav-md" onLoad="noBack();"
	onpageshow="if (event.persisted) noBack();" onUnload="">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<%-- <h2>Employee Management Screen : <security:authentication property="principal.username"></security:authentication></h2> --%>

					<div class="navbar nav_title" style="border: 0;">
						<a href="http://www.kouchanindia.com/" class="site_title"
							target="_blank"><i>K</i> <span>Kouchan India</span></a>
					</div>


					<div class="clearfix"></div>

					<!-- menu profile quick info -->
					<div class="profile clearfix">
						<div class="profile_pic">
							<img src="../resources/images/m3newlogo.png" alt="..."
								class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<span><h2>Welcome,</h2></span>
							<h2><security:authentication property="principal.userName"></security:authentication></h2>
						</div>
					</div>
					<!-- /menu profile quick info -->

					<br />

					<!-- sidebar menu -->
					<div id="sidebar-menu"
						class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<h3>General</h3>
							<ul class="nav side-menu">
								<li><a><i class="fa fa-home"></i> Home <span
										class="fa fa-chevron-down"></span></a></li>

								<li><a><i class="fa fa-users"></i> Manage Users <span
										class="fa fa-chevron-down"></span></a> <security:authorize
										access="hasAnyRole('ROLE_ADMIN','ROLE_SUPER_ADMIN')">
										<ul class="nav child_menu">

											<li><a href="#" id="openDialog">Create User</a></li>
										</ul>
									</security:authorize> <!-- <ul class="nav child_menu">
									
										<li><a href="#" id="openDialog">Create User</a></li>
									</ul> --></li>
								<li><a><i class="fa fa-clone"></i>Reports <span
										class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
									
									<security:authorize access="hasAnyRole('ROLE_SUPER_ADMIN','ROLE_CUSTOMER_REPORT_AGENT')">
									<!--All Transaction Report -->
									<li><a href="#">Customer Transaction</br> Report<span
												class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												<li><a href="/m3admin/customerTransactionReport?reportType=customerTransactionPdf"
															target="_blank">PDF</a></li>
													

												<li><a href="/m3admin/customerTransactionReport?reportType=customerTransactionExcel"
															 target="_blank">XLS </a></li>
													</ul>
													</li>
												
											
											<!-- Transaction Report Daily Weekly & Monthly -->
										<li><a href="#">Customer Transaction</br> Reports <span
												class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												<li><a href="#">PDF <span
												class="fa fa-chevron-down"></span></a>
													<ul class="nav child_menu">
														<li><a
															href="/m3admin/creditAndDebitTransactions?reportType=creditAndDebitTransactionPdf"
															target="_blank">Credit And Debit Transactions</a></li>
														<li><a
															href="/m3admin/customerTransactionReports?reportType=transactionPdfDaily"
															target="_blank">Daily</a></li>
														<li><a
															href="/m3admin/customerTransactionReports?reportType=transactionPdfWeekly"
															target="_blank">Weekly</a></li>
														<li><a
															href="/m3admin/customerTransactionReports?reportType=transactionPdfMonthly"
															target="_blank">Monthly</a></li>
														<!-- <li><a href="#">CVS</a></li> -->
													</ul></li>

												<li><a href="#">XLS <span
												class="fa fa-chevron-down"></span></a>
													<ul class="nav child_menu">
														<li><a
															href="/m3admin/creditAndDebitTransactions?reportType=creditAndDebitTransactionExcel"
															target="_blank">Credit And Debit Transactions</a></li>
														<li><a
															href="/m3admin/customerTransactionReports?reportType=transactionExcelDaily"
															target="_blank">Daily</a></li>
														<li><a
															href="/m3admin/customerTransactionReports?reportType=transactionExcelWeekly"
															target="_blank">Weekly</a></li>
														<li><a
															href="/m3admin/customerTransactionReports?reportType=transactionExcelMonthly"
															target="_blank">Monthly</a></li>
													</ul></li>
												<!-- <li><a href="#">CVS</a></li> -->
											</ul></li>
											
											<!-- Customer Transaction With Commission -->
											<li><a href="#">Customer Transaction & Commission <span
												class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												<li><a href="#">PDF <span
												class="fa fa-chevron-down"></span></a>
													<ul class="nav child_menu">
														<li><a
															href="/m3admin/customerTransactionReportWithCommission?reportType=transactionPdfDaily"
															target="_blank">Daily</a></li>
														<li><a
															href="/m3admin/customerTransactionReportWithCommission?reportType=transactionPdfWeekly"
															target="_blank">Weekly</a></li>
														<li><a
															href="/m3admin/customerTransactionReportWithCommission?reportType=transactionPdfMonthly"
															target="_blank">Monthly</a></li>
														<!-- <li><a href="#">CVS</a></li> -->
													</ul></li>

												<li><a href="#">XLS <span
												class="fa fa-chevron-down"></span></a>
													<ul class="nav child_menu">
														<li><a
															href="/m3admin/customerTransactionReportWithCommission?reportType=transactionExcelDaily"
															target="_blank">Daily</a></li>
														<li><a
															href="/m3admin/customerTransactionReportWithCommission?reportType=transactionExcelWeekly"
															target="_blank">Weekly</a></li>
														<li><a
															href="/m3admin/customerTransactionReportWithCommission?reportType=transactionExcelMonthly"
															target="_blank">Monthly</a></li>
													</ul></li>
												<!-- <li><a href="#">CVS</a></li> -->
											</ul></li>

										<!-- Wallet Balance Report -->
										<li><a href="#">Wallet Balance Report<span
												class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												<li><a href="/m3admin/walletBalanceReport?reportType=walletBalancePdf" target="_blank">PDF </a></li>
												<li><a href="/m3admin/walletBalanceReport?reportType=walletBalanceExcel" target="_blank">XLS </a></li>
											</ul>
										</li>
							
							
										<!-- Wallet Balance Report Daily, Weekly and Monthly -->
										<li><a href="#">Wallet Balance Reports <span
												class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												<li><a href="#">PDF <span
												class="fa fa-chevron-down"></span></a>
													<ul class="nav child_menu">
														<li><a
															href="/m3admin/walletBalanceReports?reportType=walletBalancePdfDaily"
															target="_blank">Daily</a></li>
														<li><a
															href="/m3admin/walletBalanceReports?reportType=walletBalancePdfWeekly"
															target="_blank">Weekly</a></li>
														<li><a
															href="/m3admin/walletBalanceReports?reportType=walletBalancePdfMonthly"
															target="_blank">Monthly</a></li>
														<!-- <li><a href="#">CVS</a></li> -->
													</ul></li>

												<li><a href="#">XLS <span
												class="fa fa-chevron-down"></span></a>
													<ul class="nav child_menu">
														<li><a
															href="/m3admin/walletBalanceReports?reportType=walletBalanceExcelDaily"
															target="_blank">Daily</a></li>
														<li><a
															href="/m3admin/walletBalanceReports?reportType=walletBalanceExcelWeekly"
															target="_blank">Weekly</a></li>
														<li><a
															href="/m3admin/walletBalanceReports?reportType=walletBalanceExcelMonthly"
															target="_blank">Monthly</a></li>
													</ul></li>

												<!-- <li><a href="#">CVS</a></li> -->
											</ul></li>

									</security:authorize>
									</ul></li>
							</ul>
						</div>

					</div>
					<!-- /sidebar menu -->

					<!-- /menu footer buttons -->
					<div class="sidebar-footer hidden-small">
						<a data-toggle="tooltip" data-placement="top" title="Settings">
							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						</a> <a data-toggle="tooltip" data-placement="top" title="FullScreen">
							<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
						</a> <a data-toggle="tooltip" data-placement="top" title="Lock"> <span
							class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
						</a> <a data-toggle="tooltip" data-placement="top" title="Logout"
							href="../login"> <span class="glyphicon glyphicon-off"
							aria-hidden="true"></span>
						</a>
					</div>
					<!-- /menu footer buttons -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">
							<li class="">
								<button type="button" class="btn btn-primary dropdown-toggle"
									data-toggle="dropdown">
									Logout <span class="caret"></span>
								</button>

								<ul class="dropdown-menu dropdown-usermenu pull-right">
									<!-- <li><a href="#">Profile</a></li>
									<li><a href="#">Settings</a></li>
									<li><a href="#">Help</a></li>

									<li class="divider"></li> -->
									<li><a href="../login"><i
											class="fa fa-sign-out pull-right"></i> Log Out</a></li>
								</ul> <!-- </div> -->


							</li>

						</ul>
					</nav>
				</div>
			</div>
			<!-- /top navigation -->

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<!-- <div class="col-md-12">
						<div class="col-md-3">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h3 class="panel-title">Total Success</h3>
								</div>
								<div class="panel-body">$ 2000000000.00</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="panel panel-danger">
								<div class="panel-heading">
									<h3 class="panel-title">Total Failed</h3>
								</div>
								<div class="panel-body">$ 1000.00</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="panel panel-warning">
								<div class="panel-heading">
									<h3 class="panel-title">Total Pending</h3>
								</div>
								<div class="panel-body">$ 1000.00</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="panel panel-info">
								<div class="panel-heading">
									<h3 class="panel-title">Total Refund</h3>
								</div>
								<div class="panel-body">$20000.00</div>
							</div>
						</div>
					</div> -->

					<div class="row">

						<div class="col-md-12">
							<style>
/* The Modal (background) */
.modal-for-success-message {
	display: none; /* Hidden by default */
	position: fixed; /* Stay in place */
	z-index: 1; /* Sit on top */
	padding-top: 100px; /* Location of the box */
	left: 0;
	top: 0;
	width: 100%; /* Full width */
	height: 100%; /* Full height */
	overflow: auto; /* Enable scroll if needed */
	background-color: rgb(0, 0, 0); /* Fallback color */
	background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
}

.modal {
	display: none; /* Hidden by default */
	position: fixed; /* Stay in place */
	z-index: 1; /* Sit on top */
	padding-top: 100px; /* Location of the box */
	left: 0;
	top: 0;
	width: 100%; /* Full width */
	height: 100%; /* Full height */
	overflow: auto; /* Enable scroll if needed */
	background-color: rgb(0, 0, 0); /* Fallback color */
	background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
	background-color: #fefefe;
	margin: auto;
	padding: 0px;
	border: 1px solid #888;
	width: 50%;
}

/* The Close Button */
.close {
	color: #aaaaaa;
	float: right;
	font-size: 28px;
	font-weight: bold;
}

.close:hover, .close:focus {
	color: #000;
	text-decoration: none;
	cursor: pointer;
}
</style>

							<div id="success-message" class="modal-for-success-message">
								<!-- Modal content -->
								<div class="modal-content" style="padding: 20px 25px">
									<span class="close">&times;</span>
									<p id="user-save-success-message"
										style="color: #055f11; font-size: 20px">User saved
										successfully..</p>
								</div>
							</div>
							<div id="myModal" class="modal">
								<!-- Modal content -->
								<div class="modal-content">
									<span class="close">&times;</span>
									<div class="panel panel-success">
										<div class="panel-heading">
											<h3 class="panel-title">Add User</h3>
										</div>
										<div class="panel-body">
											<jsp:include page="createUser.jsp"></jsp:include>
										</div>
									</div>
								</div>
							</div>
						</div>



						<%-- 						<h2>Employee Management Screen : <security:authentication property="principal.userName"></security:authentication></h2> --%>

						<div class="col-md-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Transaction Summary</h2>
									<div class="clearfix"></div>
								</div>
								<style>
.paging_full_numbers {
	width: 550px !Important;
}

.dataTables_info {
	width: 45% !Important;
}
</style>
								<security:authorize access="hasAnyRole('ROLE_SUPER_ADMIN')">

									
										<table id="example" class="table table-striped table-bordered"
											width="100%" cellspacing="0">
											<thead>
												<tr>
													<th>Customer ID</th>
													<th>Mobile Number</th>
													<th>Transaction Amount</th>
													<th>Transaction Type</th>
													<th>Transfer Type</th>
													<th>Vendor Name</th>
													<th>Transfer Status</th>
													<th>Wallet Balance</th>
													<th>Commission</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th>Customer ID</th>
													<th>Mobile Number</th>
													<th>Transaction Amount</th>
													<th>Transaction Type</th>
													<th>Transfer Type</th>
													<th>Vendor Name</th>
													<th>Transfer Status</th>
													<th>Wallet Balance</th>
													<th>Commission</th>
												</tr>
											</tfoot>
											<tbody>
											<c:forEach var="transaction" items="${transactionDetailsList}">
												<tr>
													<td>${transaction.customerId}</td>
													<td>${transaction.mobileNumber}</td>
													<td>${transaction.transactionAmount}</td>
													<td>${transaction.transactionType}</td>
													<td>${transaction.transferType}</td>
													<td>${transaction.vendorName}</td>
													<td>${transaction.transferStatus}</td>
													<td>${transaction.walletBalance}</td>
													<td>${transaction.commission}</td>
												</tr>
												</c:forEach>
											</tbody>
										</table>
									


								</security:authorize>
								<div class="x_content">
									<div class="col-md-9 col-sm-12 col-xs-12">
										<div class="demo-container" style="height: 280px">
											<div id="chart_plot_02" class="demo-placeholder"></div>
										</div>
										<!-- <div class="tiles">
											<div class="col-md-4 tile"></div>
										</div> -->



									</div>
									<security:authorize access="hasRole('ROLE_EXECUTIVE')">
										<c:if test="${top10ProfileList != null}">
											<div class="col-md-3 col-sm-12 col-xs-12">
												<div>
													<div class="x_panel">
														<div class="x_title">
															<h2>Top Profiles</h2>
															<ul class="nav navbar-right panel_toolbox">
																<li><a class="collapse-link"><i
																		class="fa fa-chevron-up"></i></a></li>
																<li><a class="close-link"><i
																		class="fa fa-close"></i></a></li>
															</ul>
															<div class="clearfix"></div>
														</div>
														<div class="x_content">
															<ul class="list-unstyled top_profiles scroll-view">
																<li class="media event"><a
																	class="pull-left border-aero profile_thumb"> <i
																		class="fa fa-user aero"></i>
																</a>
																	<div class="media-body">
																		<a class="title" href="#">Ms.
																			${top10ProfileList.get(1).name}</a>
																		<p>
																			$ <strong>${top10ProfileList.get(1).walletBalance.balance}
																			</strong>
																		</p>
																		<!-- <p>
															<Strong>Needs clarification</strong>
														</p> -->
																	</div></li>
																<li class="media event"><a
																	class="pull-left border-green profile_thumb"> <i
																		class="fa fa-user green"></i>
																</a>
																	<div class="media-body">
																		<a class="title" href="#">Ms.${top10ProfileList.get(2).name}</a>
																		<p>
																			$ <strong>${top10ProfileList.get(2).walletBalance.balance}
																			</strong>
																		</p>
																		<!-- <p>
															<Strong>Needs clarification</strong>
														</p> -->
																	</div></li>
																<li class="media event"><a
																	class="pull-left border-blue profile_thumb"> <i
																		class="fa fa-user blue"></i>
																</a>
																	<div class="media-body">
																		<a class="title" href="#">Ms.
																			${top10ProfileList.get(3).name}</a>
																		<p>
																			$ <strong>${top10ProfileList.get(3).walletBalance.balance}
																			</strong>
																		</p>
																		<!-- <p>
															<Strong>Needs clarification</strong>
														</p> -->
																	</div></li>
																<li class="media event"><a
																	class="pull-left border-aero profile_thumb"> <i
																		class="fa fa-user aero"></i>
																</a>
																	<div class="media-body">
																		<a class="title" href="#">Ms.
																			${top10ProfileList.get(4).name}</a>
																		<p>
																			$ <strong>${top10ProfileList.get(4).walletBalance.balance}
																			</strong>
																		</p>
																		<!-- <p>
															<Strong>Needs clarification</strong>
														</p> -->
																	</div></li>
																<li class="media event"><a
																	class="pull-left border-green profile_thumb"> <i
																		class="fa fa-user green"></i>
																</a>
																	<div class="media-body">
																		<a class="title" href="#">Ms.
																			${top10ProfileList.get(5).name}</a>
																		<p>
																			$ <strong>${top10ProfileList.get(5).walletBalance.balance}
																			</strong>
																		</p>
																		<!-- <p>
															<Strong>Needs clarification</strong>
														</p> -->
																	</div></li>
															</ul>

														</div>
													</div>

												</div>
											</div>
										</c:if>
									</security:authorize>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
		<!-- /page content -->

		<!-- footer content -->
		<footer>
			<div class="pull-center" align="center">

				©2017 and ©2018 All Rights Reserved for Kouchan India. Privacy and
				Terms Terms <a href="http://www.kouchanindia.com/"></a>
			</div>
			<div class="clearfix"></div>
		</footer>
		<!-- /footer content -->
	</div>

	<script src="../resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../resources/vendors/Chart.js/dist/Chart.min.js"></script>
	<script
		src="../resources/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
	<script
		src="../resources/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
	<script
		src="../resources/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
	<script src="../resources/vendors/flot.curvedlines/curvedLines.js"></script>
	<script src="../resources/vendors/DateJS/build/date.js"></script>
	<script src="../resources/vendors/moment/min/moment.min.js"></script>
	<script
		src="../resources/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

	<script src="../resources/build/js/custom.min.js"></script>
</body>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						/* $("#openDialog").click(function(){
							$("#dialog").dialog();
						}); */
						$("#openDialog").click(
								function() {
									$('#errorMessage').html("");
									$('#userName').val("");
									$('#password').val("");
									$('#confirmPassword').val("");
									//			$('#user-save-success-message').removeClass("show").removeClass("hide").addClass("hide");;
									$("#myModal").removeClass("show")
											.removeClass("hide").addClass(
													"show");
								});
						$(".close").click(
								function() {
									
									$("#myModal").removeClass("show")
											.removeClass("hide").addClass(
													"hide");
								});
						$("#create-user-cancel-btn").click(
								function() {
										$("#myModal").removeClass("show")
											.removeClass("hide").addClass(
													"hide");
								});
						$("#create-user-btn")
								.click(
										function() {

											var userName = $("#userName").val();
											var password = $("#password").val();
											var role = $("#role").val();
											var confirmPassword = $(
													"#confirmPassword").val();

											$
													.ajax({
														type : 'POST',
														url : '${pageContext.request.contextPath}/createUser',
														data : {
															userName : userName,
															password : password,
															role : role,
															confirmPassword : confirmPassword
														},
														success : function(
																result) {
															if (result
																	.includes("ERROR")) {
																var res = result
																		.split(":");
																$(
																		'#errorMessage')
																		.html(
																				res[1]);
															} else {
																$("#myModal")
																		.removeClass(
																				"show")
																		.removeClass(
																				"hide")
																		.addClass(
																				"hide");
																$(
																		"#success-message")
																		.fadeIn();
																$(
																		"#success-message")
																		.fadeOut(
																				2000);
															}
														}
													});
										});

						$('#example').DataTable({
							"pagingType" : "full_numbers"
						});
					});

	/* $(document).ready(function() {
	    $('#example').DataTable();
	} ); */
</script>
</html>