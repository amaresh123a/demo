--Wallet Balance report

SELECT C.MOBILE,C.NAME,C.EMAIL,WB.BALANCE,WB.CREATED_DATE,CS.NAME AS STATUS

FROM CUSTOMER C INNER JOIN WALLET_BALANCE WB

ON C.WALLET_BALANCE_ID = WB.ID

INNER JOIN CUSTOMER_STATUS CS ON C.CUSTOMER_STATUS_ID=CS.ID



SELECT cus.id AS customer_id,cus.`name` AS customer_name,tr.amount AS transaction_amount,tr.`utility_commission`,
ft.name AS fundtype,tt.name AS transfertype,ts.`name` AS transfer_status,vt.`name` AS vendor_name,wb.`balance` AS walletbalance
FROM `customer` cus
INNER JOIN TRANSACTION tr 
INNER JOIN `fund_type` ft
INNER JOIN transfer_type tt
INNER JOIN `transfer_status` ts
INNER JOIN  `vendor_type` vt
INNER JOIN  `wallet_balance` wb