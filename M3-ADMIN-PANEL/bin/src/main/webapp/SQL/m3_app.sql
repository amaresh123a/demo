/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.7.16-log : Database - mobile_to_mobile_money_dev
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mobile_to_mobile_money_dev` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `mobile_to_mobile_money_dev`;

/*Table structure for table `address` */

DROP TABLE IF EXISTS `address`;

CREATE TABLE `address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address_1` varchar(255) DEFAULT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `address_3` varchar(255) DEFAULT NULL,
  `city_or_distirct` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `address_type_id` bigint(20) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `house_no` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkkjn52mg1vha4yjpm0douso7f` (`address_type_id`),
  KEY `FK93c3js0e22ll1xlu21nvrhqgg` (`customer_id`),
  CONSTRAINT `FK93c3js0e22ll1xlu21nvrhqgg` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  CONSTRAINT `FKkkjn52mg1vha4yjpm0douso7f` FOREIGN KEY (`address_type_id`) REFERENCES `address_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

/*Data for the table `address` */

insert  into `address`(`id`,`address_1`,`address_2`,`address_3`,`city_or_distirct`,`country`,`created_by`,`created_date`,`is_deleted`,`modified_by`,`modified_date`,`postal_code`,`state`,`address_type_id`,`customer_id`,`house_no`) values (35,'S/o Ishwarappa Arali Near Ganapati Temple Vakkaligeri,Gadag',NULL,NULL,'Yelbarga','INDIA',NULL,NULL,0,NULL,NULL,'590006','karnataka',1,1818,NULL),(36,'Bangalore',NULL,NULL,'Gadag','INDIA',NULL,NULL,0,NULL,NULL,'563236','karnataka',2,1210814,NULL);

/*Table structure for table `address_type` */

DROP TABLE IF EXISTS `address_type`;

CREATE TABLE `address_type` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `address_type` */

insert  into `address_type`(`id`,`created_by`,`created_date`,`modified_by`,`modified_date`,`name`) values (1,NULL,NULL,NULL,NULL,'PERMANENT'),(2,NULL,NULL,NULL,NULL,'PRESENT');

/*Table structure for table `bank_detail` */

DROP TABLE IF EXISTS `bank_detail`;

CREATE TABLE `bank_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_number` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `ifsc_code` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdshbqygasp8h83o774iq41qt0` (`customer_id`),
  CONSTRAINT `FKdshbqygasp8h83o774iq41qt0` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `bank_detail` */

insert  into `bank_detail`(`id`,`account_number`,`created_by`,`created_date`,`ifsc_code`,`is_deleted`,`modified_by`,`modified_date`,`name`,`customer_id`) values (1,'026291800001191',NULL,NULL,'YESB0000270',0,NULL,NULL,'Mahesh',1210814),(2,'026291800001192',NULL,'2017-09-21','YESB0000270',0,NULL,NULL,'Mahesh',1210814);

/*Table structure for table `commission` */

DROP TABLE IF EXISTS `commission`;

CREATE TABLE `commission` (
  `id` int(11) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `kouchan_charges` double DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `range_from` double DEFAULT NULL,
  `range_to` double DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `vendor_charges` double DEFAULT NULL,
  `customer_type_id` bigint(20) DEFAULT NULL,
  `transfer_type_id` bigint(20) DEFAULT NULL,
  `IS_PERCENTAGE` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKf008d0reiow9p7clq6c1fbxit` (`customer_type_id`),
  KEY `FK1imn5r8v96cbgnfrdmvklsslm` (`transfer_type_id`),
  CONSTRAINT `FK1imn5r8v96cbgnfrdmvklsslm` FOREIGN KEY (`transfer_type_id`) REFERENCES `transfer_type` (`id`),
  CONSTRAINT `FKf008d0reiow9p7clq6c1fbxit` FOREIGN KEY (`customer_type_id`) REFERENCES `customer_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `commission` */

insert  into `commission`(`id`,`created_by`,`created_date`,`end_date`,`is_active`,`kouchan_charges`,`modified_by`,`modified_date`,`range_from`,`range_to`,`start_date`,`vendor_charges`,`customer_type_id`,`transfer_type_id`,`IS_PERCENTAGE`) values (1,NULL,NULL,'2019-01-01',1,5,NULL,NULL,0,100,'2017-08-01',0,1,4,0);

/*Table structure for table `contact_detail` */

DROP TABLE IF EXISTS `contact_detail`;

CREATE TABLE `contact_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `mobile` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `contact_type_id` bigint(20) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKl756nwqbv3wgt89cenadkiknq` (`contact_type_id`),
  KEY `FKo16lokt0qfbrmle154dhedwd5` (`customer_id`),
  CONSTRAINT `FKl756nwqbv3wgt89cenadkiknq` FOREIGN KEY (`contact_type_id`) REFERENCES `contact_type` (`id`),
  CONSTRAINT `FKo16lokt0qfbrmle154dhedwd5` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8;

/*Data for the table `contact_detail` */

insert  into `contact_detail`(`id`,`created_by`,`created_date`,`is_deleted`,`mobile`,`modified_by`,`modified_date`,`name`,`contact_type_id`,`customer_id`) values (182,NULL,NULL,0,'917676060664',NULL,NULL,'Shivaraj',1,1818);

/*Table structure for table `contact_type` */

DROP TABLE IF EXISTS `contact_type`;

CREATE TABLE `contact_type` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `contact_type` */

insert  into `contact_type`(`id`,`created_by`,`created_date`,`modified_by`,`modified_date`,`name`) values (1,NULL,NULL,NULL,NULL,'Alternate'),(2,NULL,NULL,NULL,NULL,'Nominee');

/*Table structure for table `currency_conversion` */

DROP TABLE IF EXISTS `currency_conversion`;

CREATE TABLE `currency_conversion` (
  `id` int(11) NOT NULL,
  `base_currency` tinyint(4) DEFAULT NULL,
  `conversation_factor` double DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `currency_symbol` varchar(255) DEFAULT NULL,
  `currency_type` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `currency_conversion` */

/*Table structure for table `customer` */

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `avatar` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `encryption_type` varchar(255) DEFAULT NULL,
  `fcm_token` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `mobile` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `customer_status_id` bigint(20) DEFAULT NULL,
  `customer_type_id` bigint(20) DEFAULT NULL,
  `wallet_balance_id` bigint(20) DEFAULT NULL,
  `account_type_id` bigint(20) DEFAULT NULL,
  `TRANSACTION_COUNT` bigint(20) DEFAULT NULL,
  `customer_referrer` bigint(20) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2r41502dbwehta0hpw1h1iml0` (`customer_status_id`),
  KEY `FKn8vf9jf3m29plqn6rx45p2pl7` (`customer_type_id`),
  KEY `FKsyor67wn4cyqe3is3ovmkbsi7` (`wallet_balance_id`),
  KEY `FKsscxfve43wekl9ysy9aqaw8j0` (`account_type_id`),
  KEY `FK2ooc4ew74hnsyutch7fx8esxa` (`customer_referrer`),
  CONSTRAINT `FK2ooc4ew74hnsyutch7fx8esxa` FOREIGN KEY (`customer_referrer`) REFERENCES `customer` (`id`),
  CONSTRAINT `FK2r41502dbwehta0hpw1h1iml0` FOREIGN KEY (`customer_status_id`) REFERENCES `customer_status` (`id`),
  CONSTRAINT `FKn8vf9jf3m29plqn6rx45p2pl7` FOREIGN KEY (`customer_type_id`) REFERENCES `customer_type` (`id`),
  CONSTRAINT `FKsscxfve43wekl9ysy9aqaw8j0` FOREIGN KEY (`account_type_id`) REFERENCES `customer_type` (`id`),
  CONSTRAINT `FKsyor67wn4cyqe3is3ovmkbsi7` FOREIGN KEY (`wallet_balance_id`) REFERENCES `wallet_balance` (`id`),
  CONSTRAINT `customer_customer_type_FK` FOREIGN KEY (`account_type_id`) REFERENCES `customer_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000001 DEFAULT CHARSET=utf8;

/*Data for the table `customer` */

insert  into `customer`(`id`,`avatar`,`created_by`,`created_date`,`date_of_birth`,`email`,`encryption_type`,`fcm_token`,`gender`,`is_deleted`,`mobile`,`modified_by`,`modified_date`,`username`,`password`,`customer_status_id`,`customer_type_id`,`wallet_balance_id`,`account_type_id`,`TRANSACTION_COUNT`,`customer_referrer`,`role`) values (1818,NULL,'Dev','2017-10-10 16:03:02',NULL,'ganid005@gmail.com',NULL,NULL,NULL,0,'919945961063',NULL,NULL,'test','123',5,1,1818,1,1,NULL,'ROLE_ADMIN'),(1210814,NULL,NULL,'2017-08-24 00:00:00',NULL,'XYZ@kouchan.com','',NULL,NULL,0,'919742191191',NULL,NULL,'Mahesh','pradeep1',1,2,1,2,1,NULL,NULL),(1210907,'NA',NULL,'2017-09-22 17:14:43',NULL,'aditya@gmail.com',NULL,NULL,'Male',0,'919845573055',NULL,NULL,'aditya','barca1',1,1,1891,1,1,NULL,NULL),(1210941,'NA',NULL,'2017-09-29 11:20:13',NULL,'yahoo@kouchanindia.com',NULL,NULL,'Male',0,'917676060664',NULL,NULL,'Shivaraj','123456',6,1,1922,1,1,NULL,NULL),(1210946,'NA',NULL,'2017-09-30 17:49:52',NULL,'mvlvenky123@gmail.com',NULL,NULL,'Male',0,'917070707072',NULL,NULL,'Gggg','12345678',2,1,1927,1,0,NULL,NULL),(1210953,'NA',NULL,'2017-10-02 08:56:45',NULL,'gukhui@gmail.com',NULL,NULL,'',0,'917999999999',NULL,NULL,'Jjjj PVT Ltd','123456',2,1,1934,2,0,NULL,NULL),(10000000,NULL,NULL,'2017-08-24 00:00:00',NULL,'biligi@kouchan.com','',NULL,'Male',0,'9790623123',NULL,NULL,'Kouchan India Pvt Ltd','123456789',1,1,4,NULL,0,NULL,NULL);

/*Table structure for table `customer_status` */

DROP TABLE IF EXISTS `customer_status`;

CREATE TABLE `customer_status` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `customer_status` */

insert  into `customer_status`(`id`,`created_by`,`created_date`,`description`,`is_deleted`,`modified_by`,`modified_date`,`name`) values (1,'anupamkumar','2017-08-16','ALLOW CUSTOMER TO LOGIN AND MAKE TRANSACTIONS',0,'anupamkumar','2017-08-16','ACTIVE'),(2,'anupamkumar','2017-08-16','IF KYC PENDING CUSTOMER ALLOWED TO LOGIN BUT  MAXIMUM WALLET LOADING IS 20000',0,'anupamkumar','2017-08-16','KYC-PENDING'),(5,'anupamkumar','2017-08-16','CUSTOMER SHOULD NOT ALLOW TO LOGIN AND NOT ALLOW TO MAKE TRANSACTIONS',0,'anupamkumar','2017-08-16','BLOCKED'),(6,'anupamkumar','2017-08-16','Customer Kyc is completed and upgraded his/her wallet ',0,'anupamkumar','2017-08-16','KYC_COMPLETED');

/*Table structure for table `customer_type` */

DROP TABLE IF EXISTS `customer_type`;

CREATE TABLE `customer_type` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `customer_type` */

insert  into `customer_type`(`id`,`created_by`,`created_date`,`modified_by`,`modified_date`,`name`) values (1,NULL,NULL,NULL,NULL,'INDIVIDUAL'),(2,NULL,NULL,NULL,NULL,'CORPORATE'),(3,NULL,NULL,NULL,NULL,'GOVT_UNIT'),(4,NULL,NULL,NULL,NULL,'SOLE_PROPRIETARYSHIP'),(5,NULL,NULL,NULL,NULL,'HUF'),(6,NULL,NULL,NULL,NULL,'NGO'),(7,NULL,NULL,NULL,NULL,'FIRM');

/*Table structure for table `customer_unregistered` */

DROP TABLE IF EXISTS `customer_unregistered`;

CREATE TABLE `customer_unregistered` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(14) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `registered_date` datetime DEFAULT NULL,
  `amount` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `customer_unregistered` */

/*Table structure for table `fund_type` */

DROP TABLE IF EXISTS `fund_type`;

CREATE TABLE `fund_type` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `fund_type` */

insert  into `fund_type`(`id`,`created_by`,`created_date`,`modified_by`,`modified_date`,`name`) values (1,NULL,NULL,NULL,NULL,'DEBIT'),(2,NULL,NULL,NULL,NULL,'CREDIT');

/*Table structure for table `keygenerator` */

DROP TABLE IF EXISTS `keygenerator`;

CREATE TABLE `keygenerator` (
  `unique_code` int(11) NOT NULL AUTO_INCREMENT,
  `prefix` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`unique_code`)
) ENGINE=InnoDB AUTO_INCREMENT=30003 DEFAULT CHARSET=utf8;

/*Data for the table `keygenerator` */

insert  into `keygenerator`(`unique_code`,`prefix`) values (30001,'M3UAT'),(30002,'M3 ');

/*Table structure for table `kyc_detail` */

DROP TABLE IF EXISTS `kyc_detail`;

CREATE TABLE `kyc_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `kyc_avatar` varchar(255) DEFAULT NULL,
  `kyc_number` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `kyc_document_type_id` bigint(20) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `kyc_otp` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKpp52xtohft6bjht7tomjana6e` (`customer_id`),
  KEY `FK9rpgyk0b8gifc68a6p68spw9h` (`kyc_document_type_id`),
  CONSTRAINT `FK9rpgyk0b8gifc68a6p68spw9h` FOREIGN KEY (`kyc_document_type_id`) REFERENCES `kyc_document_type` (`id`),
  CONSTRAINT `FKpp52xtohft6bjht7tomjana6e` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

/*Data for the table `kyc_detail` */

insert  into `kyc_detail`(`id`,`created_by`,`created_date`,`is_deleted`,`kyc_avatar`,`kyc_number`,`modified_by`,`modified_date`,`kyc_document_type_id`,`customer_id`,`kyc_otp`) values (90,NULL,NULL,0,'588ajkdkjjd','TNYY133551235',NULL,NULL,1,1818,NULL);

/*Table structure for table `kyc_document_type` */

DROP TABLE IF EXISTS `kyc_document_type`;

CREATE TABLE `kyc_document_type` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `kyc_document_type` */

insert  into `kyc_document_type`(`id`,`created_by`,`created_date`,`is_deleted`,`modified_by`,`modified_date`,`name`) values (1,NULL,NULL,0,NULL,NULL,'PAN'),(3,NULL,NULL,0,NULL,NULL,'GST');

/*Table structure for table `login_history` */

DROP TABLE IF EXISTS `login_history`;

CREATE TABLE `login_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login_time` datetime DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKitwtxp13f5oji0yw95mwqg03e` (`customer_id`),
  CONSTRAINT `FKitwtxp13f5oji0yw95mwqg03e` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1223 DEFAULT CHARSET=utf8;

/*Data for the table `login_history` */

insert  into `login_history`(`id`,`login_time`,`customer_id`) values (196,'2017-10-02 08:57:32',1210953),(1222,'2017-11-10 15:26:21',1818);

/*Table structure for table `m3_settlement_transfer_type` */

DROP TABLE IF EXISTS `m3_settlement_transfer_type`;

CREATE TABLE `m3_settlement_transfer_type` (
  `code` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` bigint(20) DEFAULT NULL,
  `bussiness_day_setteled` int(11) DEFAULT NULL,
  `cut_office_time` datetime DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `m3_settlement_transfer_type` */

/*Table structure for table `oauth_access_token` */

DROP TABLE IF EXISTS `oauth_access_token`;

CREATE TABLE `oauth_access_token` (
  `token_id` varchar(255) DEFAULT NULL,
  `token` mediumblob,
  `authentication_id` varchar(255) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `authentication` mediumblob,
  `refresh_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `oauth_access_token` */

insert  into `oauth_access_token`(`token_id`,`token`,`authentication_id`,`user_name`,`client_id`,`authentication`,`refresh_token`) values ('c0b2cf80784066457d0eaff5843dc5a5','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0mobilet\0919945961063t\0\ncustomerIdsr\0java.lang.Long;��̏#�\0J\0valuexr\0java.lang.Number������\0\0xp\0\0\0\0\0\0\Zt\0idsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.TreeSetݘP���[\0\0xpsr\0Forg.springframework.security.core.userdetails.User$AuthorityComparator\0\0\0\0\0\0�\0\0xpw\0\0\0sr\0Borg.springframework.security.core.authority.SimpleGrantedAuthority\0\0\0\0\0\0�\0L\0roleq\0~\0xpt\0ROLE_M3_END_CUSTOMERxx\0sr\0java.util.Datehj�KYt\0\0xpw\0\0_�4@?xsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xpt\0$86d81ed7-b44f-44c9-bfee-5008d9b03d9csq\0~\0w\0\0`��xsq\0~\0sr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0readxt\0bearert\0$3f7e6433-1f3f-4765-9f6b-9ace30b98405','00ef0284820df7f208c57a828f43798c','919945961063','M3_ANDROID_CLIENT','��\0sr\0Aorg.springframework.security.oauth2.provider.OAuth2Authentication�@bR\0L\0\rstoredRequestt\0<Lorg/springframework/security/oauth2/provider/OAuth2Request;L\0userAuthenticationt\02Lorg/springframework/security/core/Authentication;xr\0Gorg.springframework.security.authentication.AbstractAuthenticationTokenӪ(~nGd\0Z\0\rauthenticatedL\0authoritiest\0Ljava/util/Collection;L\0detailst\0Ljava/lang/Object;xp\0sr\0&java.util.Collections$UnmodifiableList�%1��\0L\0listt\0Ljava/util/List;xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0cq\0~\0xpsr\0java.util.ArrayListx����a�\0I\0sizexp\0\0\0w\0\0\0sr\0Borg.springframework.security.core.authority.SimpleGrantedAuthority\0\0\0\0\0\0�\0L\0rolet\0Ljava/lang/String;xpt\0ROLE_M3_END_CUSTOMERxq\0~\0psr\0:org.springframework.security.oauth2.provider.OAuth2Request\0\0\0\0\0\0\0\0Z\0approvedL\0authoritiesq\0~\0L\0\nextensionst\0Ljava/util/Map;L\0redirectUriq\0~\0L\0refresht\0;Lorg/springframework/security/oauth2/provider/TokenRequest;L\0resourceIdst\0Ljava/util/Set;L\0\rresponseTypesq\0~\0xr\08org.springframework.security.oauth2.provider.BaseRequest6(z>�qi�\0L\0clientIdq\0~\0L\0requestParametersq\0~\0L\0scopeq\0~\0xpt\0M3_ANDROID_CLIENTsr\0%java.util.Collections$UnmodifiableMap��t�B\0L\0mq\0~\0xpsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\ngrant_typet\0passwordt\0	client_idt\0M3_ANDROID_CLIENTt\0scopet\0readt\0usernamet\0919945961063xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xq\0~\0	sr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0q\0~\0!xsq\0~\0\'w\0\0\0?@\0\0\0\0\0sq\0~\0\rt\0ROLE_M3_END_CUSTOMERsq\0~\0\rt\0M3_END_CUSTOMER_READsq\0~\0\rt\0M3_END_CUSTOMER_WRITExsq\0~\0\Z?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xppsq\0~\0\'w\0\0\0?@\0\0\0\0\0\0xsq\0~\0\'w\0\0\0?@\0\0\0\0\0\0xsr\0Oorg.springframework.security.authentication.UsernamePasswordAuthenticationToken\0\0\0\0\0\0�\0L\0credentialsq\0~\0L\0	principalq\0~\0xq\0~\0sq\0~\0sq\0~\0\0\0\0w\0\0\0q\0~\0xq\0~\06sr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxq\0~\0\Z?@\0\0\0\0\0w\0\0\0\0\0\0q\0~\0q\0~\0q\0~\0 q\0~\0!t\0\rclient_secrett\0android-m3-kouchan-secretq\0~\0q\0~\0q\0~\0\"q\0~\0#x\0psr\02org.springframework.security.core.userdetails.User\0\0\0\0\0\0�\0Z\0accountNonExpiredZ\0accountNonLockedZ\0credentialsNonExpiredZ\0enabledL\0authoritiesq\0~\0L\0passwordq\0~\0L\0usernameq\0~\0xpsq\0~\0$sr\0java.util.TreeSetݘP���[\0\0xpsr\0Forg.springframework.security.core.userdetails.User$AuthorityComparator\0\0\0\0\0\0�\0\0xpw\0\0\0q\0~\0xpt\0919945961063','22daee1f06bb56b465141b37243c24f9');

/*Table structure for table `oauth_client_details` */

DROP TABLE IF EXISTS `oauth_client_details`;

CREATE TABLE `oauth_client_details` (
  `client_id` varchar(255) NOT NULL,
  `resource_ids` varchar(255) DEFAULT NULL,
  `client_secret` varchar(255) DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  `authorized_grant_types` varchar(255) DEFAULT NULL,
  `web_server_redirect_uri` varchar(255) DEFAULT NULL,
  `authorities` varchar(255) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `oauth_client_details` */

insert  into `oauth_client_details`(`client_id`,`resource_ids`,`client_secret`,`scope`,`authorized_grant_types`,`web_server_redirect_uri`,`authorities`,`access_token_validity`,`refresh_token_validity`,`additional_information`,`autoapprove`) values ('M3_ANDROID_CLIENT',NULL,'android-m3-kouchan-secret','read,write','client-credentials,refresh_token,password',NULL,'ROLE_M3_END_CUSTOMER,M3_END_CUSTOMER_READ,M3_END_CUSTOMER_WRITE',56000,5000000,NULL,'1');

/*Table structure for table `oauth_refresh_token` */

DROP TABLE IF EXISTS `oauth_refresh_token`;

CREATE TABLE `oauth_refresh_token` (
  `token_id` varchar(255) DEFAULT NULL,
  `token` mediumblob,
  `authentication` mediumblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `oauth_refresh_token` */

insert  into `oauth_refresh_token`(`token_id`,`token`,`authentication`) values ('2a6844110ef1df05dfb8534654256787','��\0sr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationt\0Ljava/util/Date;xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valuet\0Ljava/lang/String;xpt\0$09c3c586-0f73-4a59-bec6-393bda94d3b1sr\0java.util.Datehj�KYt\0\0xpw\0\0`�� x','��\0sr\0Aorg.springframework.security.oauth2.provider.OAuth2Authentication�@bR\0L\0\rstoredRequestt\0<Lorg/springframework/security/oauth2/provider/OAuth2Request;L\0userAuthenticationt\02Lorg/springframework/security/core/Authentication;xr\0Gorg.springframework.security.authentication.AbstractAuthenticationTokenӪ(~nGd\0Z\0\rauthenticatedL\0authoritiest\0Ljava/util/Collection;L\0detailst\0Ljava/lang/Object;xp\0sr\0&java.util.Collections$UnmodifiableList�%1��\0L\0listt\0Ljava/util/List;xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0cq\0~\0xpsr\0java.util.ArrayListx����a�\0I\0sizexp\0\0\0w\0\0\0sr\0Borg.springframework.security.core.authority.SimpleGrantedAuthority\0\0\0\0\0\0�\0L\0rolet\0Ljava/lang/String;xpt\0ROLE_M3_END_CUSTOMERxq\0~\0psr\0:org.springframework.security.oauth2.provider.OAuth2Request\0\0\0\0\0\0\0\0Z\0approvedL\0authoritiesq\0~\0L\0\nextensionst\0Ljava/util/Map;L\0redirectUriq\0~\0L\0refresht\0;Lorg/springframework/security/oauth2/provider/TokenRequest;L\0resourceIdst\0Ljava/util/Set;L\0\rresponseTypesq\0~\0xr\08org.springframework.security.oauth2.provider.BaseRequest6(z>�qi�\0L\0clientIdq\0~\0L\0requestParametersq\0~\0L\0scopeq\0~\0xpt\0M3_ANDROID_CLIENTsr\0%java.util.Collections$UnmodifiableMap��t�B\0L\0mq\0~\0xpsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\ngrant_typet\0passwordt\0	client_idt\0M3_ANDROID_CLIENTt\0scopet\0readt\0usernamet\0919036008829xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xq\0~\0	sr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0q\0~\0!xsq\0~\0\'w\0\0\0?@\0\0\0\0\0sq\0~\0\rt\0ROLE_M3_END_CUSTOMERsq\0~\0\rt\0M3_END_CUSTOMER_READsq\0~\0\rt\0M3_END_CUSTOMER_WRITExsq\0~\0\Z?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xppsq\0~\0\'w\0\0\0?@\0\0\0\0\0\0xsq\0~\0\'w\0\0\0?@\0\0\0\0\0\0xsr\0Oorg.springframework.security.authentication.UsernamePasswordAuthenticationToken\0\0\0\0\0\0�\0L\0credentialsq\0~\0L\0	principalq\0~\0xq\0~\0sq\0~\0sq\0~\0\0\0\0w\0\0\0q\0~\0xq\0~\06sr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxq\0~\0\Z?@\0\0\0\0\0w\0\0\0\0\0\0q\0~\0q\0~\0q\0~\0 q\0~\0!t\0\rclient_secrett\0android-m3-kouchan-secretq\0~\0q\0~\0q\0~\0\"q\0~\0#x\0psr\02org.springframework.security.core.userdetails.User\0\0\0\0\0\0�\0Z\0accountNonExpiredZ\0accountNonLockedZ\0credentialsNonExpiredZ\0enabledL\0authoritiesq\0~\0L\0passwordq\0~\0L\0usernameq\0~\0xpsq\0~\0$sr\0java.util.TreeSetݘP���[\0\0xpsr\0Forg.springframework.security.core.userdetails.User$AuthorityComparator\0\0\0\0\0\0�\0\0xpw\0\0\0q\0~\0xpt\0919036008829'),('22daee1f06bb56b465141b37243c24f9','��\0sr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationt\0Ljava/util/Date;xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valuet\0Ljava/lang/String;xpt\0$86d81ed7-b44f-44c9-bfee-5008d9b03d9csr\0java.util.Datehj�KYt\0\0xpw\0\0`��x','��\0sr\0Aorg.springframework.security.oauth2.provider.OAuth2Authentication�@bR\0L\0\rstoredRequestt\0<Lorg/springframework/security/oauth2/provider/OAuth2Request;L\0userAuthenticationt\02Lorg/springframework/security/core/Authentication;xr\0Gorg.springframework.security.authentication.AbstractAuthenticationTokenӪ(~nGd\0Z\0\rauthenticatedL\0authoritiest\0Ljava/util/Collection;L\0detailst\0Ljava/lang/Object;xp\0sr\0&java.util.Collections$UnmodifiableList�%1��\0L\0listt\0Ljava/util/List;xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0cq\0~\0xpsr\0java.util.ArrayListx����a�\0I\0sizexp\0\0\0w\0\0\0sr\0Borg.springframework.security.core.authority.SimpleGrantedAuthority\0\0\0\0\0\0�\0L\0rolet\0Ljava/lang/String;xpt\0ROLE_M3_END_CUSTOMERxq\0~\0psr\0:org.springframework.security.oauth2.provider.OAuth2Request\0\0\0\0\0\0\0\0Z\0approvedL\0authoritiesq\0~\0L\0\nextensionst\0Ljava/util/Map;L\0redirectUriq\0~\0L\0refresht\0;Lorg/springframework/security/oauth2/provider/TokenRequest;L\0resourceIdst\0Ljava/util/Set;L\0\rresponseTypesq\0~\0xr\08org.springframework.security.oauth2.provider.BaseRequest6(z>�qi�\0L\0clientIdq\0~\0L\0requestParametersq\0~\0L\0scopeq\0~\0xpt\0M3_ANDROID_CLIENTsr\0%java.util.Collections$UnmodifiableMap��t�B\0L\0mq\0~\0xpsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\ngrant_typet\0passwordt\0	client_idt\0M3_ANDROID_CLIENTt\0scopet\0readt\0usernamet\0919945961063xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xq\0~\0	sr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0q\0~\0!xsq\0~\0\'w\0\0\0?@\0\0\0\0\0sq\0~\0\rt\0ROLE_M3_END_CUSTOMERsq\0~\0\rt\0M3_END_CUSTOMER_READsq\0~\0\rt\0M3_END_CUSTOMER_WRITExsq\0~\0\Z?@\0\0\0\0\0\0w\0\0\0\0\0\0\0xppsq\0~\0\'w\0\0\0?@\0\0\0\0\0\0xsq\0~\0\'w\0\0\0?@\0\0\0\0\0\0xsr\0Oorg.springframework.security.authentication.UsernamePasswordAuthenticationToken\0\0\0\0\0\0�\0L\0credentialsq\0~\0L\0	principalq\0~\0xq\0~\0sq\0~\0sq\0~\0\0\0\0w\0\0\0q\0~\0xq\0~\06sr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxq\0~\0\Z?@\0\0\0\0\0w\0\0\0\0\0\0q\0~\0q\0~\0q\0~\0 q\0~\0!t\0\rclient_secrett\0android-m3-kouchan-secretq\0~\0q\0~\0q\0~\0\"q\0~\0#x\0psr\02org.springframework.security.core.userdetails.User\0\0\0\0\0\0�\0Z\0accountNonExpiredZ\0accountNonLockedZ\0credentialsNonExpiredZ\0enabledL\0authoritiesq\0~\0L\0passwordq\0~\0L\0usernameq\0~\0xpsq\0~\0$sr\0java.util.TreeSetݘP���[\0\0xpsr\0Forg.springframework.security.core.userdetails.User$AuthorityComparator\0\0\0\0\0\0�\0\0xpw\0\0\0q\0~\0xpt\0919945961063');

/*Table structure for table `operatorcommission` */

DROP TABLE IF EXISTS `operatorcommission`;

CREATE TABLE `operatorcommission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `providername` varchar(1000) DEFAULT NULL,
  `commission` double DEFAULT '0',
  `vendor` int(11) DEFAULT NULL,
  `url` varchar(10000) DEFAULT NULL,
  `providerid` int(11) NOT NULL,
  `operator_type_name` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `providerid_UNIQUE` (`providerid`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `vendorRef_idx` (`vendor`),
  KEY `fk_operator_type` (`operator_type_name`(255))
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

/*Data for the table `operatorcommission` */

insert  into `operatorcommission`(`id`,`providername`,`commission`,`vendor`,`url`,`providerid`,`operator_type_name`) values (1,'Jio',8,1,'https://www.jio.com/en-in/4g-plans',167,'PREPAID'),(2,'	Vodafone',3.7,1,'https://shop.vodafone.in/shop/prepaid/all-recharge-plans.jsp',5,'PREPAID'),(33,'Airtel DTH\r\n',3.75,1,NULL,22,'DTH'),(34,'Dish TV\r\n',3.5,1,NULL,17,'DTH'),(35,'Sun Direct\r\n',4,1,NULL,20,'DTH'),(36,'Tata Sky\r\n',3.75,1,NULL,19,'DTH'),(37,'Videocon D2h\r\n',4,1,NULL,21,'DTH');

/*Table structure for table `reward` */

DROP TABLE IF EXISTS `reward`;

CREATE TABLE `reward` (
  `ID` bigint(20) NOT NULL,
  `CREATED_BY` varchar(40) DEFAULT NULL,
  `CREATED_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime NOT NULL,
  `AMOUNT` double NOT NULL,
  `IS_ACTIVE` tinyint(1) NOT NULL,
  `MODIFIED_BY` varchar(40) DEFAULT NULL,
  `MODIFIED_DATE` datetime DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `ACTION` varchar(40) NOT NULL,
  `CUSTOMER_ID` bigint(20) NOT NULL,
  KEY `FKq4a0acffv4i9l135k43n7dwr9` (`CUSTOMER_ID`),
  CONSTRAINT `FKq4a0acffv4i9l135k43n7dwr9` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `customer` (`id`),
  CONSTRAINT `reward_customer_FK` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `reward` */

insert  into `reward`(`ID`,`CREATED_BY`,`CREATED_DATE`,`END_DATE`,`AMOUNT`,`IS_ACTIVE`,`MODIFIED_BY`,`MODIFIED_DATE`,`START_DATE`,`ACTION`,`CUSTOMER_ID`) values (1,NULL,NULL,'2019-01-01 00:00:00',1,1,NULL,NULL,'2017-08-01 00:00:00','DOWNLOAD',10000000),(2,NULL,NULL,'2019-01-01 00:00:00',3,1,NULL,NULL,'2017-08-01 00:00:00','DEBIT',10000000);

/*Table structure for table `service_tax` */

DROP TABLE IF EXISTS `service_tax`;

CREATE TABLE `service_tax` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `rate` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `service_tax` */

/*Table structure for table `sms` */

DROP TABLE IF EXISTS `sms`;

CREATE TABLE `sms` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `format` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sms` */

insert  into `sms`(`id`,`name`,`format`) values (1,'Block_Message','Dear %s, please note that your M3 Acc No (%s) is blocked by Alternate mobile holder (%s) designated by you. In case you wish to unblock, contact him/her. And also you may contact our Customer care on %s for further help.\n'),(2,'Unblock_Message','Dear %s, please note that your M3 Acc No. (%s) is unblocked by Alternate mobile holder designated by you. Now you may start using M3 without any hassle. Enjoy the M3 Cash World, the most simple and easy to make payments.');

/*Table structure for table `transaction` */

DROP TABLE IF EXISTS `transaction`;

CREATE TABLE `transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` double NOT NULL,
  `cgst` double NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `details` varchar(500) DEFAULT NULL,
  `igst` double NOT NULL,
  `kouchan_reference_number` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `service_charge` double DEFAULT NULL,
  `sgst` double NOT NULL,
  `vendor_or_group_reference_number` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `fund_type_id` bigint(20) DEFAULT NULL,
  `transaction_status_id` bigint(20) DEFAULT NULL,
  `transfer_type_id` bigint(20) DEFAULT NULL,
  `vendor_type_id` bigint(20) DEFAULT NULL,
  `utility_commission` double DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FKnbpjofb5abhjg5hiovi0t3k57` (`customer_id`),
  KEY `FK3rmctcxocwow2dietsfqe0wdp` (`fund_type_id`),
  KEY `FKrgwk5b1yesi7dgq0ijyh4ft82` (`transaction_status_id`),
  KEY `FK72fees9mu9egmw08gu86qe0qh` (`transfer_type_id`),
  KEY `FKta72eibxejnntrxberpva9xfl` (`vendor_type_id`),
  CONSTRAINT `FK3rmctcxocwow2dietsfqe0wdp` FOREIGN KEY (`fund_type_id`) REFERENCES `fund_type` (`id`),
  CONSTRAINT `FK72fees9mu9egmw08gu86qe0qh` FOREIGN KEY (`transfer_type_id`) REFERENCES `transfer_type` (`id`),
  CONSTRAINT `FKnbpjofb5abhjg5hiovi0t3k57` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  CONSTRAINT `FKrgwk5b1yesi7dgq0ijyh4ft82` FOREIGN KEY (`transaction_status_id`) REFERENCES `transfer_status` (`id`),
  CONSTRAINT `FKta72eibxejnntrxberpva9xfl` FOREIGN KEY (`vendor_type_id`) REFERENCES `vendor_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=399 DEFAULT CHARSET=utf8;

/*Data for the table `transaction` */

insert  into `transaction`(`id`,`amount`,`cgst`,`created_by`,`created_date`,`details`,`igst`,`kouchan_reference_number`,`modified_by`,`modified_date`,`service_charge`,`sgst`,`vendor_or_group_reference_number`,`customer_id`,`fund_type_id`,`transaction_status_id`,`transfer_type_id`,`vendor_type_id`,`utility_commission`) values (398,100,0,NULL,'2017-10-10','{\"receivedFrom\":\"Devu\",\"sentTo\":\"Shivaraj\",\"sendToMobile\":\"917676060664\",\"recieverFromMobile\":\"919945961063\",\"transactionReasons\":\"Reason\",\"transactionReferenceOne\":\"Reference one\",\"transactionReferenceTwo\":\"\",\"transactionReferenceThree\":\"\"}',0,'M330194',NULL,NULL,0,0,NULL,1210941,2,5,6,3,0);

/*Table structure for table `transaction_history` */

DROP TABLE IF EXISTS `transaction_history`;

CREATE TABLE `transaction_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `kouchan_reference_number` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `bank_detail_id` bigint(20) DEFAULT NULL,
  `transaction_status_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrens5l4vfs372ej76af05bgwy` (`bank_detail_id`),
  KEY `FK4s2aggdxl05ox0q37ldchdne0` (`transaction_status_id`),
  CONSTRAINT `FK4s2aggdxl05ox0q37ldchdne0` FOREIGN KEY (`transaction_status_id`) REFERENCES `transfer_status` (`id`),
  CONSTRAINT `FKrens5l4vfs372ej76af05bgwy` FOREIGN KEY (`bank_detail_id`) REFERENCES `bank_detail` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `transaction_history` */

insert  into `transaction_history`(`id`,`created_by`,`created_date`,`kouchan_reference_number`,`modified_by`,`modified_date`,`bank_detail_id`,`transaction_status_id`) values (1,NULL,'2017-09-21','M330002',NULL,NULL,2,2);

/*Table structure for table `transaction_reasons` */

DROP TABLE IF EXISTS `transaction_reasons`;

CREATE TABLE `transaction_reasons` (
  `transaction_reasons_id` int(11) NOT NULL,
  `transaction_reason_type` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `transaction_reasons` */

insert  into `transaction_reasons`(`transaction_reasons_id`,`transaction_reason_type`) values (27,'Milk Supply\r\n');

/*Table structure for table `transactional_error_log` */

DROP TABLE IF EXISTS `transactional_error_log`;

CREATE TABLE `transactional_error_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `transactional_exception` text,
  `transactional_response` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `transactional_error_log` */

insert  into `transactional_error_log`(`id`,`created_date`,`transactional_exception`,`transactional_response`) values (1,'2017-10-22 20:00:06',NULL,'TransferWebServiceResponseData [version=2.0, uniqueResponseNo=79670666b73511e793b30a0028ef0000, attemptNo=1, transferType=IMPS, lowBalanceAlert=false, transactionStatus=com.kouchan.m3.core.integration.yesbank.messages.TransactionStatusType@4d682552, nameWithBeneficiaryBank=AMEYA ANIL, requestReferenceNo=M330372, totalTransaction=100.00, subTotalTransaction=null, cgstTax=0.00, sgstTax=0.00, ngstTax=null, bankServiceCharge=0, kouchanServiceCharge=0, totalServiceCharge=0, customerID=1210814, accountNumber=026291800001191]');

/*Table structure for table `transfer_status` */

DROP TABLE IF EXISTS `transfer_status`;

CREATE TABLE `transfer_status` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `vendor_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKo6177eycs6rs3usmecsw8nij8` (`vendor_id`),
  CONSTRAINT `FKo6177eycs6rs3usmecsw8nij8` FOREIGN KEY (`vendor_id`) REFERENCES `vendor_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `transfer_status` */

insert  into `transfer_status`(`id`,`created_by`,`created_date`,`description`,`modified_by`,`modified_date`,`name`,`vendor_id`) values (1,NULL,NULL,'INITIATED',NULL,NULL,'SENT_TO_BENEFICIARY',1),(2,NULL,NULL,'CREDITED TO BENEFICIARY',NULL,NULL,'COMPLETED',1),(5,NULL,NULL,'CREDITED TO BENEFICIARY',NULL,NULL,'SUCCESS',3);

/*Table structure for table `transfer_type` */

DROP TABLE IF EXISTS `transfer_type`;

CREATE TABLE `transfer_type` (
  `id` bigint(20) NOT NULL,
  `business_process_time` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `vendor_type_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4jsu2qliyoxp7qmeh4gldsrey` (`vendor_type_id`),
  CONSTRAINT `FK4jsu2qliyoxp7qmeh4gldsrey` FOREIGN KEY (`vendor_type_id`) REFERENCES `vendor_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `transfer_type` */

insert  into `transfer_type`(`id`,`business_process_time`,`created_by`,`created_date`,`modified_by`,`modified_date`,`name`,`vendor_type_id`) values (1,NULL,NULL,NULL,NULL,NULL,'W-BANK-NEFT',1),(2,NULL,NULL,NULL,NULL,NULL,'W-BANK-RTGS',1),(4,NULL,NULL,NULL,NULL,NULL,'W-BANK-ANY',1),(5,NULL,NULL,NULL,NULL,NULL,'W-BANK-FT',1),(6,NULL,NULL,NULL,NULL,NULL,'W-W',3),(7,NULL,NULL,NULL,NULL,NULL,'W-W-GROUP',3),(8,NULL,NULL,NULL,NULL,NULL,'B-W',1),(9,NULL,NULL,NULL,NULL,NULL,'NEFT',1),(10,NULL,NULL,NULL,NULL,NULL,'FT',1),(11,NULL,NULL,NULL,NULL,NULL,'ANY',1),(12,NULL,NULL,NULL,NULL,NULL,'Utility-Bill-Paymart',4);

/*Table structure for table `user_group` */

DROP TABLE IF EXISTS `user_group`;

CREATE TABLE `user_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `owner_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKopk2e4gxotk6wfphpckdgy9bj` (`owner_id`),
  CONSTRAINT `FKopk2e4gxotk6wfphpckdgy9bj` FOREIGN KEY (`owner_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8;

/*Data for the table `user_group` */

insert  into `user_group`(`id`,`created_by`,`created_date`,`is_deleted`,`modified_by`,`modified_date`,`name`,`owner_id`) values (169,NULL,'2017-10-10 16:32:40',1,NULL,NULL,'CVS',1210946);

/*Table structure for table `user_group_members_map` */

DROP TABLE IF EXISTS `user_group_members_map`;

CREATE TABLE `user_group_members_map` (
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `group_member_mobile` varchar(255) DEFAULT NULL,
  `group_member_name` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `group_member_id` bigint(20) DEFAULT NULL,
  `group_id` bigint(20) DEFAULT NULL,
  KEY `FKl64fbldp8qbkx79uc41buy8nv` (`group_member_id`),
  KEY `FKiryldxxqoxmdd5d6ob9fsaen0` (`group_id`),
  CONSTRAINT `FKiryldxxqoxmdd5d6ob9fsaen0` FOREIGN KEY (`group_id`) REFERENCES `user_group` (`id`),
  CONSTRAINT `FKl64fbldp8qbkx79uc41buy8nv` FOREIGN KEY (`group_member_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_group_members_map` */

insert  into `user_group_members_map`(`created_by`,`created_date`,`group_member_mobile`,`group_member_name`,`is_deleted`,`modified_by`,`modified_date`,`group_member_id`,`group_id`) values (NULL,NULL,'919845573055','aditya',1,NULL,NULL,1210907,169);

/*Table structure for table `utilityservicevendor` */

DROP TABLE IF EXISTS `utilityservicevendor`;

CREATE TABLE `utilityservicevendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `utilityservicevendor` */

insert  into `utilityservicevendor`(`id`,`name`) values (1,'http://billpaymart.com/');

/*Table structure for table `vendor_type` */

DROP TABLE IF EXISTS `vendor_type`;

CREATE TABLE `vendor_type` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `vendor_type` */

insert  into `vendor_type`(`id`,`created_by`,`created_date`,`modified_by`,`modified_date`,`name`) values (1,NULL,NULL,NULL,NULL,'YESBANK'),(3,NULL,NULL,NULL,NULL,'KOUCHAN'),(4,NULL,NULL,NULL,NULL,'BILLPAYMART');

/*Table structure for table `wallet_balance` */

DROP TABLE IF EXISTS `wallet_balance`;

CREATE TABLE `wallet_balance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `balance` double NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2103 DEFAULT CHARSET=utf8;

/*Data for the table `wallet_balance` */

insert  into `wallet_balance`(`id`,`balance`,`created_by`,`created_date`,`is_deleted`,`modified_by`,`modified_date`) values (1,5264.360000000001,NULL,'2017-08-24 00:00:00',0,NULL,NULL),(2,16023,NULL,'2017-08-24 00:00:00',0,NULL,NULL),(3,21591,NULL,'2017-08-24 00:00:00',0,NULL,NULL),(4,19960,NULL,'2017-08-24 00:00:00',0,NULL,NULL),(1818,247,NULL,NULL,0,NULL,NULL),(1835,0,NULL,'2017-09-20 16:45:45',0,NULL,NULL),(1836,0,NULL,'2017-09-20 18:28:09',0,NULL,NULL),(1837,0,NULL,'2017-09-20 18:32:05',0,NULL,NULL),(1838,0,NULL,'2017-09-20 21:20:04',0,NULL,NULL),(1864,0,NULL,'2017-09-21 17:16:56',0,NULL,NULL),(1865,0,NULL,'2017-09-21 17:18:53',0,NULL,NULL),(1866,0,NULL,'2017-09-21 17:20:44',0,NULL,NULL),(1867,0,NULL,'2017-09-21 17:24:39',0,NULL,NULL),(1868,0,NULL,'2017-09-21 17:55:30',0,NULL,NULL),(1869,0,NULL,'2017-09-21 18:10:49',0,NULL,NULL),(1875,0,NULL,'2017-09-22 11:40:16',0,NULL,NULL),(1876,0,NULL,'2017-09-22 11:42:17',0,NULL,NULL),(1877,0,NULL,'2017-09-22 11:45:16',0,NULL,NULL),(1878,0,NULL,'2017-09-22 11:49:11',0,NULL,NULL),(1879,0,NULL,'2017-09-22 11:50:39',0,NULL,NULL),(1880,0,NULL,'2017-09-22 11:52:49',0,NULL,NULL),(1881,0,NULL,'2017-09-22 11:56:48',0,NULL,NULL),(1882,0,NULL,'2017-09-22 12:00:30',0,NULL,NULL),(1883,0,NULL,'2017-09-22 12:20:47',0,NULL,NULL),(1884,0,NULL,'2017-09-22 12:21:43',0,NULL,NULL),(1885,0,NULL,'2017-09-22 14:37:27',0,NULL,NULL),(1887,0,NULL,'2017-09-22 14:48:52',0,NULL,NULL),(1888,0,NULL,'2017-09-22 16:39:20',0,NULL,NULL),(1889,0,NULL,'2017-09-22 16:41:27',0,NULL,NULL),(1890,0,NULL,'2017-09-22 16:49:35',0,NULL,NULL),(1891,2224.45,NULL,'2017-09-22 17:14:43',0,NULL,NULL),(1892,0,NULL,'2017-09-22 17:26:27',0,NULL,NULL),(1893,0,NULL,'2017-09-22 17:28:20',0,NULL,NULL),(1894,0,NULL,'2017-09-22 17:40:38',0,NULL,NULL),(1895,0,NULL,'2017-09-22 18:14:46',0,NULL,NULL),(1896,0,NULL,'2017-09-22 23:11:10',0,NULL,NULL),(1902,0,NULL,'2017-09-23 16:26:13',0,NULL,NULL),(1903,0,NULL,'2017-09-23 16:27:38',0,NULL,NULL),(1904,0,NULL,'2017-09-23 16:30:50',0,NULL,NULL),(1905,0,NULL,'2017-09-23 16:37:55',0,NULL,NULL),(1906,0,NULL,'2017-09-23 16:49:28',0,NULL,NULL),(1907,0,NULL,'2017-09-23 16:51:07',0,NULL,NULL),(1908,0,NULL,'2017-09-23 16:52:05',0,NULL,NULL),(1910,0,NULL,'2017-09-25 11:35:59',0,NULL,NULL),(1911,0,NULL,'2017-09-25 11:48:33',0,NULL,NULL),(1912,0,NULL,'2017-09-25 11:50:35',0,NULL,NULL),(1913,0,NULL,'2017-09-25 12:19:59',0,NULL,NULL),(1915,0,NULL,'2017-09-25 13:55:18',0,NULL,NULL),(1916,0,NULL,'2017-09-25 14:02:36',0,NULL,NULL),(1917,0,NULL,'2017-09-25 14:44:57',0,NULL,NULL),(1918,0,NULL,'2017-09-25 15:01:49',0,NULL,NULL),(1919,0,NULL,'2017-09-25 15:15:21',0,NULL,NULL),(1920,0,NULL,'2017-09-28 12:47:13',0,NULL,NULL),(1921,0,NULL,'2017-09-28 12:59:01',0,NULL,NULL),(1922,29450.97999999999,NULL,'2017-09-29 11:20:13',0,NULL,NULL),(1923,9900.04,NULL,'2017-09-29 11:27:39',0,NULL,NULL),(1924,763,NULL,'2017-09-29 11:32:05',0,NULL,NULL),(1925,0,NULL,'2017-09-29 11:32:59',0,NULL,NULL),(1926,205.6599999999999,NULL,'2017-09-29 11:59:16',0,NULL,NULL),(1927,77.82999999999996,NULL,'2017-09-30 17:49:52',0,NULL,NULL),(1928,0,NULL,'2017-09-30 22:01:45',0,NULL,NULL),(1929,1034,NULL,'2017-10-01 08:58:03',0,NULL,NULL),(1930,0,NULL,'2017-10-02 08:33:06',0,NULL,NULL),(1931,15000,NULL,'2017-10-02 08:38:26',0,NULL,NULL),(1932,1136,NULL,'2017-10-02 08:49:37',0,NULL,NULL),(1933,0,NULL,'2017-10-02 08:52:06',0,NULL,NULL),(1934,0,NULL,'2017-10-02 08:56:45',0,NULL,NULL),(1935,0,NULL,'2017-10-02 09:22:39',0,NULL,NULL),(1936,0,NULL,'2017-10-02 09:57:24',0,NULL,NULL),(1937,0,NULL,'2017-10-02 09:59:38',0,NULL,NULL),(1938,0,NULL,'2017-10-04 13:24:24',0,NULL,NULL),(1939,18350,NULL,'2017-10-04 17:43:38',0,NULL,NULL),(1940,0,NULL,'2017-10-05 14:23:48',0,NULL,NULL),(1941,1000,NULL,'2017-10-05 14:42:49',0,NULL,NULL),(1942,0,NULL,'2017-10-07 12:26:59',0,NULL,NULL),(1943,10277,NULL,'2017-10-09 12:20:28',0,NULL,NULL),(1944,0,NULL,'2017-10-09 12:32:02',0,NULL,NULL),(1945,100253,NULL,'2017-10-09 12:45:48',0,NULL,NULL),(1946,11520,NULL,'2017-10-09 13:51:52',0,NULL,NULL),(1947,89900,NULL,'2017-10-09 14:19:27',0,NULL,NULL),(1948,9944,NULL,'2017-10-09 14:29:40',0,NULL,NULL),(1949,9850,NULL,'2017-10-09 15:33:26',0,NULL,NULL),(1950,31143,NULL,'2017-10-09 15:36:07',0,NULL,NULL),(1951,20250,NULL,'2017-10-09 15:37:44',0,NULL,NULL),(1952,0,NULL,'2017-10-09 16:45:41',0,NULL,NULL),(1953,0,NULL,'2017-10-09 17:01:01',0,NULL,NULL),(1954,0,NULL,'2017-10-09 17:53:18',0,NULL,NULL),(1955,0,NULL,'2017-10-09 18:07:43',0,NULL,NULL),(1956,11827,NULL,'2017-10-10 16:35:40',0,NULL,NULL),(1957,0,NULL,'2017-10-10 17:54:34',0,NULL,NULL),(1958,0,NULL,'2017-10-17 14:09:53',0,NULL,NULL),(1959,0,NULL,'2017-10-17 14:33:21',0,NULL,NULL),(1960,619,NULL,'2017-10-17 14:42:38',0,NULL,NULL),(1961,0,NULL,'2017-10-17 15:12:32',0,NULL,NULL),(1962,13,NULL,'2017-10-17 15:19:16',0,NULL,NULL),(1968,1205,NULL,'2017-10-18 12:51:39',0,NULL,NULL),(1969,0,NULL,'2017-10-18 13:32:02',0,NULL,NULL),(1970,1960,NULL,'2017-10-18 13:35:57',0,NULL,NULL),(1971,0,NULL,'2017-10-18 14:09:00',0,NULL,NULL),(1972,407,NULL,'2017-10-18 14:28:36',0,NULL,NULL),(1973,0,NULL,'2017-10-20 12:33:09',0,NULL,NULL),(1974,0,NULL,'2017-10-23 17:14:36',0,NULL,NULL),(1975,0,NULL,'2017-10-24 11:47:40',0,NULL,NULL),(1976,0,NULL,'2017-10-24 11:48:36',0,NULL,NULL),(1977,0,NULL,'2017-10-24 12:08:03',0,NULL,NULL),(1978,0,NULL,'2017-10-24 12:08:54',0,NULL,NULL),(1980,0,NULL,'2017-10-24 12:16:48',0,NULL,NULL),(1981,0,NULL,'2017-10-24 12:33:18',0,NULL,NULL),(1982,0,NULL,'2017-10-24 12:34:25',0,NULL,NULL),(1983,513,NULL,'2017-10-24 12:43:03',0,NULL,NULL),(1984,50,NULL,'2017-10-25 15:50:06',0,NULL,NULL),(1985,0,NULL,'2017-10-25 16:43:19',0,NULL,NULL),(1986,0,NULL,'2017-10-25 18:07:48',0,NULL,NULL),(1987,0,NULL,'2017-10-26 15:35:18',0,NULL,NULL),(1988,0,NULL,'2017-10-26 18:07:56',0,NULL,NULL),(1989,0,NULL,'2017-10-26 18:09:23',0,NULL,NULL),(1990,0,NULL,'2017-10-27 11:20:52',0,NULL,NULL),(1991,0,NULL,'2017-10-27 11:50:47',0,NULL,NULL),(1992,0,NULL,'2017-10-27 13:25:58',0,NULL,NULL),(1993,0,NULL,'2017-10-27 14:28:56',0,NULL,NULL),(1994,0,NULL,'2017-10-27 14:28:56',0,NULL,NULL),(1995,0,NULL,'2017-10-27 14:28:57',0,NULL,NULL),(1996,0,NULL,'2017-10-27 14:28:57',0,NULL,NULL),(1997,0,NULL,'2017-10-27 14:28:57',0,NULL,NULL),(1998,0,NULL,'2017-10-27 14:28:58',0,NULL,NULL),(1999,0,NULL,'2017-10-27 14:28:59',0,NULL,NULL),(2000,0,NULL,'2017-10-27 14:28:59',0,NULL,NULL),(2001,0,NULL,'2017-10-27 14:29:00',0,NULL,NULL),(2002,0,NULL,'2017-10-27 14:29:01',0,NULL,NULL),(2003,0,NULL,'2017-10-27 17:12:14',0,NULL,NULL),(2004,0,NULL,'2017-10-27 17:12:14',0,NULL,NULL),(2005,0,NULL,'2017-10-27 17:12:15',0,NULL,NULL),(2006,0,NULL,'2017-10-27 17:12:15',0,NULL,NULL),(2007,0,NULL,'2017-10-27 17:12:16',0,NULL,NULL),(2008,0,NULL,'2017-10-27 17:12:16',0,NULL,NULL),(2009,0,NULL,'2017-10-27 17:12:17',0,NULL,NULL),(2010,0,NULL,'2017-10-27 17:12:17',0,NULL,NULL),(2011,0,NULL,'2017-10-27 17:12:18',0,NULL,NULL),(2012,0,NULL,'2017-10-27 17:12:19',0,NULL,NULL),(2013,0,NULL,'2017-10-27 17:15:35',0,NULL,NULL),(2014,0,NULL,'2017-10-27 17:15:35',0,NULL,NULL),(2015,0,NULL,'2017-10-27 17:15:35',0,NULL,NULL),(2016,0,NULL,'2017-10-27 17:15:36',0,NULL,NULL),(2017,0,NULL,'2017-10-27 17:15:36',0,NULL,NULL),(2018,0,NULL,'2017-10-27 17:15:36',0,NULL,NULL),(2019,0,NULL,'2017-10-27 17:15:37',0,NULL,NULL),(2020,0,NULL,'2017-10-27 17:15:38',0,NULL,NULL),(2021,0,NULL,'2017-10-27 17:15:38',0,NULL,NULL),(2022,0,NULL,'2017-10-27 17:15:39',0,NULL,NULL),(2023,0,NULL,'2017-10-30 10:58:40',0,NULL,NULL),(2025,0,NULL,'2017-10-30 12:02:54',0,NULL,NULL),(2026,0,NULL,'2017-10-30 12:17:59',0,NULL,NULL),(2052,0,NULL,'2017-11-02 11:20:34',0,NULL,NULL),(2054,0,NULL,'2017-11-02 11:11:10',0,NULL,NULL),(2055,0,NULL,'2017-11-02 11:16:07',0,NULL,NULL),(2056,0,NULL,'2017-11-02 11:21:13',0,NULL,NULL),(2057,0,NULL,'2017-11-02 11:22:38',0,NULL,NULL),(2058,0,NULL,'2017-11-02 11:23:24',0,NULL,NULL),(2059,0,NULL,'2017-11-02 11:24:57',0,NULL,NULL),(2060,0,NULL,'2017-11-02 11:28:22',0,NULL,NULL),(2061,0,NULL,'2017-11-02 11:29:12',0,NULL,NULL),(2062,0,NULL,'2017-11-02 15:45:08',0,NULL,NULL),(2063,0,NULL,'2017-11-03 12:28:12',0,NULL,NULL),(2064,1000,NULL,'2017-11-06 09:01:19',0,NULL,NULL),(2065,0,NULL,'2017-11-06 09:12:00',0,NULL,NULL),(2066,0,NULL,'2017-11-06 10:08:14',0,NULL,NULL),(2067,0,NULL,'2017-11-06 10:08:14',0,NULL,NULL),(2068,0,NULL,'2017-11-06 10:08:14',0,NULL,NULL),(2069,0,NULL,'2017-11-06 10:08:15',0,NULL,NULL),(2070,0,NULL,'2017-11-06 10:08:15',0,NULL,NULL),(2071,0,NULL,'2017-11-06 10:08:16',0,NULL,NULL),(2072,0,NULL,'2017-11-06 10:08:16',0,NULL,NULL),(2073,0,NULL,'2017-11-06 10:08:17',0,NULL,NULL),(2074,0,NULL,'2017-11-06 10:08:18',0,NULL,NULL),(2075,0,NULL,'2017-11-06 10:08:18',0,NULL,NULL),(2076,0,NULL,'2017-11-06 14:11:36',0,NULL,NULL),(2090,0,NULL,'2017-11-09 13:31:20',0,NULL,NULL),(2091,1500,NULL,'2017-11-10 18:15:57',0,NULL,NULL),(2092,0,NULL,'2017-11-13 17:54:30',0,NULL,NULL),(2093,0,NULL,'2017-11-14 17:08:02',0,NULL,NULL),(2094,0,NULL,'2017-11-14 17:21:40',0,NULL,NULL),(2095,0,NULL,'2017-11-14 17:32:57',0,NULL,NULL),(2096,0,NULL,'2017-11-14 17:45:02',0,NULL,NULL),(2097,0,NULL,'2017-11-14 18:02:10',0,NULL,NULL),(2098,0,NULL,'2017-11-14 18:12:57',0,NULL,NULL),(2099,0,NULL,'2017-11-15 17:50:26',0,NULL,NULL),(2102,0,NULL,'2017-11-17 12:30:31',0,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
